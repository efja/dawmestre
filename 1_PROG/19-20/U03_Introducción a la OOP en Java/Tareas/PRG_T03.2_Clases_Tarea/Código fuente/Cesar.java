public class P2 {
    public static String cesar(String text, int d) {
        String res = "";
        int range = 'Z' - 'A' + 1;
        text = text.toUpperCase();
        for(int i=0; i<text.length(); i++) {
            char c = text.charAt(i);
            if(c>='A' && c<='Z') {
                c += d;
                if(c>'Z') c -= range;
                else if(c<'A') c += range;
            }
            res += c;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(cesar("Alea iacta est", 3));
        System.out.println(cesar("Ave, Caesar, morituri te salutant", 7));
        System.out.println(cesar("VW VCODKGP, DTWVQ?", -2));
    }
}