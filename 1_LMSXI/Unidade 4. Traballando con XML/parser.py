import os.path
import xml.etree.ElementTree as ET

def media(lista):
    return sum(lista)/len(lista)


# Crea lista de strings e imprimide un a un
estudantes =  ["a18franciscogc","A18JulioPC","a18cristinarp","a18jorgepr1","a18juancg","a16oscarmv","a15alfonsofm","a14simonda","a18lorenzotc","a18miguelyr","a18braiscv","a18daviddo"]
ruta_inicial = "/media/isaacjgonzalez/Alumnos/dawMP/"
ruta_final = "/LinguaxesDeMarcas/notas.xml"

alumno_notas = {}
for i in range(1,14):
    alumno_notas[i] = []

for estudante in estudantes:
    ruta_completa = ruta_inicial + estudante + ruta_final
    print(ruta_completa)
    if (os.path.isfile(ruta_completa)):
        try:
            # Programa que parsea as notas en xml e calcula a media
            tree = ET.parse(ruta_completa)
            root = tree.getroot()

            for child in root:
                if (child.tag == "exposicion"):
                    id_da_exposicion =  int(child.attrib["id"])
                    if child[1].text == None:
                        continue
                    nota_da_exposicion = float(child[1].text)
                    #print("id: ",id_da_exposicion," nota: ",nota_da_exposicion)
                    alumno_notas[id_da_exposicion].append(nota_da_exposicion)
            print()
        except Exception as e:
            #raise "Erro parseando ou recorrendo o xml"
            print("Erro parseando ou recorrendo o xml, erro: ",e)

    else:
        print("Non puiden atopar o ficheiro: ", ruta_completa)



for alumno in sorted(alumno_notas.keys()):
    print("Alumno: ",alumno," Media: ", round(media(alumno_notas[alumno]),2))
