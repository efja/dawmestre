## [Patrón de diseño Singleton](https://es.wikipedia.org/wiki/Singleton)

## [Patrón de diseño Factory Method](https://es.wikipedia.org/wiki/Factory_Method_(patr%C3%B3n_de_dise%C3%B1o))

## [Patrón de diseño Observer](https://es.wikipedia.org/wiki/Observer_(patr%C3%B3n_de_dise%C3%B1o))

## [Patrón de diseño arquitectónico MVC URL](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador)

