function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

// Gardamos o elemento html do spinner en unha variable (ollo, debemos recoller o container para ocultar tamén o fondo)
var spinnerEL = document.getElementById('spinner');

/* Poñemos a JS a escoitar cándo remata a transición que cambia a opacidade de 1 a 0
Cando a transición remate precisamos engadir un display: none para que o spinner 
NON impida interactuar co contido da páxina */
spinnerEL.addEventListener('transitionend', addDisplayNone);
spinnerEL.addEventListener('webkitTransitionEnd', addDisplayNone);


/* Definimos a función encargada de engadir display:none ao spinner */
var addDisplayNone = (event) => { 
    event.target.style.display = 'none';

    /* Unha vez ocultamos o spinner completamente xa non precisamos
     seguir escoitando si a transición remata, polo que podemos eliminar o listener */
    spinnerEL.removeEventListener('transitionend', addDisplayNone);
    spinnerEL.removeEventListener('webkitTransitionEnd', addDisplayNone);
};

/* Cando o documento está listo pasamos unha función que engade a clase hide
Esta aplica o css opacity:0 que iniciará a transición do spinner para ocultalo */
docReady( () => { spinnerEL.classList.add('hide'); } );


// A SECUENCIA SERÍA ALGO ASÍ:
// Reload

// Peticións a servidor
// Inicia a descarga de arquivos (img, js, css, html)
// remata a descarga e comenza a renderizar

// Cando a páxina está interactiva e renderizada 
//execútase a función que pasamos como parámetro de docReady

// Spinner inicia a transición para opacity 0

// CANDO REMATA A TRANSICIÓN engadimos display: none;
