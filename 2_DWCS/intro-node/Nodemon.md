# Nodemon

*nodemon* es una herramienta que ayuda a desarrollar aplicaciones basadas en node.js al reiniciar automáticamente la aplicación de nodo cuando se detectan cambios de archivo en el directorio.

## Instalación

```bash
    npm install -g nodemon
```
