let deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre: function() {
        return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder}`
    }
};

// let nombre = deadpool.nombre;
// let apellido = deadpool.apellido;
// let poder = deadpool.poder;

// Se puede omitir las tres líneas comentadas anteriores para hacerlo con desestructuración
let { nombre: primerNombre, apellido, poder } = deadpool;

console.log(primerNombre, apellido, poder);