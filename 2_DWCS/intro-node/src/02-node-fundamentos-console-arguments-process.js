console.log(process.argv);

// Desestructuración de arrays
const [ , , arg3, arg4] = process.argv;

console.log("arg3:", arg3);
console.log("arg4:", arg4);

const [ , numVeces] = arg3.split('=');
const [ , mensaje]  = arg4.split('=');

console.log(`Voy a imprimer ${numVeces} veces el mensaje: ${mensaje}`);
for (let num = 0; num < numVeces; num++) {
    console.log(mensaje);    
}


