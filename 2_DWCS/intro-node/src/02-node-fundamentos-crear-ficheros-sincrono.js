const fs = require('fs');

const cadena_to_save = "adios mundo";

// Revisa qué sucede si la carpeta 'temp' está creada o no en la raiz del repo
try {
    fs.writeFileSync("temp/holamundo.txt", cadena_to_save);
    console.log("Terminé de grabar el fichero");
}
catch(ex){
    console.log("Hubo un problema en el momento grabar el fichero", ex);
}
