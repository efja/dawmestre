let nombre = 'Paco';
let real = '(Francisco)';

// Se puede ejecutar con concatenación como en Java
console.log(nombre + ' ' + real);

// Otra forma es usar 'template strings' con 'back tips'
console.log(`${ nombre } ${ real }`);

// Una característica es que sictáticamente son idénticos
let nombreCompleto = nombre + ' ' + real;
let nombreTemplate = `${ nombre } ${ real }`;

// Ambas variables son exactamente iguales. Tanto contenido como identidad son iguales
console.log(nombreCompleto === nombreTemplate);

// Función que refresa un string
function getNombre() {
    return `${ nombre } ${ real }`;
}

// Puedo inyectar la función directamente
console.log(`El nombre de: ${ getNombre() }`);