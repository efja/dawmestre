// Realizar nuestra propia función similar a timeout que ejecute un callback
let getUsuarioById = (id, callback) => {
    let usuario = {
        nombre: null,
        id
    }

    // Aquí se realizaría una consulta a db (imagínate esto funcionando en segundo plano)
    /////////////////////////////////////////
        // Se simula en este ejemplo que sólo existe el usuario con id 20
        if (id === 10) {
            usuario.nombre = "Manuel";
            callback(null, usuario);
        } else {
            callback(`El usuario con id ${ id }, no existe en la BD`);
        }
    /////////////////////////////////////////

    // El bloque anterior se ejecutaría en segundo plano y pasaría a la cola de 'Node Apis'
    // La ejecución seguiría por aquí a la espera de que se invoque el callback
    // Nota. Tal y como está este ejemplo, esto no sucede y la llamada al callback será síncrona
}

// 1ª Instrucción que invoca a nuestra función `getUsuarioById`
getUsuarioById(10, (err, usuario) => {
    if (err) {
        return console.log(err);
    }
    console.log('Usuario de base de datos', usuario);
});

// 2ª Instrucción que invoca a nuestra función `getUsuarioById`
getUsuarioById(20, (err, usuario) => {
    if (err) {
        return console.log(err);
    }
    console.log('Usuario de base de datos', usuario);
});
