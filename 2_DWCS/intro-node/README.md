# Node

- [1. Básicos](#1-básicos)
- [2. Fundamentos de ECMAScrip](#2-fundamentos-de-ecmascrip)
  - [2.1. `let` vs `var`](#21-let-vs-var)
  - [2.2. Plantillas literales (Template strings)](#22-plantillas-literales-template-strings)
  - [2.3. Clases y objetos](#23-clases-y-objetos)
  - [2.4. Funciones tradicionales](#24-funciones-tradicionales)
    - [2.4.1. Ejemplo: El objeto `arguments`](#241-ejemplo-el-objeto-arguments)
    - [2.4.2. Ejemplo: Uso de parámetros `rest`](#242-ejemplo-uso-de-parámetros-rest)
  - [2.5. Funciones de flecha](#25-funciones-de-flecha)
    - [2.5.1. Ejemplo: Función con DOS parámetros de entrada y un `return`](#251-ejemplo-función-con-dos-parámetros-de-entrada-y-un-return)
    - [2.5.2. Ejemplo: Función SIN parámetros de entrada y un `return`](#252-ejemplo-función-sin-parámetros-de-entrada-y-un-return)
    - [2.5.3. Ejemplo: Función con UN parámetro de entrada y un `return`](#253-ejemplo-función-con-un-parámetro-de-entrada-y-un-return)
    - [2.5.4. Ejemplo: Uso problemático del `this`](#254-ejemplo-uso-problemático-del-this)
    - [2.5.5. Ejemplo: Retornando objetos literales](#255-ejemplo-retornando-objetos-literales)
    - [2.5.6. Ejemplo: Funciones de flecha con parámetros `rest`](#256-ejemplo-funciones-de-flecha-con-parámetros-rest)
  - [2.6. Callbacks](#26-callbacks)
  - [2.7. Promesas](#27-promesas)
  - [2.8. Desestructuración](#28-desestructuración)
    - [2.8.1. Desestructuración de objetos](#281-desestructuración-de-objetos)
    - [2.8.2. Desestructuración directamente en los argumentos de una función](#282-desestructuración-directamente-en-los-argumentos-de-una-función)
    - [2.8.3. Desestructuración de arrays](#283-desestructuración-de-arrays)
- [3. Fundamentos Node](#3-fundamentos-node)
  - [3.1. Requerir paquetes. Cómo grabar un fichero en disco](#31-requerir-paquetes-cómo-grabar-un-fichero-en-disco)
  - [3.2. Importar archivos de nuestro proyecto](#32-importar-archivos-de-nuestro-proyecto)
  - [3.3. Inicio de un proyecto node: `package.json` y gestión de dependencias](#33-inicio-de-un-proyecto-node-packagejson-y-gestión-de-dependencias)
  - [3.4. Recibir información desde la línea de comandos](#34-recibir-información-desde-la-línea-de-comandos)
    - [3.4.1. Método primitivo](#341-método-primitivo)
    - [3.4.2. Paquete `Yargs`](#342-paquete-yargs)
  - [3.5. Manejar el stdin - stdout y Readline para una interfaz por terminal](#35-manejar-el-stdin---stdout-y-readline-para-una-interfaz-por-terminal)
  - [3.6. Eventos en Node](#36-eventos-en-node)
- [4. Fundamentos webserver con Node](#4-fundamentos-webserver-con-node)
  - [4.1. Objetos `request` y `response`](#41-objetos-request-y-response)
  - [4.2. Manipular en `Content-Type` de la respuesta](#42-manipular-en-content-type-de-la-respuesta)
  - [4.3. Procesar las rutas y la información del cliente](#43-procesar-las-rutas-y-la-información-del-cliente)
- [5. Implementar un webserver con Nodejs y Express](#5-implementar-un-webserver-con-nodejs-y-express)
  - [Inicializar un Web Server con Express](#inicializar-un-web-server-con-express)
  - [Routing básico con Express](#routing-básico-con-express)
- [Servir ficheros estáticos](#servir-ficheros-estáticos)

Node se autodefine de la siguiente forma:

> Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, `npm`, us the largest ecosystem of open source libraries in the world.

Node.js es un *runtime* de JavaScript construido sobre el [motor V8 de Google Chrome](https://v8.dev). Node.js utiliza un modelo de E/S sin bloqueo controlado por eventos que lo hace liviano y eficiente. El ecosistema de paquetes de Node.js, [`npm`](https://www.npmjs.com), es el ecosistema de bibliotecas de código abierto más grande del mundo.

También como:

> Ideado como un entorno de ejecución de JavaScript **orientado a eventos asíncronos**, *Node.js* está diseñado para crear aplicaciones network escalables.

## 1. Básicos

Para ejecutar un [script](src/01-fundamentos-holamundo.js) node simplemente:

```bash
node intro-node/src/holamundo
```

Es posible que quieras estar ejecutando un script de forma continuada mientras se detecte algún cambio en el mismo. Para ello, utiliza [Nodemon](Nodemon.md)

```bash
nodemon intro-node/src/holamundo
```

## 2. Fundamentos de ECMAScrip

### 2.1. `let` vs `var`

Ambos definen la posibilidad de declarar una variable. Sin embargo `let` es más específico y protector respecto al ámbito en el que se declara una variable a diferencia de la permisividad de `var`:

- `let` no deja volver a iniciar una variable ya iniciada en el mismo ámbito
- `let` destruye la variable cuando finaliza el ámbito en el que fue inicializada

```bash
nodemon intro-node/src/let-var.js
```

Puedes depurar estos ejemplos directamente transcritos [al script](src/01-fundamentos-01-fundamentos-let-var.js).

### 2.2. Plantillas literales (Template strings)

[Las plantillas literales](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/template_strings) son cadenas literales que habilitan el uso de expresiones incrustadas. Con ellas, es posible utilizar cadenas de caracteres de más de una línea, y funcionalidades de interpolación de cadenas de caracteres.

En ediciones anteriores de la especificación ES2015, solían llamarse "plantillas de cadenas de caracteres".

Supongamos la definición de dos cadenas:

```js
let nombre = 'Paco';
let real = '(Francisco)';
```

La forma más clásica de concatenarlas (tb en Java, PHP...)

```js
console.log(nombre + ' ' + real);
```

Otra forma es usar 'template strings' con 'back tips'

```js
console.log(`${ nombre } ${ real }`);
```

Una característica es que sictáticamente son idénticos

```js
let nombreCompleto = nombre + ' ' + real;
let nombreTemplate = `${ nombre } ${ real }`;

// Ambas variables son exactamente iguales. Tanto contenido como identidad son iguales
console.log(nombreCompleto === nombreTemplate);
```

Y es frecuente también su uso en funciones:

```js
// Función que refresa un string
function getNombre() {
    return `${ nombre } ${ real }`;
}

// Puedo inyectar la función directamente
console.log(`El nombre de: ${ getNombre() }`);
```

Puedes depurar estos ejemplos directamente transcritos [al script](src/01-01-fundamentos-template-string.js).

### 2.3. Clases y objetos

Hacemos referencia a saber qué es un objeto:

> JavaScript está diseñado en un **paradigma simple basado en objetos**. Un objeto es una colección de propiedades, y una propiedad es una asociación entre un nombre (o clave) y un valor. El valor de una propiedad puede ser una función, en cuyo caso la propiedad es conocida como un método.

Haremos referencia al concepto clase:

> Las clases de javascript, introducidas en ECMAScript 2015, son una mejora sintáctica sobre la herencia basada en prototipos de JavaScript. La sintaxis de las clases no introduce un nuevo modelo de herencia orientada a objetos en JavaScript. Las clases de JavaScript proveen una sintaxis mucho más clara y simple para crear objetos y lidiar con la herencia.

Recuerda que en Javascript no necesitamos definir una clase para crear una instancia de objetos. Podemos hacerlo con *objetos literales*:

```js
let personaje = {
    nombre: 'Tony Stark',
    codeName: 'Ironman',
    vivo: false,
    edad: 40,
    coords: {
        lat: 34.034,
        lng: -118.70
    },
    trajes: ['Mark I', 'Mark V', 'Hulkbuster'],
    direccion: {
        zip: '10880, 90265',
        ubicacion: 'Malibu, California',
    },
    // No se recomienda utilizar ni espacio ni caracteres especiales en las claves de las propiedades
    'ultima-pelicula': 'Infinity War'
};
```

La forma de acceso a las propiedades del objeto sería:

```js
console.log( personaje );
console.log('Nombre', personaje.nombre );
console.log('Nombre', personaje['nombre'] );
console.log('Edad', personaje.edad );

console.log('Coors', personaje.coords );
console.log('Lat', personaje.coords.lat );

console.log('Num. Trajes', personaje.trajes.length );
console.log('último traje', personaje.trajes[ personaje.trajes.length - 1 ] );

// A través de un valor definido en una variable intermedia
const x = 'vivo';
console.log('Vivo', personaje[x] );

// No podemos utilizar la notación de punto en este caso:
console.log('Última película', personaje['ultima-pelicula'] );
```

Es posible eliminar una propiedad de un objeto:

```js
delete personaje.edad;
```

Para hacer inmutable un objeto:

```js
// Cogela las propiedades del objeto pero no de los objetos que contiene (p.e. dirección)
Object.freeze(personaje);
```

Para obtener propiedades y valores de un objeto:

```js
const propiedades = Object.getOwnPropertyNames( personaje );
const valores     = Object.values( personaje );
```

Es muy recomendable refrescar estos aspecto leyendo los siguientes artículos:

- [Conceptos básicos de los objetos JavaScript](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/Basics)
- [Las clases de javascript](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Classes)
- [Trabajando con objetos](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Trabajando_con_objectos)
- [Prototipos de objetos](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/Object_prototypes)

También recuerda que:

> JavaScript es a menudo descrito como un lenguaje basado en prototipos - para proporcionar mecanismos de herencia, los objetos pueden tener un objeto prototipo, el cual actúa como un objeto plantilla que hereda métodos y propiedades.

```js
const person = {
  isHuman: false,
  printIntroduction: function() {
    console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
  }
};

const me = Object.create(person);

me.name = 'Matthew'; // "name" is a property set on "me", but not on "person"
me.isHuman = true; // inherited properties can be overwritten

me.printIntroduction();
// expected output: "My name is Matthew. Am I human? true"
```

### 2.4. Funciones tradicionales

Las funciones son uno de los bloques de construcción fundamentales en JavaScript. Y como tal conviene repasar [la documentación sobre ellas](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones).

Destacar sobre ellas el uso del objeto [`arguments`](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones#usar_el_objeto_arguments) y el uso de [parámetros `rest`](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones#parámetros_rest).

#### 2.4.1. Ejemplo: El objeto `arguments`

```js
function imprimeArgumentos(){
    console.log( arguments );
}
imprimeArgumentos("hola", "adios", true, 1, null, {} )
```

Imprimirá (Para ejecutar y que lo muestre por consola formateado se ejecutó: `console.log( JSON.stringify( arguments, null,'  ') );`):

```js
{
  "0": "hola",
  "1": "adios",
  "2": true,
  "3": 1,
  "4": null,
  "5": {}
}
```

Es decir, todas las funciones definidas en su forma tradiciona (no como funciones de flecha), tendrán definido el objeto `arguments`. Un array con todos sus parámetros.

#### 2.4.2. Ejemplo: Uso de parámetros `rest`

La sintaxis del parámetro rest nos permite representar un número indefinido de argumentos como un array.

```js
function mifuncion(...losArgumentos){
    console.log(losArgumentos);
}
mifuncion("hola", "adios", true, 1, null, {} );
```

Imprimirá:

```js
[ 'hola', 'adios', true, 1, null, {} ]
```

Precaución porque argumentos **colocados después** de los parámetros `rest` no serán tenidos en cuenta para nada:

```js
//'esteArgSeraTotalmenteInutil' is declared but its value is never read.ts(6133)
function mifuncion(...losArgumentos, esteArgSeraTotalmenteInutil){
    console.log(losArgumentos);
}
```

Colocar argumentos previos de los parámetros `rest` se tratarán por separado:

```js
function mifuncion(otroArgumento, ...losArgumentos){
    console.log(losArgumentos);
}
mifuncion("hola", "adios", true, 1, null, {} );
```

Imprimirá:

```js
[ 'adios', true, 1, null, {} ]
```

### 2.5. Funciones de flecha

Una expresión de [función flecha](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Funciones/Arrow_functions) es una alternativa compacta a una expresión de función tradicional, pero es limitada y no se puede utilizar en todas las situaciones.

Diferencias y limitaciones:

- No tiene sus propios enlaces a `this` o `super` y no se debe usar como métodos.
- No tiene argumentos o palabras clave `new.target`.
- No apta para los métodos `call`, `apply` y `bind`, que generalmente se basan en establecer un ámbito o alcance
- No se puede utilizar como `constructor`.
- No se puede utilizar `yield` dentro de su cuerpo.

Ejemplos de cómo transformar funciones tradiciones a funciones de flechas:

```js
// Función de flecha
let nombreFuncion = () => {
    // Instrucciones
}

// Si la función tiene una única instruccion de retorno p.e. Parámetros a y b -> return a + b
// se omitirán las llaves y el return
let nombreFunc = (param1, param2) => param1 + param2;
```

#### 2.5.1. Ejemplo: Función con DOS parámetros de entrada y un `return`

```js
// Ejemplo 1
function sumar(a, b) {
    return a + b;
}

let sumar = (a, b) => {
    return a+b;
}

// Se puede acortar más cuando tiene sólo una línea
let sumar = (a, b) => a + b;
```

#### 2.5.2. Ejemplo: Función SIN parámetros de entrada y un `return`

```js
// Ejemplo 2
function saludar() {
    return 'Hola Mundo';
}
let saludar = () => 'Hola mundo';
```

#### 2.5.3. Ejemplo: Función con UN parámetro de entrada y un `return`

```js
// Ejemplo 3
function saludar(nombre) {
    return `Hola ${ nombre }`;
}
let saludar = (nombre) => `Hola ${ nombre }`

// También funcionaría pero por estilo es preferible no omitir los paréntesis
let saludar = nombre => `Hola ${ nombre }`
```

#### 2.5.4. Ejemplo: Uso problemático del `this`

Las funciones de flecha implican en algunas ocasiones algún problema:

```js
// Caso problemático
let persona = {
    nombre: 'Manuel',
    apellido: 'Pérez',
    telf: '+34 ..',
    // En este caso, con la función de flecha tendríamos problemas con la palabra reservada `$this`
    getNombre: () =>  {
        return `${ this.nombre } ${ this.apellido } - contacto: ${ this.telf}`
    }
};
```

En este caso, con la función de flecha tendríamos problemas con la palabra reservada `this`, porque esta palabra podría pensarse que hace referencia al objeto `persona` **pero NO es así**. En este caso `this` hace referencia en node js al objeto `Global`.

#### 2.5.5. Ejemplo: Retornando objetos literales

Podemos utilizar una función factoría que nos retorne un literal de esta manera:

```js
function crearPersona(nombre, apellido){
    return {
        nombre, //No es necesario poner nombre:nombre al ser exactametne iguales
        apellido
    }
}
```

Si lo transformamos en función de fecha nótese que **es necesario poner el objeto literal entre paréntesis** para distinguir entre el cuerpo de la función y el cuerpo del objeto literal:

```js
const crearPersona = (nombre, apellido) => ( { nombre, apellido} );
```

#### 2.5.6. Ejemplo: Funciones de flecha con parámetros `rest`

No será posible utilizar el [`arguments`](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones#usar_el_objeto_arguments) en funciones de flecha. Sólo dispondremos de ese objeto en la forma tradicionl de definir funciones.

Para poder operar de igual forma con funciones de flecha, tendremos que usar [parámetros `rest`](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones#parámetros_rest):

```js
const imprimeArgumentos = (...args) => console.log(args);
```

### 2.6. Callbacks

Los callbacks no son más que una función que se manda como argumento.

En node js es de vital importancia comprender esta técnica.

Si no los conoces puedes ver [esta descripción a través de ejemplos](01-fundamentos-callbacks.md) para profundizar más en su implementación.

### 2.7. Promesas

Una *Promise* (promesa en castellano) es un objeto que representa la terminación o el fracaso de una operación asíncrona.

Si no las conoces puedes ver [esta descripción a través de ejemplos](01-fundamentos-promesas.md) para profundizar más en su implementación o ver [esta documentación](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Usar_promesas).

### 2.8. Desestructuración

#### 2.8.1. Desestructuración de objetos

Supongamos la definición del siguiente objeto literal:

```js
const manu = {
    nombre: "Manuel",
    apellido: "Pérez",
    activo: true, 
    getNombre() {
        return `${this.nombre} ${this.apellido}`
    }
}
```

Supongamos que necesitamos declarar variables inicializadas con valores que forman parte de propiedades de un objeto:

```js
const nombre = manu.nombre;
const apellido = manu.apellido;
const activo = manu.activo;
```

Empleando **desestructuración** de objetos se evitan las líneas precedentes quedando más sencillo el códido.

```js
const { nombre, apellido, activo } = manu;
```

Tambien se pueden emplear **propiedades que pueden no existir** en el objeto:

```js
const { nombre, apellido, activo, edad } = manu;
console.log(nombre, apellido, activo, edad); // Resultado será: Manuel Pérez true undefined
```

O establecer un **valor por defecto** para ella si no tuviesen esa propiedad:

```js
const { nombre, apellido, activo, edad=0 } = manu;
console.log(nombre, apellido, activo, edad); // Resultado será: Manuel Pérez true 0
```

#### 2.8.2. Desestructuración directamente en los argumentos de una función

Podemos utilizar la desestructuración en funciones, con la particularidad de poder definir un valor `const` o no:

```js
function imprimePersona(persona){
    const { nombre, apellido, activo, edad=0 } = persona;
    //nombre = "Pepe"; // Uncaught TypeError: Assignment to constant variable.
    console.log(nombre, apellido, activo, edad);
}
imprimePersona(manu)
```

El siguiente código sería equivalente al previo. Desestructurar directamente en los argumentos. Ten en cuenta que **debe ser definida como function tradicional**. Realizar la desestructuración en los argumentos a diferencia del ejemplo previo, implica que no estamos definiciendo las variables como `const` y que por tanto pueden ser cambiadas. Es la única particularidad.

```js
function imprimePersona({ nombre, apellido, activo, edad=0 }){
    nombre = "Pepe"; // Esto sí se permitiría
    console.log(nombre, apellido, activo, edad);
}
imprimePersona(manu)
```

#### 2.8.3. Desestructuración de arrays

Supongamos la definición del siguiente array:

```js
const heroes = ["Deadpool", "Superman", "Batman"];
```

Y extrayendo sus valores de la forma 'tradicional':

```js
const h1 = heroes[0];
const h2 = heroes[1];
const h3 = heroes[2];
console.log(h1, h2, h3);
```

**Desestructurando esta vez un array**. Obtenemos el mismo resultado:

```js
const [h1, h2, h3] = heroes;
```

Si no interesa todos los elementos del array podríamos sólo definir la variable h3:

```js
const [ , , h3] = heroes;
```

## 3. Fundamentos Node

En este apartado se pretende sintetizar fundamentos de Node:

- Cómo crear un proyecto Node
- Cómo utilizar otros paquetes de Node en nuestro proyecto

### 3.1. Requerir paquetes. Cómo grabar un fichero en disco

Para escribir un fichero en disco Node ya provee de una api de funciones para lograrlo. Sencillamente iríamos a consultar la documentación buscando por las palabras clave `File System`, localizando [este recurso](https://nodejs.org/dist/latest-v14.x/docs/api/fs.html).

En esa documentación nos pueden interesar las siguientes funciones:

- [`fs.writeFile(file, data[, options], callback)`](https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_writefile_file_data_options_callback)
- [`fs.writeFileSync(file, data[, options])`](https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_writefilesync_file_data_options)

Para utilizar estas funciones no hay que importar un fichero específico pues ya forman parte de la API de Node. Sencillamente sería necesaria la siguiente instrucción para usarlos:

```js
const fs = require('fs');
```

**Si se emplea el método asíncrono**, como puedes ver en el [script](src/02-node-fundamentos-crear-ficheros-asincrono.js), la documentación nos facilita una firma de la función con un *callback*, que nos indicará que el proceso de guardado concluyó o por el contrario nos pasa como parámetro el error provocado.

```js
const cadena_to_save = "adios mundo";

// Revisa qué sucede si la carpeta 'temp' está creada o no en la raiz del repo
fs.writeFile("temp/holamundo.txt", cadena_to_save, (err) => {
    if(!err){
        console.log("Fichero guardado correctamente");
    }
    else{
        console.log(err);
    }
});

console.log("Estoy en ello...");
```

Nótese que se ejecutará la línea `console.log("Estoy en ello...");` antes de que se guarde el fichero.

**Si se emplea el método síncrono**, como puedes ver en el [script](src/02-node-fundamentos-crear-ficheros-sincrono.js), tenemos que tener en cuenta que este puede fallar y que por lo tanto debe ser envuelto entre `try`/`catch`:

```js
const fs = require('fs');

const cadena_to_save = "adios mundo";

// Revisa qué sucede si la carpeta 'temp' está creada o no en la raiz del repo
try {
    fs.writeFileSync("temp/holamundo.txt", cadena_to_save);
    console.log("Terminé de grabar el fichero");
}
catch(ex){
    console.log("Hubo un problema en el momento grabar el fichero", ex);
}
```

Nótese que NO se seguirán realizando operaciones hasta que el fichero se guarde éxitosamente o por el contrario provoque un error.

> Para que los siguientes ejemplos no concluyan en error, puedes crear el la raiz de este repo una carpeta nombrada literalmente `temp/`

### 3.2. Importar archivos de nuestro proyecto

> Node.js follows the CommonJS module system, and the builtin require function is the easiest way to include modules that exist in separate files. The basic functionality of require is that it reads a JavaScript file, executes the file, and then proceeds to return the exports object

Supongamos que vamos a definir una función `saluda` en un [fichero `js`](src/02-node-fundamentos-script-saluda.js) que queremos emplear desde otro fichero. Esa función sólo estará definida en el *scope* del fichero en el que fue definida a menos que la exportemos.

Podremos hacerlo de la siguiente forma:

```js
module.exports = saluda
```

Desde [otro script](src/02-node-fundamentos-script-require-saluda.js) ahora podríamos invocar a esa función:

```js
require('./02-node-fundamentos-script-saluda')
saluda('Manuel');
```

¿Qué pasaría si existiesen más funciones definidas en ese fichero [fichero `js`](src/02-node-fundamentos-script-saluda.js)? En lugar de retornar en ese objeto global `module.exports` sólo la función `saluda` podríamos querer retornar varias funciones que queremos hacer 'públicas' a otros módulos. Podríamos retornar un objeto en ese caso:

```js
module.exports = {
    saluda,
    saluda_cortesmente
}
```

Para a continuación requerir en otros script las definiciones de las funciones que se necesitan:

```js
// Por medio de desestructuración del objeto devuelto en 'module.exports'
const { saluda } = require('./02-node-fundamentos-script-saluda')

saluda('Manuel');
```

Si necesitásemos ambas funciones publicadas en el script, entoces por desestructuración obtendríamos las dos:

```js
const { saluda, saluda_cortesmente } = require('./02-node-fundamentos-script-saluda')

saluda('Manuel');
saluda_cortesmente("José")
```

### 3.3. Inicio de un proyecto node: `package.json` y gestión de dependencias

Para iniciar un proyecto en node, podemos utilizar el siguiente script:

```bash
npm init
```

Esto provocará una serie de introducción de datos de forma interactiva:

```bash
package name: (my-project-node) top-invernaderos
version: (1.0.0) 
description: Un proyecto para gestión de invernaderos
entry point: (index.js) app.js
test command: 
git repository: 
keywords: 
author: Gabriel Gutierrez
license: (ISC) MIT
About to write to /Users/user/git/my-project-node/package.json:

{
  "name": "home-register",
  "version": "1.0.0",
  "description": "Un proyecto para gestión de invernaderos",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Enrique Agrasar",
  "license": "MIT"
}


Is this OK? (yes)
```

Y esto lo que provará es que en la raiz del proyecto creará un fichero `package.json` que te sugiere crear para que confirmes su creación.

Además, ha creado un script para lanzar los tests automatizados que codifiquemos. Como no se ha especificado ningún *test command*, por ese motivo crea el script con sencillamente un `echo` de advertencia. Podríamos incluir más scripts, y se invocarían de la siguiente forma:

```bash
# npm run <name_script>
npm run test
```

También en el archivo `package.json` se van a gestionar las dependencias (otros módulos) de nuestro proyecto. Podríamos especificar directamente en el fichero la dependencia que necesitamos (por ejemplo [esta](https://www.npmjs.com/package/colors)), pero resulta mucho más productivo realizarlo con el `npm`:

```bash
# Ejecutar en la raiz del proyecto (Equivalente: npm i colors)
npm install colors
```

Esto agraga la siguiente propiedad en el fichero `package.json`:

```json
"dependencies": {
    "colors": "^1.4.0"
  }
```

En la misma raiz del proyecto se crea el directorio **`node_modules/`** donde se descargan los scripts necesarios para utilizar estos módulos. **Este directorio es voluble**, lo que quiere decir que lo gestionará el `npm` y que puede cambiar fácilmente, con la agregación de más dependencias o actualización de las mismas. No debe editarse ni manipularse directamente. Por otra parte, es fácilmente recreable simplemente con el comando `npm install`, por lo que NO es necesario, ni en absoluto recomendable, introducir su segimiento en git (ni ningún otro sistemas de control de veriones).

Por otra parte, también se crea el fichero **`package-lock.json`**. Este fichero sí que conviene mantenerlo bajo el control de versiones (git). Este fichero ayudará a recrear las misma dependencias en otro equipo de un compañero. En el la dependencia se ha indicado que se utilizará la versión de `colors` `1.4.0` *o superior*. Puede que otro compañero, en el momento en el que se obtenga el proyecto, al realizar `npm install` se obtenga la versión de `colors` `1.5.0`. Si queremos evitar eso, y que el compañero se pueda recrear exactamente el directorio `node_modules` de su compañero, debe tener el fichero `package-lock.json` y ejecutar en su lugar `npm ci` para que no tenga en cuenta las posibles nuevas versiones de módulos dependientes. Se recomienda [esta lectura](https://elabismodenull.wordpress.com/2017/07/07/el-fichero-package-lock-json-tengo-que-versionarlo/) para saber más sobre el tema.

También es posible partir de una dependencia en una versión concreta. Podría hacerlo de esta forma:

```bash
npm install colors@1.0.0
```

Con el comando **`npm update`**, actualizaría los paquetes o módulos a su última versión y se actualizaría el fichero `package.json`.

Si hay ciertas dependiencias que sólo tienen sentido en un entorno de desarrollo (y no en un sistema en producción), se pueden agregar empleando:

```bash
npm install nodemon --save-dev
```

Esto agregará las dependencias en una colección diferente dentro del mismo archivo `package.json`:

```json
"devDependencies": {
    "nodemon": "^2.0.7"
  }
```

### 3.4. Recibir información desde la línea de comandos

#### 3.4.1. Método primitivo

La obtención de los argumentos de forma nativa en Node, podríamos realizarla así:

```js
console.log(process.argv);
```

Al lanzar desde terminal [este script](src/02-node-fundamentos-console-arguments-process.js) en la raiz de este repo:

```bash
node intro-node/src/02-node-fundamentos-console-arguments-process.js --veces=3 --mensaje=hola
```

Obtendríamos la siguiente salida:

```bash
[
  '/usr/local/bin/node',
  '/Users/enrique/git/formacion/edu-wiki-dwcs/intro-node/src/02-node-fundamentos-console-arguments-process.js',
  '--veces=3',
  '--mensaje=hola'
]
```

Nótese que los dos primeros valores de este array hacen referencia a *paths*. Respectivamente directorio de invocación de node y directorio de invocación del script actual. Y luego a continuación los argumentos pasados al script.

El problema de este mecanismo que es toda la complegidad de interpretar o *parsear* estos argumentos no está implementada. Y no es tan sencillo como pudiera parecer:

- Debemos decidir si queremos que los **argumentos** sea **posicionales**. Cada posición (primera, segunda...) tiene un propósito específico... o puede que no.
- Debemos pensar en un posible argumento **`--help`** habitual para documentar los parámetros de un script.
- Debemos determinar si los parámetros son de la **forma** `param=1` o bien `param 1`.
- Debemos determinar si hay argumentos que tomarán posibles **valores por defecto**.
- Debemos determinar si hay argumento que tengan **una clave pero no un valor**. P. e. `--ignore` en lugar de `--ignore=true`

#### 3.4.2. Paquete `Yargs`

La complegidad expresada en el apartado anterior, podemos abordarla utilizando una forma bastante normalizada de pasar parámetros, empleando un *package* ya desarrollado que implementa y soluciona todos los problemas descritos; Se trata de [Yargs](https://www.npmjs.com/package/yargs). Se realizamos la instalación y posteriormente vemos cómo nos aporta los argumentos:

```js
const argv = require('yargs').argv;
console.log(argv);
```

Después de la ejecución del script anterior (denominado `app.js`):

```bash
node app --veces=3 --mensaje=hola
```

Obtendríamos un objeto con los parámetros. En este caso *Yargs* es mucho más efeciente: ya nos aporta los argumentos separados en forma *clave-valor*:

```json
{ _: [], veces: 3, mensaje: 'hola', '$0': 'app.js' }
```

Por otra parte, podríamos combinar la forma de especificar los parámetros. También sería capaz de interpretar esto:

```bash
node app veces 3 --mensaje=hola --colorear
```

Y lo interpretaría de la siguiente forma:

```json
{ _: [ 'veces', 3 ], mensaje: 'hola', colorear: true, '$0': 'app.js' }
```

Por último, sin configurarlo, también sería capaz de contestar a los parámetros `--help` y `--version`. Por ejemplo:

```bash
node app --version
```

Respondería, a falta de customizar la configuración de este, la siguiente salida:

```bash
Opciones:
  --help     Muestra ayuda                                            [booleano]
  --version  Muestra número de versión                                [booleano]
```

##### 3.4.2.1. Configurando Yargs

Debes acceder a la [documentación oficial de Yargs](https://yargs.js.org/docs/) pero realizaremos lo básico y más frecuente.

Por ejemplo, con el método [`options`](https://yargs.js.org/docs/#api-reference-optionskey-opt) podemos indicarle a `yargs` los argumentos que se espera recibir :

```js
const argv = require('yargs')
    .options('v', {
        alias: 'veces', // por convención el alias es la forma larga
        type: 'number', // para la conversión explícita
        demandOption: true, // es obligatorio introducirlo
        describe: 'Número de veces que un mensaje va a ser impreso', // para el help
    })
    .argv;
```

 Si ejecutamos la app con los siguientes parámetros: `node app.js --veces 3 --mensaje=hola --colorear` obtendríamos al realizar un `console.log(argv);`

 ```json
{
  _: [],
  veces: 3,
  v: 3,
  mensaje: 'hola',
  colorear: true,
  '$0': 'app.js'
}
 ```

 Nótese que al indicar `type: 'number'` para el parámetro veces, si introdujésemos por ejemplo `--veces=tres`, no fallaría. Simplemente el valor `v` y `veces` del objeto `argv` tendría el valor `NaN` (*Not a number*). Conviene hacer entonces una validación a mayores de los parámetros de entrada y eso se consigue con el método [`check`](https://yargs.js.org/docs/#api-reference-checkfn-globaltrue):

 ```js
 const { number } = require('yargs');

const argv = require('yargs')
    .options('v', {
        alias: 'veces', // por convención el alias es la forma larga
        type: 'number',
        demandOption: true,
        describe: 'Número de veces que un mensaje va a ser impreso', // para el help
    })
    .check((argv, options) => { // establezco una regla (debe retornar true/false)
        if (isNaN(argv.v)) {
            throw 'El parámetro -v o --veces debe ser un valor numérico'
        }
        return true;
    })
    .argv;
 ```

 Con esto conseguimos una definición, validación, documentación de los parámetros suministrados a la app con una mínima configuración. Podríamos definir todo este `require` y configuración en un fichero aparte, por ejemplo `config/yargs.js`. Exportarlo con `module.exports = argv` y realizar de forma mucho más simple `const argv = require('./config/yargs')` en nuestro fichero principal de aplicación.

### 3.5. Manejar el stdin - stdout y Readline para una interfaz por terminal

El módulo `readline` de Node js provee una interfaz para leer datos desde un *Stream* (por ejemplo la entrada estándar de un script por terminal - `process.stdin`), línea por línea

```js
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('What do you think of Node.js? ', (answer) => {
  // TODO: Log the answer in a database
  console.log(`Thank you for your valuable feedback: ${answer}`);

  rl.close();
});
```

### 3.6. Eventos en Node

Recordemos que NodeJS está ideado como un entorno de ejecución de JavaScript orientado a eventos asíncronos. Esto es la implementación de un patrón Observado-Suscriptor.

Veamos algún ejemplo:

En los lenguajes que son compilados, tenemos el proceso de compilación en el que se nos va avisando de errores. En el caso de NodeJS, no existe el proceso de compilación, pero tenemos **una serie de eventos que se publican cuando se produce un error**, y a los que podemos subscribirnos para realizar distintas acciones dependiendo del error que se produzca.

```js
process.on('unhandledRejection', (err, p) => {
  console.log('Custom Error: An unhandledRejection occurred')
  console.log(`Custom Error: Rejection: ${err}`)
})

process.on('uncaughtException', err => {
  console.log('Custom Error: An uncaughtException occurred')
  console.log(`Custom Error: Rejection: ${err}`)
})

setTimeout(() => console.log('This will still run.'), 500)

// El error es JSON.pasre
Promise(resolve => JSON.pasre({ color: 'azul' }))

// Error de referencia
test()

// Lanzar una exception
throw 'casa'
```

[Otro ejemplo](./src/02-node-fundamentos-eventos.js): Podemos utilar la librería [`readline`](https://nodejs.org/dist/latest-v14.x/docs/api/readline.html#readline_event_pause) de node en base a eventos y a callbacks. Quizás interesaría capturar el evento producido al presionar las teclas <kbd>Ctrl</kbd> + <kbd>C</kbd> que le envía la señal `SIGINT` al proceso para que termine. Se podría subscribir al evento de la siguiente forma:

```js
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('What do you think of Node.js? ', (answer) => {
  // TODO: Log the answer in a database
  console.log(`Thank you for your valuable feedback: ${answer}`);
  rl.close();
});

rl.on('SIGINT', () => {
    console.log("adios!");
    process.exit(0);
});
```

## 4. Fundamentos webserver con Node

Con node es muy sencillo [levantar o crear un web server](https://nodejs.org/dist/latest-v14.x/docs/api/http.html#http_http_createserver_options_requestlistener).

Tan sólo sería necesario esto:

```js
const http = require('http');

http.createServer( (request, response) => {
    res.write('Hola mundo');
    res.end();
}).listen(8080);
```

Esto, sencillamente crea un webserver en el host local escuchando por el puerto `8080` en el que por cada petición retornará el mensaje `Hola mundo`.

![Respuesta al webserver básico](img/03-webserver-http-serverbasico-cabeceras.png)

Nótese que el cliente web (Safari en este caso), ha recibido la respuesta y la ha presentado en pantalla. Sin embargo no conoce nada acerca de cómo ha de presentar esa respuesta (de qué tipo es, cómo lo debe renderizar, en qué idioma viene, etc. etc.). Nótese la ausencia de cabeceras http recibidas a este respecto.

### 4.1. Objetos `request` y `response`

En nuestro web server **podemos manejar tanto la petición como la respuesta** a esta porque se encuentran representadas con dos objetos, recibidos como parámetros de callback (`request` y `response` respectivamente).

[Por ejemplo](./src/03-webserver-http-serverbasico.js): Podríamos ver qué path está solicitando el cliente. Nótese que sea cual sea el patch, según el ejemplo anterior, el servidor siempre está retornando lo mismo. Pero podríamos consultar qué path está solicitando directamente el cliente.

Podríamos analizar algunas propiedades interesantes de la request para ver qué solicita el cliente:

```js
console.log(request.headers);
console.log('El cliente solicita: ', request.url);
console.log('El cliente envía el mensaje: ', request.method);
```

Podríamos incluso realizar una petición enviando nuestros propios [*headers http*](https://developer.mozilla.org/es/docs/Web/HTTP/Headers). Con [curl](https://curl.se) como agente de usuario por ejemplo:

```shell
curl -H "Mi-Propio-Header: lo-que-quiera" http://localhost:8080
```

Que dará como resultado:

```curl
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.64.1
> Accept: */*
> Mi-Propio-Header: lo-que-quiera
> 
< HTTP/1.1 200 OK
< Date: Mon, 08 Mar 2021 11:14:47 GMT
< Connection: keep-alive
< Keep-Alive: timeout=5
< Transfer-Encoding: chunked
< 
* Connection #0 to host localhost left intact
Hola mundo* Closing connection 0
```

### 4.2. Manipular en `Content-Type` de la respuesta

Supongamos que queremos retornar a cualquier petición hecha al servidor un objeto JSON. Como previamente se mencionaba podemos advertir al cliente el tipo de contenido que va a ser devuelto manipulando en *header* de *http* `Content-Type`, especificando un [*mimetype*](https://es.wikipedia.org/wiki/Multipurpose_Internet_Mail_Extensions).

[Como por ejemplo](./src/03-webserver-http-server-json.js) se realiza aquí:

```js
const http= require('http');

http.createServer( (request, response) => {

    const objeto = {
        id: 1,
        nombre: "Pedro",
    }

    response.writeHead(201, {'Content-Type': 'application/json'});
    response.write( JSON.stringify(objeto));
    response.end();
}).listen(8080);
```

Es destacable que el fichero debe ser texto plano en este caso (JSON es texto plano). Por ello debemos convertir el objeto JS en texto utilizando la librería `JSON`, concretamente con su método `stringify`.

### 4.3. Procesar las rutas y la información del cliente

Supongamos que el cliente va a pedir difentes recursos concatenando al host y al puerto un path. Si el path existe para nosotros, retornaremos un [código HTTP](https://es.wikipedia.org/wiki/Anexo:Códigos_de_estado_HTTP) `2xx`, en caso contrario un `404`.

En el [siguiente ejemplo](./src/03-webserver-http-server-csv.js), en función del método y del mensaje HTTP se realizan o devuelven diferentes acciones:

```js
const http= require('http');

http.createServer( (request, response) => {

    console.log(`${request.method} ${request.url}`);

    if(request.url === '/download_csv'){
        console.log('GET ', request.url);
        console.log("Hola");
        response.setHeader('Content-Disposition', 'attachment; filename=fichero.csv')

        response.writeHead(201, {'Content-Type': 'application/csv'});


        response.write( 'id, nombre\n');
        response.write( '1, Mateo\n');
        response.write( '2, Julia\n');
        response.end();
    }
    else if(request.url === '/alumnos' && request.method === 'GET'){
        
        response.writeHead(200, {'Content-Type': 'application/json'});
        const objeto = { id: 1, nombre: "Pedro"};
        response.write( JSON.stringify(objeto))
        response.end();
    }
    else if(request.url === '/alumnos'  && request.method === 'POST'){
        response.writeHead(201, {'Content-Type': 'application/json'});
        // console.log(request);
        request.on('data', (data) => {
            // Too much POST data, kill the connection!
            const cadena = String(data);
            console.log(`Recibo el cuerpo de la petición: ${cadena}`);
            // La parseo
            const [ param1, param2 ] = cadena.split('&');
            const nombre  = param1.split('=')[1];
            const apellidos = param2.split('=')[1];
            const objeto = { msg: "Alumno creado", alumno: { id: 3, nombre, apellidos } };
            response.write( JSON.stringify(objeto))
            response.end();
        });
    }
    else {
        response.writeHead(404, {'Content-Type': 'text/html'});
        response.write('<h1>404 Not Found :( </h1>')
        response.end();
    }

}).listen(8080);
```

Con curl podemos realizar diferentes peticiones para enviar datos, subir ficheros o hacer peticiones con diferentes métodos:

```shell
# Get XML:
curl -H "Accept: application/xml" -H "Content-Type: application/xml" -X GET http://localhost:8080 --verbose

# For posting data:
curl --data "param1=value1&param2=value2" -X GET http://localhost:8080 --verbose

# For file upload:
curl --form "fileupload=@filename.txt" http://localhost:8080 --verbose

# RESTful HTTTP Post:
curl -X POST -d @filename http://localhost:8080 --verbose

# For logging into a site (auth):
curl -d "username=admin&password=admin&submit=Login" --dump-header headers http://localhost:8080 --verbose
curl -L -b headers http://localhost:8080
```

## 5. Implementar un webserver con Nodejs y Express

Express es un framework para realizar Aplicaciones web. Según su [documentación oficial](https://expressjs.com), se describe como un framework rápido, *unopinionated*, minimalista para Nodejs.

Una puntualización sobre una duda surgida en [*stackoverflow*](https://stackoverflow.com/questions/802050/what-is-opinionated-software)

> *A menudo veo gente que dice que cierto software es "opinionated" o que Microsoft tiende a escribir marcos "unopinionated". ¿Qué significa esto realmente?*

En primer lugar, la traducción más cercana a *opinionated* sería [dogmático](https://dle.rae.es/dogma). Esto nos lleva a pensar que existe una forma correcta de hacer las cosas, de forma innegable y de carácter indescutible. Un *framework dogmático*, nos presentaría un marco que nos resolvería mucha de la complegidad frecuente en nuestros desarrollos, sin embargo nos propondía una forma muy poco flexible de adaptarlo a nuestro contexto del problema. Esto puede resultar que un determinado framework nos proporciones a muy bajo coste el 95% de nuestro desarrollo, pero adaptarlo a las necesidades específicas (ese 5% restante), puede suponer un considerable esfuerzo. El framework nos presenta un dogma de cómo debemos hacer las cosas, pero por otra parte, no es posible que un framework contemple todos los escenarios posibles.

En segunto lugar, la traducción más cercana a *unopinionated* sería nada o poco dogmático. Quizá podríamos traducirlo a flexible. O que al menos, existen varias formas de hacer correctamente las cosas. Un framework *poco intrusivo* en nuestra forma de trabajar, organizar las cosas.

Para comentazar con express sería recomendable hacer unas pruebas con su [*Getting started Guide*](https://expressjs.com/en/starter/installing.html).

Se destaca de esto:

- [Inicialización de un server](https://expressjs.com/en/starter/hello-world.html)
- [Hacer un *routing* básico](https://expressjs.com/en/starter/basic-routing.html)
- [Servir ficheros estáticos](https://expressjs.com/en/starter/static-files.html)

### Inicializar un Web Server con Express

Es tan básico como esto:

```node
const express = require('express')

const app = express()
const port = 8080
  
app.listen(port, () => {
    console.log(`Webserver app listening at http://localhost:${port}`)
})
```

### Routing básico con Express

Al código precedente le faltaría atender a los diferentes peticiones (*requests*) que provienen de un cliente web. Para ello hay que asociar, posibles rutas (*paths*) solicitados a *callbacks* para manejar las respuestas, en función del método de la petición (`GET`,`POST`,`PUT`,`DELETE`, `HEAD`, etc.):

```node
app.get("/", (req, res) => {
    res.send('Hello World!')
})

//Manejo 404. Nótese la utilización de wildcards
app.get("/*", function(req, res) {
    res.send('Recurso no encontrado!')
});
```

## Servir ficheros estáticos

Para hacerlo, tan sólo tendremos que usar un *middleware* de esta forma: `express.static(root, [options])`, como por ejemplo:

```node
// To use multiple static assets directories, call the express.static middleware function multiple times:
app.use(express.static('public'))
app.use(express.static('files'))
```

Ojo con el orden en el que se definen las rutas. Este orden puede implicar cambios en el enrutado.
