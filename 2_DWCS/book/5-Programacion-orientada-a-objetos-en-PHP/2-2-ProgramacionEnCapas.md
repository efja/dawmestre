# Programación en capas

En el [ejemplo anterior](2-1-CaracteristicasDeOrientacionAObjetosEnPHP.md#Ejemplo-de-POO-en-PHP), hemos programado una aplicación web sencilla utilizando programación orientada a objetos. Sin embargo, si observaste el resultado obtenido, habrás visto como en muchas ocasiones se mezcla el código propio de la lógica de la aplicación, con el código necesario para crear el interface web que se presenta a los usuarios.

Por ejemplo, tanto la clase `CestaCompra` como la clase `Producto`, cuyo objetivo debería ser implementar la lógica de la aplicación, tienen un método llamado muestra destinado a generar etiquetas HTML. E inversamente, en algunas páginas que deberían simplemente generar HTML, puedes encontrar código que forma parte de la lógica de la aplicación. Por ejemplo, en la página `productos.php`, el código para incluir un nuevo producto en la cesta de la compra se encuentra mezclado con las etiquetas HTML.

Existen varios métodos que permiten separar la lógica de presentación (en nuestro caso, la que genera las etiquetas HTML) de la lógica de negocio, donde se implementa la lógica propia de cada aplicación. El más extendido es el patrón de diseño [*Modelo – Vista – Controlador (MVC)*](https://es.wikipedia.org/wiki/Modelo–vista–controlador). Este patrón pretende dividir el código en tres partes, dedicando cada una a una función definida y diferenciada de las otras.

* **Modelo**. Es el encargado de manejar los datos propios de la aplicación. Debe proveer mecanismos para obtener y modificar la información del mismo. Si la aplicación utiliza algún tipo de almacenamiento para su información (como un SGBD), tendrá que encargarse de almacenarla y recuperarla.

* **Vista**. Es la parte del modelo que se encarga de la interacción con el usuario. En esta parte se encuentra el código necesario para generar el interface de usuario (en nuestro caso en HTML), según la información obtenida del modelo.

* **Controlador**. En este módulo se decide qué se ha de hacer, en función de las acciones del usuario con su interface. Con esta información, interactúa con el modelo para indicarle las acciones a realizar y, según el resultado obtenido, envía a la vista las instrucciones necesarias para generar el nuevo interface.

La gran ventaja de este patrón de programación es que genera código muy estructurado, fácil de comprender y de mantener. En la web puedes encontrar algunos ejemplos de implementación del modelo MVC en PHP. Échale un vistazo al siguiente artículo sobre MVC en PHP.

Aunque puedes programar utilizando MVC por tu cuenta, es más habitual utilizar el patrón MVC en conjunción con un framework o marco de desarrollo. Existen numerosos frameworks disponibles en PHP, muchos de los cuales incluyen soporte para MVC. En esta unidad no profundizaremos en la utilización de un framework específico, pero existen numerosos recursos con información en Internet, incluyendo la página phpwebframeworks.com, dedicada exclusivamente a ellos.


## Separación de la lógica de negocio

Otros mecanismos disponibles en PHP, menos complejos que la utilización del patrón MVC, y que también permiten la separación de la lógica de presentación y la lógica de negocio, son los llamados motores de plantillas (*template engines*).

**Un motor de plantillas web** es una aplicación que genera una página web a partir de un fichero con la información de presentación (denominado plantilla o template, que viene a ser similar a la vista en el patrón MVC) y otro con la lógica interna de la aplicación (similar al modelo de MVC). De esta forma, es sencillo dividir el trabajo de programación de una aplicación web en dos perfiles: un programador, que debe conocer el lenguaje de programación en el que se implementará la lógica de la aplicación (en nuestro caso PHP), y un diseñador, que se encargará de elaborar las plantillas, (en el caso de la web básicamente en HTML, aunque como veremos 
la lógica de presentación que se incorpore utilizará un lenguaje propio).

En PHP existen varios motores de plantillas con diferentes características. Quizás el más conocido es [*Smarty*](https://www.smarty.net), de código abierto y disponible bajo licencia LGPL.

Entre las **características** de Smarty cabe destacar:

* Permite la inclusión en las plantillas de una lógica de presentación compleja.

* Acelera la generación de la página web resultante. Uno de los problemas de los motores de plantillas es que su utilización influye negativamente en el rendimiento. Smarty convierte internamente las plantillas a guiones PHP equivalentes, y posibilita el almacenamiento del resultado obtenido en memoria temporal.

* Al ser usado por una amplia comunidad de desarrolladores, existen multitud de ejemplos y foros para la resolución de los problemas que te vayas encontrando al utilizarlo.

Para instalar *Smarty*, debes descargar la última versión desde su sitio web, y seguir los siguientes pasos:

1. Copiar los archivos de la librería de Smarty (el directorio libs que obtienes tras descomprimir el fichero que has descargado) en tu sistema; por ejemplo en la carpeta `/usr/share/smarty`.

	![](img/DWES05_CONT_R20_Instalar_Smarty.jpg)

2. Modificar el fichero php.ini para que se incluya en la variable `include_path` de PHP la ruta en la que acabas de instalar Smarty, y reiniciar Apache para aplicar los cambios.

	![](img/DWES05_CONT_R21_Smarty_include_path.jpg)

3. Crear la estructura de directorios necesaria para Smarty. Concretamente debes crear cuatro directorios, con nombres `templates`, `templates_c`, `configs` y `cache`. Es conveniente que estén ubicados en un lugar no accesible por el servidor web, y en una ubicación distinta para cada aplicación web que programes. Por ejemplo, puedes crear en la raíz de tu servidor una carpeta llamada `smarty`, y bajo ella otra con el nombre de cada aplicación (por ejemplo, `stienda`), que será la que contendrá las carpetas.


### Separación de la lógica de negocio

Para utilizar *Smarty*, simplemente tienes que añadir a tus páginas PHP el fichero `Smarty.class.php`, que es donde está declarada la clase `Smarty`. Después debes instanciar un nuevo objeto de esa clase, y configurar la ruta a cada uno de los directorios que acabas de crear.

```php
require_once('Smarty.class.php');
$smarty = new Smarty;
$smarty->template_dir = '/web/smarty/stienda/templates/';
$smarty->compile_dir  = '/web/smarty/stienda/templates_c/';
$smarty->config_dir   = '/web/smarty/stienda/configs/'; 
$smarty->cache_dir    = '/web/smarty/stienda/cache/';
```

Cuando quieras que algún valor o variable obtenido en tus páginas, esté disponible para mostrarlo en las páginas web a través de una plantilla, tienes que usar el método `assign`, indicando el nombre del identificador. Puedes utilizar `assign` con variables de cualquier tipo, incluyendo arrays asociativos y objetos.

```php
$smarty->assign('usuario', $_SESSION['usuario']);
$smarty->assign('productoscesta', $cesta->get_productos());
$smarty->assign('coste', $cesta->get_coste());
```

Y una vez que hayas preparado los identificadores que se usarán en la plantilla, deberás mostrarla utilizando el método `display`.

```php
$smarty->display('cesta.tpl');
```

Así, por ejemplo, al utilizar Smarty en la página p`roductos.php` de la [tienda online](2-1-CaracteristicasDeOrientacionAObjetosEnPHP.md#Ejemplo-de-POO-en-PHP), puedes obtener algo como:

```php
require_once('include/DB.php');
require_once('include/CestaCompra.php');
require_once('Smarty.class.php');

// Recuperamos la información de la sesión session_start();
// Y comprobamos que el usuario se haya autentificado
if (!isset($_SESSION['usuario'])) {
	die("Error - debe <a href='login.php'>identificarse</a>.<br />"); 
}

// Recuperamos la cesta de la compra
$cesta = CestaCompra::carga_cesta();

// Cargamos la librería de Smarty
$smarty = new Smarty;
$smarty->template_dir = '/web/smarty/stienda/templates/';
$smarty->compile_dir = '/web/smarty/stienda/templates_c/';
$smarty->config_dir = '/web/smarty/stienda/configs/';
$smarty->cache_dir = '/web/smarty/stienda/cache/';

// Comprobamos si se ha enviado el formulario de vaciar la cesta 
if (isset($_POST['vaciar'])) {
	unset($_SESSION['cesta']);
	$cesta = new CestaCompra();
}

// Comprobamos si se quiere añadir un producto a la cesta
if (isset($_POST['enviar'])) {
	$cesta->nuevo_articulo($_POST['cod']);
	$cesta->guarda_cesta(); 
}

// Ponemos a disposición de la plantilla los datos necesarios

$smarty->assign('usuario', $_SESSION['usuario']); 
$smarty->assign('productos', DB::obtieneProductos());
$smarty->assign('productoscesta', $cesta->get_productos());

// Mostramos la plantilla
$smarty->display('productos.tpl');
```


## Generación del interface de usuario

Las plantillas de Smarty son ficheros con extensión tpl, en los que puedes incluir prácticamente cualquier contenido propio de una página web. Además, en el medio intercalarás delimitadores para indicar la inclusión de datos y de lógica propia de la presentación.

![Plantillas_Smarty](img/DWES05_CONT_R24_Plantillas_Smarty.jpg)

Los delimitadores de Smarty son llaves. De los distintos elementos que puedes incluir entre las llaves están:

* **Comentarios**. Van encerrados entre asteriscos.
	
	```smarty
	{* Este es un comentario de plantilla en Smarty *}
	```

* **Variables**. Se incluye simplemente su nombre, precedido por el símbolo $. También se pueden especificar modificadores, separándolos de la variable por una barra vertical. Existen varios modificadores para, por ejemplo, dar formato a una fecha (date_format) o mostrar un contenido predeterminado si la variable está vacía (default).

	```smarty
	{$producto->codigo}
	```

* **Estructura de procesamiento condicional**: `if`, `elseif`, `else`. Permite usar condiciones, de forma similar a PHP, para decidir si se procesa o no cierto contenido.
	
	```smarty
	{if empty($productoscesta)}
		<p>Cesta vacía</p>
	{else} 
		...
	{/if}
	```

* **Bucles**: `foreach`. Son muy útiles para mostrar varios elementos, por ejemplo en una tabla. Deberás indicar al menos con from el array en el que están los elementos, y con item la variable a la que se le irán asignado los elementos en cada iteración.

	```smarty
	{foreach from=$productoscesta item=producto} 
		<p>{$producto->codigo}</p>
	{/foreach}
	```

* **Inclusión de otras plantillas**. Smarty permite descomponer una plantilla compleja en trozos más pequeños y almacenarlos en otras plantillas, que se incluirán en la actual utilizando la sentencia include.

	```smarty
	<div id="cesta">
		{include file="productoscesta.tpl"}
	</div>
	<div id="productos">
		{include file="listaproductos.tpl"}
	</div>
	```

>  Las que acabas de ver son una pequeña parte de las funcionalidades que ofrece Smarty. En su página web, puedes acceder a [la documentación completa](https://www.smarty.net/docsv2/es/). En el momento de escribir estas líneas, la última versión disponible en español de la documentación era la correspondiente a la versión 2.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)