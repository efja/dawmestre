# Readme Ejemplo tiendaOnLine

Encontrarás información más detallada sobre este ejemplo en el apartado ['Ejemplo de POO en PHP' del capítulo 5'](../../2-1-CaracteristicasDeOrientacionAObjetosEnPHP.md#Ejemplo-de-POO-en-PHP)

## Ejecución de la App


Puedes Comprobar su funcionamiento usando el servidor http interno de PHP. Para ello, sitúate en la base de este repositorio ejecuta la siguiente instrucción:

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t book/5-Programacion-orientada-a-objetos-en-PHP/doc/tiendaOnLine
```