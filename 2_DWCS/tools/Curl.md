# Curl

[Curl](https://curl.haxx.se) es una herramienta y librería para transferir datos con URLs.


## Para qué se usa Curl

Curl se usa en líneas de comando o scripts para transferir datos. También se utiliza en automóviles, televisores, enrutadores impresoras, equipos de audio, teléfonos móviles, tabletas, decodificadores, reproductores multimedia y es la columna vertebral de transferencia de Internet para miles de aplicaciones de software que afectan a miles de millones de seres humanos a diario.

## Qué soporta Curl

Soporta...

DICT, FILE, FTP, FTPS, Gopher, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, MQTT, POP3, POP3S, RTMP, RTMPS, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, Telnet and TFTP. 

Curl soporta SSL certificates, HTTP POST, HTTP PUT, FTP uploading, HTTP form based upload, proxies, HTTP/2, HTTP/3, cookies, user+password authentication (Basic, Plain, Digest, CRAM-MD5, NTLM, Negotiate and Kerberos), file transfer resume, proxy tunneling and more.

## Quien hizo Curl?

Curl es un software gratuito y de código abierto y existe gracias a miles de colaboradores y patrocinadores. El proyecto curl sigue las mejores prácticas de código abierto bien establecidas.

## Referencias externas

* [Página ofical](https://curl.haxx.se)
* [Everything curl](https://ec.haxx.se)
* [Inicio rápido](https://curl.haxx.se/docs/manual.html)
* [cURL vs wget](https://maslinux.es/curl-vs-wget-sus-diferencias-uso-y-cual-deberias-usar/)
