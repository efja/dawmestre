# Forms y procesamiento de envíos

Otra de las características más potentes de PHP es la [forma de gestionar formularios HTML](https://www.php.net/manual/es/tutorial.forms.php). El concepto básico que es importante entender es que cualquier elemento de un formulario estará disponible automáticamente en sus scripts de PHP. 

Ejemplo sencillo de creación de un form en HTML

```html
<form action="accion.php" method="post">
    <p>Su nombre: <input type="text" name="nombre" /></p>
    <p>Su edad: <input type="text" name="edad" /></p>
    <p><input type="submit" /></p>
</form>
```

No hay nada especial en este formulario. Es solamente un formulario HTML sin ninguna clase de etiqueta especial. Cuando el usuario rellena este formulario y oprime el botón de envío, se llama a la página accion.php. En este fichero se podría escribir algo así:

```php
Hola <?php echo htmlspecialchars($_POST['nombre']); ?>.
Usted tiene <?php echo (int)$_POST['edad']; ?> años.
```

### Aspectos reseñables

* Tener precaución con vulnerabilidades típicas como la [inyección de código](https://es.wikipedia.org/wiki/Inyecci%C3%B3n_de_c%C3%B3digo). [`htmlspecialchars()`](https://www.php.net/manual/es/function.htmlspecialchars.php) garantiza que cualquier carácter que sea especial en html se codifique adecuadamente, de manera que nadie pueda inyectar etiquetas HTML o Javascript en la página.

* En PHP, también puedes tratar con entradas de `XForms`; aunque probablemente al principio te sientas cómodo con los formularios de HTML, los cuales están ampliamente respaldados.

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/10-forms.php](http://localhost:9090/10-forms.php)

### [Volver al índice](README.md#Conceptos-basicos)