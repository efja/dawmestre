# Programación en PHP

[PHP](https://www.php.net/manual/es/preface.php), acrónimo de "PHP: **Hypertext Preprocessor**", es un lenguaje de 'scripting' de propósito general y de código abierto que está especialmente pensado para el desarrollo web y que puede ser embebido en páginas HTML. Su sintaxis recurre a C, Java y Perl, siendo así sencillo de aprender. El objetivo principal de este lenguaje es permitir a los desarrolladores web escribir dinámica y rápidamente páginas web generadas; aunque se puede hacer mucho más con PHP.

```php
<!DOCTYPE html>
<html>
    <head>
        <title>Ejemplo</title>
    </head>
    <body>

        <?php
            echo "¡Hola, soy un script de PHP!";
        ?>

    </body>
</html>
```

## ¿Qué puede hacer PHP?

En la [siguiente referencia](https://www.php.net/manual/es/intro-whatcando.php) encontrarás más detalles pero principalmente, estos son los tres campos principales donse se usan scripts de PHP:

* Scripts del lado del servidor

* Scripts desde la línea de comandos

* Escribir aplicaciones de escritorio


## Getting started...

En la documentación de PHP, obtendrás [un sencillo tutorial](https://www.php.net/manual/es/tutorial.php) para iniciarse en la programación de PHP.

En este se te indicará [qué necesitas](https://www.php.net/manual/es/tutorial.requirements.php) y cómo pueder genera tu primera página dinámica con PHP. Tienes indicaciones sobre la [instalación y configuración](https://www.php.net/manual/es/install.php) en cada plataforma también en la documentación oficial.

## Algunos componentes a conocer...

SAPI significa "API de servidor" (y API significa "Interfaz de programación de aplicaciones"). Es el mecanismo que controla la interacción entre el "mundo exterior" y el motor PHP / Zend.

Las SAPIs disponibles en PHP son:

* **PHP-CGI**, es un protocolo para interconectar programas interactivos con un servidor web.

* **PHP-FastCGI**, FastCGI es una variación de la ya conocida Common Gateway Interface (CGI ó Interfaz Común de Entrada), permitiéndole a un servidor atender más peticiones a la vez.

* **PHP-FPM**, es la implementación alternativa más popular de PHP FastCGI, que cuenta con características adicionales realmente útiles para sitios web de alto tráfico

* **PHP-CLI**, (Server API) su principal objetivo es el desarrollo de aplicaciones de consola con PHP.

Vale la pena aclarar que CLI y CGI son SAPIs [diferentes](https://www.php.net/manual/es/features.commandline.differences.php) pese a que comparten la mayoría de características.

La siguiente función `php_sapi_name()` retorna el tipo de interface entre el servidor http y PHP.

**El módulo del Servidor HTTP para Apache** (`mod_php`) consiste en que el intérprete de PHP está "*integrado*" dentro del proceso de Apache: no hay un proceso de PHP externo, lo que significa que Apache y PHP pueden comunicarse mejor.

## Interactuar con el intérprete PHP

Podemos operar con PHP desde la línea de comandos. Las [opciones de línea de comandos](https://www.php.net/manual/es/features.commandline.options.php) las obtendrás de la siguiente forma: `php -h`

**Podemos comunicarnos de forma interactica** con el intérprete de PHP utilizando el siente comando:

```shell
$ php -a
```

De forma que podamos ejecutar un `"Hola mundo"` desde terminal:

```
Interactive mode enabled

php > echo("Hola Mundo\n");
Hola Mundo
php > 
```

**O podemos decirle a PHP que ejecute un determinado fichero** (ambas correctas):

```shell
$ php mi_script.php

$ php -f mi_script.php
```

También es posible **pasar el código PHP directamente a la línea de comandos**:

```shell
php -r 'print_r(get_defined_constants());'
```

**Los argumentos** estarán disponibles en el array global `$argv`

Otras opciones:

~~~Shell
# Show compiled in modules
php -m

# PHP information and configuration
php -i

# Muestra los ficheros de configuración
php --ini
~~~

## Conceptos básicos

1. [Primera página web](01-basico.md)
2. [Tipos y Variables en PHP](02-variables.md)
3. [Operadores aritméticos](03-aritmetica.md)
4. [Operadores de comparación](04-comparacion.md)
5. [Operadores lógicos](05-logica.md)
6. [Estructuras de Control en PHP](06-condicional.md)
7. [Bucles en PHP](07-bucles.md)
8. [Arrays en PHP](08-arrays.md)
9. [Funciones en PHP](09-funciones.md)
10. [Manejo de Forms](10-forms.md)
11. [Manejo de Sesiones](sesiones.md)

## Ver también

* [Utilizar el servidor web interno de PHP](https://www.php.net/manual/es/features.commandline.webserver.php)
    
    * Accediendo a la CLI del servidor web **desde máquinas remotas**: Puede hacer que el servidor web sea accesible en el puerto 8000 a cualquier interfaz con: `$ php -S 0.0.0.0:8000`

