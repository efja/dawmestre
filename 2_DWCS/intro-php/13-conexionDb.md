# Información general de los [controladores de MySQL para PHP](https://www.php.net/manual/es/mysql.php)

Dependiendo de la versión de PHP, existen dos o tres API de PHP para acceder a las bases dedatos de MySQL. Los usuarios de PHP pueden elegir entre las siguientes extensiones:

* [mysql (nativa y obsoleta, sólo disponible para PHP 5)]()
* [mysqli](https://www.php.net/manual/es/book.mysqli.php)
* [PDO_MYSQL](https://www.php.net/manual/es/ref.pdo-mysql.php)

Antes es importante aclarar qué es esta [terminología](https://www.php.net/manual/es/mysqlinfo.terminology.php): *API*, *conector*, *controlador* y *extensión*.

### ¿Qué es una API?

Una Interfaz de Programación de Aplicaciones (o API de sus siglas en inglés), **define las clases, métodos, funciones y variables que una aplicación necesita llamar para realizar una tarea**. En el caso de aplicaciones de PHP que necesiten comunicarse con bases de datos, *las API necesarias normalmente son expuestas mediante extensiones de PHP*.

Las API pueden ser **procedimentales** u **orientadas a objetos**. Con una API procedimental se llaman a funciones para realizar tareas, con una API orientada a objetos se instancian clases y luego se llaman a métodos de los objetos resultantes. De las dos, la última normalmente es la interfaz preferida debido a que es más moderna y conduce a un código mejor organizado.

Al escribir aplicaciones de PHP que necesitan conectarse a un servidor de MySQL, **existen varias opciones de API disponibles**.

### ¿Qué es un conector?

En la documentación de MySQL, el término conector se refiere a una **pieza de software que permite a las aplicaciones conectarse con el servidor de bases de datos** de MySQL. MySQL proporciona conectores para muchos lenguajes, incluido PHP.

### ¿Qué es un controlador?

Un controlador (o *driver*) es una **pieza de software diseñada para la comunicación con un tipo específico de servidor de bases de datos**. El controlador también llama a una biblioteca, como la Biblioteca Cliente de MySQL o el Controlador Nativo de MySQL. Estas bibliotecas implementan el protocolo de bajo nivel usado para comunicarse con el servidor de bases de datos de MySQL.

Por poner un ejemplo, la capa de abstracción de bases de datos **Objetos de Datos de PHP (PDO) pueden usar UNO de los VARIOS controladores específicos de bases de datos**. Uno de los controladores disponibles es el controlador de MySQL de PDO, que permite hacer de interfaz con el servidor MySQL.

**A veces se emplean los términos conector y controlador intercambiablemente**, y esto puede ser confuso. El la documentación relacionada con MySQL, el término controlador está reservado para el software que proporciona la parte específica de bases de datos de un paquete conector.

### ¿Qué es una extensión?

En la documentación de PHP se encontrará con otro término: *extensión*. **El código de PHP consiste en un núcleo**, con extensiones opcionales para la funcionalidad del núcleo. Las extensiones relacionadas con MySQL de PHP, como la extensión *mysqli*, y la extensión *mysql*, están implementadas con el marco de extensiones de PHP.

Una extensión **típicamente expone una API** al programador de PHP para así utilizar sus prestaciones programáticamente. Sin embargo, **algunas extensiones que usan el marco de extensiones de PHP no exponen una API** al programador de PHP.

La extensión del controlador de MySQL de PDO, por ejemplo, no expone una API al programador de PHP, pero proporciona una interfaz para la capa superior a PDO.

Los términos API y extensión no deberían tomarse como si fuesen la misma cosa; una extensión puede no exponer necesariamente una API al programador.