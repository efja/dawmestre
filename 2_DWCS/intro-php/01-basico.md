# Tu primera página con PHP

Comienza por crear un fichero llamado `01-basico.php` y póngalo en el directorio raíz de su servidor web (`DOCUMENT_ROOT`) con el siguiente contenido:

```php
<html>
    <head>
        <title>Exemplo de PHP</title>
        <style>
            .entrada-blog {background-color:#222}
            h1 {color:#aaa;}
            .entrada-blog p {color:#ccc;}
        </style>
    </head>
    <body>

        Parte de HTML estático.
        <br><br>

        <?php
            //Isto é un comentario nunha liña
            echo "Parte de HTML dinámico generado con PHP<br>";
            
            /*Isto é un comentario
             en varias liñas*/
            
            for($i=0;$i<10;$i++) {
                echo("<div class=entrada-blog>");
                    echo("<h1>Titulo ".$i."</h1>");
                    echo("<p>Cuerpo</p>");
                echo("</div>");
            }
        ?>
    </body>
</html>
```

Comprueba como se integra el código estático con el código que generará PHP de forma dinámica.

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib
```

Verás que se incicia un Servidor Web:

```
PHP 7.3.11 Development Server started at Wed Oct  7 13:35:58 2020
Listening on http://localhost:8080
Document root is [...]/edu-wiki-dwcs/intro-php/lib
Press Ctrl-C to quit.
```

Y accede para comprobarlo a la url:

```
http://localhost:9090/01-basico.php
```

## Consideraciones a tener en cuenta

* [Etiquetas de apertura y cierre de un script PHP](https://www.php.net/manual/es/language.basic-syntax.phptags.php)

    * **Evitar** la etiqueta de **apertura abreviada** `<?`

    * Si un fichero contiene solamente código de PHP, es preferible **omitir la etiqueta de cierre de PHP al final del mismo**. Así se previene la adición de espacios en blanco o nuevas líneas accidentales después de la etiqueta de cierre, lo cual causaría efectos no deseados debido a que PHP comenzará la salida del búfer cuando no había intención por parte del programador de enviar ninguna salida en ese punto del script.

    * Para imprimir bloques de texto grandes, es más eficiente abandonar el modo intérprete de PHP que enviar todo el texto a través de `echo` o `print`. Para ello se puede usar la [Salida avanzada usando condiciones](https://www.php.net/manual/es/language.basic-syntax.phpmode.php)

* La [separación de instrucciones](https://www.php.net/manual/es/language.basic-syntax.instruction-separation.php) se hace con `;` y se puede omitir este en la última instrucción del bloque de PHP

* [La sintaxis de los comentarios](https://www.php.net/manual/es/language.basic-syntax.comments.php) es similar a la de Java, aunque también se puede comentar una línea como en shell script con el carácter `#`


### [Volver al índice](README.md#Conceptos-basicos)