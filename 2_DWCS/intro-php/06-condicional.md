# Estructuras de Control en PHP

Considerando el paradigma de [programación estructurada](https://es.wikipedia.org/wiki/Programación_estructurada), es necesario conocer las estructuras de control en PHP como estructura básica.

### Un ejemplo. IF y IF-ELSE

```php
//Declaración en pantalla e impresión de la operación. ¿Podrías mejorarlo?
$a = 8;
echo "<p>a = 8</p>";
$b = 3;
echo "<p>b = 3</p>";


if ($a < $b)
{
    //Esta instrucción se ejecutará siempre que a sea menor estricto que b
    echo "<p>'a' É menor que 'b'</p>";
}

if ($a == $b)
{
    //Esta instrucción se ejecutará si a es igual a b
    echo "<p>'a' É igual a 'b'</p>";
}
else
{
    //Esta instrucción se ejecutará si la condición del if que la precede, no se cumple
    echo "'a' É maior que 'b' o 'b' É maior que 'a'</p>";
} 
```

### Otro ejemplo. Operador ternario

```php
$a = 5;
$b = 7;

// Por exemplo
if ($a>$b)
    $resultado= "A é máis grande que B";
else
    $resultado= "B é máis grande que A";

// Otra alternativa a la escritura de un if, con asignación de variable asociada. Importante entenderlo (ternaria)
// Podería quedar así: 
$resultado = ($a>$b) ? "A é máis grande que B":"B é máis grande que A";
echo '<br>'.$resultado;
```

### Otro ejemplo. SWITCH

```php
$posicion = "arriba";

switch($posicion) {
    case "arriba":   // Bloque 1
        echo "<p>A variable contén";
        echo " o valor arriba</p>";
        //El break interrumple la evaluación del resto de sentencias case. Importante entenderlo
        break;
    case "abaixo":   // Bloque 2
        echo "<p>A variable contén";
        echo " o valor abaixo</p>";
        break;
    default:   // Bloque 3
        echo "<p>A variable contén outro valor";
        echo " distinto de arriba e abaixo</p>";
} 
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/06-condicional.php](http://localhost:9090/06-condicional.php)

### [Volver al índice](README.md#Conceptos-basicos)