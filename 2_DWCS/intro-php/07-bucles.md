# Bucles en PHP

Considerando el paradigma de [programación estructurada](https://es.wikipedia.org/wiki/Programación_estructurada), es necesario conocer las iteraciones en PHP como estructura básica.

### Ejemplo WHILE

[Los bucles while](https://www.php.net/manual/es/control-structures.while.php) son el tipo más sencillo de bucle en PHP. Se comportan igual que su contrapartida en C.

```php
//Declaramos una variable asignándole el valor 0
$i=0; 

//Se ejecutará lo contenido entre los {} del bucle while, mientras se cumpla la condicion de que i sea menor que 10
while ($i<10) 
{ 
    echo "O valor de i é " . $i . "<br>"; 
    $i++; 
}
```

Reflexiona:

* ¿Qué hará este código tal y como está?
* ¿Qué pasaría si iniciasemos la `i` a `10`? ¿Entraría en el `while`?


### Ejemplo DO-WHILE

[Los bucles `do-while`](https://www.php.net/manual/es/control-structures.do.while.php) son muy similares a los bucles `while`, excepto que la expresión verdadera es verificada al final de cada iteración en lugar que al principio.

```php
$i = 1;

do {
    echo $i,'<br/>';
    $i++;
} while ($i <=10);
```

Reflexiona:

* ¿Qué hará este código tal y como está?
* ¿Qué pasaría si iniciasemos la `i` a `10`? ¿Entraría en el `while`?

### Ejemplo FOR

[Los bucles for](https://www.php.net/manual/es/control-structures.for.php) son los más complejos en PHP. Se comportan como sus homólogos en C

```php
//Un bucle for, véase la composición de las 3 sentencias dentro de ()
for($i=0 ; $i<10 ; $i++) 
{ 
    echo "O valor de i é " . $i . "<br />"; 
}
```

### Ejemplo y sintaxis `foreach`

[El constructor foreach](https://www.php.net/manual/es/control-structures.foreach.php) proporciona un modo sencillo de iterar sobre arrays. foreach funciona sólo sobre arrays y objetos, y emitirá un error al intentar usarlo con una variable de un tipo diferente de datos o una variable no inicializada. Existen dos sintaxis:

```php
foreach (expresión_array as $valor)
    sentencias
foreach (expresión_array as $clave => $valor)
    sentencias
```

### Ejemplo GOTO

Está bien conocer el precedente de la [programación estructurada](https://es.wikipedia.org/wiki/Programación_estructurada) que es precisamente la programación desestructurada caracterizada por el uso de la sentencia [`goto`](https://en.wikipedia.org/wiki/Goto)

[PHP también soporta](https://www.php.net/manual/es/control-structures.goto.php) esta sentencia:

```php
$z = 1;
goto salto;
$z++; //Esta sentencia no se ejecuta
salto:
echo "El valor de z = $z";
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/07-bucles.php](http://localhost:9090/07-bucles.php)

### [Volver al índice](README.md#Conceptos-basicos)