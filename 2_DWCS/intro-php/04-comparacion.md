# Operadores de comparación

Los [operadores de comparación](https://www.php.net/manual/es/language.operators.comparison.php#language.operators.comparison), como su nombre lo indica, permiten comparar dos valores.

## Ejemplos

```php
//Declaración de variables
$a = 8;
$b = 3;
$c = 3;

echo $a == $b;
echo $a != $b; 
echo $a > $b; 
echo $a >= $b;
echo $c < $b;
echo $b <= $c;
echo $d === $c;
```

## Particularidades

A partir de la versión `7.0` de php tenemos disponibles estos operadores

### Operador nave espacial `<=>`

```php
// Integers
echo 1 <=> 1; // 0
echo 1 <=> 2; // -1
echo 2 <=> 1; // 1

// Strings
echo "a" <=> "a"; // 0
echo "a" <=> "b"; // -1
echo "b" <=> "a"; // 1

// Arrays
echo [] <=> []; // 0
echo [1, 2, 3] <=> [1, 2, 3]; // 0
echo [1, 2, 3] <=> []; // 1
echo [1, 2, 3] <=> [1, 2, 1]; // 1
echo [1, 2, 3] <=> [1, 2, 4]; // -1
```

### Fusión de Null

El primer operando de izquierda a derecha que exista y no sea `NULL`. `NULL` si no hay valores definidos y no son `NULL`. 

```php
$x = 5;
$y = NULL;
$z = 7;

echo $x ?? $y ?? $z; // 5
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/04-comparacion.php](http://localhost:9090/04-comparacion.php)

### [Volver al índice](README.md#Conceptos-basicos)