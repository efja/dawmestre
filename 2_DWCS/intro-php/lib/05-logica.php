<?php include "cabecera.html" ?>

<pre class="parte">
    *******************************************************
    * Declarando variables                                *
    *******************************************************
    $a = 8;
    $b = 3;
    $c = 3;
</pre>

<?php
    // Declaración de variables
    $a = 8;
    $b = 3;
    $c = 3;
?>

<pre class="parte">
    // ¿Que imprime? ¿Que tipo de dato es?
    echo ($a == $b) && ($c > $b);
</pre>
<div class="script-php">
    <?php echo ($a == $b) && ($c > $b) ?> 
</div>

<pre class="parte">
    // ¿Que imprime? ¿Que tipo de dato es?
    echo ($a == $b) || ($b == $c);
</pre>
<div class="script-php">
    <?php echo ($a == $b) || ($b == $c) ?> 
</div>

<pre class="parte">
    // ¿Que imprime? ¿Que tipo de dato es?
    echo !($b <= $c);
</pre>
<div class="script-php">
    <?php echo !($b <= $c) ?> 
</div>