<?php include "cabecera.html" ?>

<pre class="parte">
    *******************************************************
    * PRIMER EJEMPLO WHILE                                *
    *******************************************************
    $i=0; 
   
   //Se ejecutará lo contenido entre los {} del bucle while, mientras se cumpla la condicion de que i sea menor que 10
   while ($i<10) 
   { 
      echo "O valor de i é " . $i; 
      $i++; 
   }
</pre>

<div class="script-php">
    <?php
        $i=0; 

        //Se ejecutará lo contenido entre los {} del bucle while, mientras se cumpla la condicion de que i sea menor que 10
        while ($i<10) 
        { 
            echo "O valor de i é " . $i . "<br>"; 
            $i++; 
        }
    ?>
</div>

<pre class="parte">
    *******************************************************
    * SEGUNDO EJEMPLO DO-WHILE                                *
    *******************************************************
    $i=0; 
   
   //Se ejecutará lo contenido entre los {} del bucle while, mientras se cumpla la condicion de que i sea menor que 10
   while ($i<10) 
   { 
      echo "O valor de i é " . $i; 
      $i++; 
   }
</pre>

<div class="script-php">
    <?php
        $i=0; 

        do {
            echo $i,'<br/>';
            $i++;
        } while ($i <=10);
    ?>
</div>

<pre class="parte">
    *******************************************************
    * TERCER EJEMPLO FOR                                *
    *******************************************************
	for($i=0 ; $i<10 ; $i++) 
   	{ 
    	echo "O valor de i é " . $i . "<br />"; 
   	}
</pre>

<div class="script-php">
    <?php
        for($i=0 ; $i<10 ; $i++) 
        { 
            echo "O valor de i é " . $i . "<br />"; 
        }
    ?>
</div>

<pre class="parte">
    *******************************************************
    * CUARTO EJEMPLO GOTO                                *
    *******************************************************
    $z = 1;
    goto salto;
    $z++; //Esta sentencia no se ejecuta
    salto:
    echo "El valor de z = $z";
</pre>

<div class="script-php">
    <?php
        $z = 1;
        goto salto;
        $z++; //Esta sentencia no se ejecuta
        salto:
        echo "El valor de z = $z";
    ?>
</div>