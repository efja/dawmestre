<?php include "cabecera.html" ?>

<pre class="parte">

    //Un array nos permite asignar más de un valor en una sola variable

    //Poderemos crear un array asignando sus valores de forma rápida, utilizando los corchetes:
    $datos= ['casa','coche','gato'];

    // Podemos crear un array con diferentes valores, indexados por una clave (como si fuera un índice en un libro)
    // clave puede ser un ''integer'' o ''string'' 
    // valor puede ser cualquier valor
    $datos= [
        'propietario' => 'Antonio',
        'domicilio' => 'Santiago de Compostela',
        'edad' => 45
    ];

    // Esta matriz é a mesma que ...
    $datos = array(5 => 43, 32, 56, 'b' => 12, 60);
    echo "{$datos[5]}"; // Amosará 43
    echo "{$datos[6]}"; // Amosará 32
    echo "{$datos[7]}";// Amosará 56
    echo "{$datos[8]}";// Amosará 60
    echo "{$datos['b']}"; // Amosará 12

    // ...esta outra matriz. 
    array(5 => 43, "b" => 12, 7 => 56, 6 => 32, 60);
    echo "{$datos[5]}"; // Amosará 43
    echo "{$datos[6]}"; // Amosará 32
    echo "{$datos[7]}";// Amosará 56
    echo "{$datos[8]}";// Amosará 60
    echo "{$datos['b']}"; // Amosará 12


    //Si declaramos el siguiente array...
    $produtos = array("Azúcar", "Aceite", "Arroz");

    //¿Cómo listarías todos los productos? Investiga...
    //¿Soportaría la forma que has deducido un cambio que supone introducir un nuevo elemento, o tendrías que hacer alguna modificación?
</pre>

<div class="script-php">
    <?php

        //Un array nos permite asignar más de un valor en una sola variable

        //Poderemos crear un array asignando sus valores de forma rápida, utilizando los corchetes:
        $datos= ['casa','coche','gato'];
        
        // Podemos crear un array con diferentes valores, indexados por una clave (como si fuera un índice en un libro)
        // clave puede ser un ''integer'' o ''string'' 
        // valor puede ser cualquier valor
        $datos= [
            'propietario' => 'Antonio',
            'domicilio' => 'Santiago de Compostela',
            'edad' => 45
        ];
        
        // Esta matriz é a mesma que ...
        $datos = array(5 => 43, 32, 56, 'b' => 12, 60);
        echo "{$datos[5]}"; // Amosará 43
        echo "<br/>{$datos[6]}"; // Amosará 32
        echo "<br/>{$datos[7]}";// Amosará 56
        echo "<br/>{$datos[8]}";// Amosará 60
        echo "<br/>{$datos['b']}<hr/>"; // Amosará 12
    
        // ...esta outra matriz. 
        array(5 => 43, "b" => 12, 7 => 56, 6 => 32, 60);
        echo "{$datos[5]}"; // Amosará 43
        echo "<br/>{$datos[6]}"; // Amosará 32
        echo "<br/>{$datos[7]}";// Amosará 56
        echo "<br/>{$datos[8]}";// Amosará 60
        echo "<br/>{$datos['b']}"; // Amosará 12
        
    
        //Si declaramos el siguiente array...
        $produtos = array("Azúcar", "Aceite", "Arroz");
        
        //¿Cómo listarías todos los productos? Investiga...
        //¿Soportaría la forma que has deducido un cambio que supone introducir un nuevo elemento, o tendrías que hacer alguna modificación?
    ?>
</div>


<pre class="parte">
    Para expresar estas estructuras de datos de forma más legible para el programador
    se puede hacer uso de la sentencia var_dump(). Como ejemplo:

    var_dump($datos);
    var_dump($produtos);

</pre>

<div class="script-php">
        <?php
            var_dump($datos);
            var_dump($produtos);
        ?>
</div>