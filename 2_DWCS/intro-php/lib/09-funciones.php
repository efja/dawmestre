<?php include "cabecera.html" ?>

<pre class="parte">
    Ejemplo declaración de función saluda
    
    function saluda($pNombre) {
            echo 'Hola, qué tal? '.$pNombre;
    }
</pre>

<div class="script-php">
    <?php
        //Podríamos hacer una secuencia de intrucciones para saludar programáticamente
        echo '<br> Hi Enrique';
        echo '<br> Hi Mario';
        echo '<br> Hi Laura';
        echo '<br> Hi Saul';
        echo '<br> Hi Pablo';

        echo '<br><br> Cambiamos la forma del saludo';
        //Pero si queremos cambiar la forma del saludo, p.e. Hola, qué tal? Enrique
        //Deberíamos cambiar cada una de las líneas anteriores.
        //Mejor es agrupar el código que se repite y que podemos reutilizar
        //La forma es emplear una función que llamaremos 'saluda'

        //Así la definiremos. La función no devuelve nada y recibe un parámetro
        function saluda($pNombre) {
            echo '<br> Hola, qué tal? '.$pNombre;
        }

        //Así la utilizaremos
        echo saluda("Enrique");
        echo saluda("Mario");
        echo saluda("Laura");
        echo saluda("Saul");
        echo saluda("Pablo");
        echo '<br>Vamos con más ejemplos<br><hr>';

        //Si quisiéramos cambiar la forma del saludo. En este caso sólo tendríamos que cambiar una sólo línea
    ?>
</div>

<pre class="parte">
    Ejemplo función que retornará un resultado

    function suma($operando1,$operando2){
        return $operando1+$operando2;
   }
</pre>

<div class="script-php">
    <?php
        //Otro ejemplo con una función que retornará un resultado
        $resultadoSuma = suma(2,3);

        //Utilizando una variable que recoja el resultado
        echo "La suma de 2 y de 3 es: $resultadoSuma";

        //Imprimiendo el valor de retorno directamente
        echo '<br>La suma de 2 y de 3 es: '.suma(2,3);
        echo "<br><hr>";

        function suma($operando1,$operando2){
            return $operando1+$operando2;
        }
    ?>
</div>

<pre class="parte">
    Ejemplo Buscando dnis en una estructura de datos

    $dnis = ["867398224","8673918274","8673982274","8674398274","8673958274","8673198274","8673398274","8467398274"];
    buscaDni("8467398274",$dnis)
</pre>

<div class="script-php">
    <?php
        echo '<br>Buscando dnis en una estructura de datos...<br><br>';
        $dnis = ["867398224","8673918274","8673982274","8674398274","8673958274","8673198274","8673398274","8467398274"];

        if(buscaDni("8467398274",$dnis)){
                echo "<br><br>Ese dni se encuentra en el array";
        }
        else echo "<br><br>Ese dni no existe en nuestros datos";

        function buscaDni($pDniParaBuscar, $dnis){
            foreach($dnis as $unDni){
                echo "<br> ... sigo buscando...";
                if($pDniParaBuscar == $unDni){
                    echo "<br>Lo encontré!";
                    return true;
                }
            }
            return false;
        }

        function buscaDniConFor($pDniParaBuscar, $dnis){
            for($i=0; $i<count($dnis);$i++){
                if($pDniParaBuscar == $dnis[$i]){
                    return true;
                }
            }
            return false;
        }
    ?>
</div>