<?php include "cabecera.html" ?>

<pre class="parte">
    *******************************************************
    * Declarando variables y operando con ellas...        *
    *******************************************************
</pre>

<div class="script-php">
    <?php
        //Inicialización de variables
        $a = 8;
        $b = 3;

        //Operaciones con ellas e Impresión de valores
        echo $a + $b,"<br>";
        echo $a - $b,"<br>";
        echo $a * $b,"<br>";
        echo $a / $b,"<br>";
        $a++;
        echo $a,"<br>";
        $b--;
        echo $b,"<br>";
    ?>
</div>
