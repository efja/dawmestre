<?php include "cabecera.html" ?>

<pre class="parte">
    /******************/ 
    // PRIMER EJEMPLO
    /******************/
    $a = 8;
    $b = 3;
    
    if ($a < $b)
    {
        echo "'a' É menor que 'b'";
    }
    
    if ($a == $b)
    {
        echo "'a' É igual a 'b'";
    }
    else
    {
        echo "'a' É maior que 'b' o 'b' É maior que 'a'</p>";
    } 
</pre>

<div class="script-php">
    <?php
        //Declaración en pantalla e impresión de la operación. ¿Podrías mejorarlo?
        $a = 8;
        $b = 3;

        if ($a < $b)
        {
            //Esta instrucción se ejecutará siempre que a sea menor estricto que b
            echo "<p>'a' É menor que 'b'</p>";
        }
        
        if ($a == $b)
        {
            //Esta instrucción se ejecutará si a es igual a b
            echo "<p>'a' É igual a 'b'</p>";
        }
        else
        {
            //Esta instrucción se ejecutará si la condición del if que la precede, no se cumple
            echo "'a' É maior que 'b' o 'b' É maior que 'a'</p>";
        } 
    ?>
</div>

<pre class="parte">
    /******************/ 
    // SEGUNDO EJEMPLO
    /******************/ 
    $a = 5;
    $b = 7;

    // Por exemplo
    if ($a>$b)
        $resultado= "A é máis grande que B";
    else
        $resultado= "B é máis grande que A";
    
    // Otra alternativa a la escritura de un if, con asignación de variable asociada. Importante entenderlo
    // Podería quedar así:

    $resultado = ($a>$b) ? "A é máis grande que B":"B é máis grande que A";
    echo $resultado; 
</pre>

<div class="script-php">
    <?php
        $a = 5;
        $b = 7;
        // Por exemplo
        if ($a>$b)
            $resultado= "A é máis grande que B";
        else
            $resultado= "B é máis grande que A";
        
        // Otra alternativa a la escritura de un if, con asignación de variable asociada. Importante entenderlo
        // Podería quedar así: 
        $resultado = ($a>$b) ? "A é máis grande que B":"B é máis grande que A";
        echo '<br>'.$resultado;
    ?>
</div>

<pre class="parte">
    /******************/ 
    // TERCER EJEMPLO
    /******************/ 
    $posicion = "arriba";
    
    switch($posicion) {
        case "arriba":   // Bloque 1
            echo "A variable contén";
            echo " o valor arriba";
            //El break interrumple la evaluación del resto de sentencias case. Importante entenderlo
            break;
        case "abaixo":   // Bloque 2
            echo "A variable contén";
            echo " o valor abaixo";
            break;
        default:   // Bloque 3
            echo "A variable contén outro valor";
            echo " distinto de arriba e abaixo";
    } 
</pre>

<div class="script-php">
    <?php
        $posicion = "arriba";

        switch($posicion) {
            case "arriba":   // Bloque 1
                echo "<p>A variable contén";
                echo " o valor arriba</p>";
                //El break interrumple la evaluación del resto de sentencias case. Importante entenderlo
                break;
            case "abaixo":   // Bloque 2
                echo "<p>A variable contén";
                echo " o valor abaixo</p>";
                break;
            default:   // Bloque 3
                echo "<p>A variable contén outro valor";
                echo " distinto de arriba e abaixo</p>";
        } 
    ?>
</div>