<?php include "cabecera.html" ?>

<pre class="parte">
    *******************************************************
    * Declarando variables... *
    *******************************************************
</pre>

<?php
    // Primer fragmento
    // Declaración de variables, nótese que php es débilmente tipado
    $a = 1;
    $b = 3.34;
    $c = "Hola Mundo";
?>

<pre class="parte">
    *******************************************************
    * Presentación en pantalla del valor de las variables *
    *******************************************************
</pre>

<div class="script-php">
    <?php echo $a,"<br>",$b,"<br>",$c,"<br>" ?>
</div>

<pre class="parte">
    *******************************************************
    * Presentación en pantalla del resultado de opeción   *
    *******************************************************
</pre>

<div class="script-php">
    <?php
        // Una operacion
        $x = $c / 2;
        echo($x);
    ?>
</div>

<pre class="parte">
    *******************************************************
    * Info sobre variable (var_dump)                      *
    *******************************************************
</pre>

<div class="script-php">
    <?php
        // Comprobación del tipo
        var_dump($a);
        var_dump($b);
        var_dump($c);
        var_dump($x);
    ?>
</div>

<pre class="parte">
    *******************************************************
    * Impresiones                                         *
    *******************************************************
</pre>

<div class="script-php">
    <?php
        echo "<hr/>";
        echo "Ola Mundo";

        echo "<hr/>";
        echo "<pre>Este texto exténdese 
        por varias liñas. Os saltos de liña 
        tamén se envían. En modo cli consérvanse, 
        en modo embebido en html tiene que estar 
        entre las etiquetas de texto preformateado</pre>";
        
        echo "<hr/>";
        echo "Este texto exténdese \npor varias liñas. Os saltos de liña\ntamén se envían.";
        
        echo "<hr/>";
        echo "Para escapar caracteres, débese indicar \"deste xeito\".";
        
        // Pódense empregar variables dentro da sentenza echo
        $saudo = "que tal";
        $despedida = "ata logo";
        
        echo "<hr/>";
        echo "Ola, $saudo";  // Ola, que tal
        
        echo "<hr/>";
        // Tamén se poden empregar arrays
        $cadea = array("valor" => "saúdo dende un array");
        echo "Este é un {$cadea['valor']} "; //Este é un saúdo dende un array
        
        // Se se empregan comiñas simples, móstrase o nome da variable, non o seu valor
        echo "<hr/>";
        echo 'Ola, $saudo'; // ola, $saudo
        
        // Se non se engade ningún carácter, tamén é posible empregar echo para mostrar o valor das variables 
        echo "<hr/>";
        echo $saudo;             // que tal
        echo "<hr/>";
        echo $saudo,$despedida;  // que talata logo
        
        // O uso de echo con múltiples parámetros é igual que realizar unha concatenación
        echo "<hr/>";
        echo 'Esta ', 'cadea ', 'está ', 'construída ', 'con moitos parámetros.', chr(10);
        echo "<hr/>";
        echo 'Esta ' . 'cadea ' . 'está ' . 'construída ' . 'empregando concatenacion.' . "\n";
        
        //También se puede imprimir utilizando la siguiente sentencia, formateada. Investiga los diferentes formatos en Internet (%d, $f, etc...)
        printf("O numero dous con diferentes formatos: %d %f %.2f",2,2,2);
    ?>
</div>

