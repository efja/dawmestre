const min=0;
const max=9;
const tamano=3;

const aleatorio = function (min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

const matrizAleatoria = function(lonxitude){
    var matriz = new Array();
    for(i=0;i<lonxitude;i++){
        let fila =new Array();
        for(j=0; j<lonxitude;j++){
            fila.push(aleatorio(min,max));
        }
        matriz.push(fila);
    }

    
    return matriz;
}

const mostrarMatriz = function(matriz){
    matriz.forEach(i=> console.log(i));
    //matriz.forEach(fila=> fila.forEach(i=>console.log(i)));
}
 

const mostrarMatrizUnhaFila = function(matriz){
    matriz.forEach(fila=> fila.forEach(i=>console.log(i)));
}

const mostrarMatrizTabla = function(matriz){
    let saida="<table> "
    matriz.forEach(i=> saida+=generateRow(i));
    saida+="</table>";
    return saida;
}

const generateRow= function (row){
    var fila="<tr>";
    row.forEach(i=> fila+="<td width='10%'>" +i+"</td>");
    fila+="</tr>";
    return fila;
}

let m = matrizAleatoria(tamano);
mostrarMatriz(m);

let tablaMatriz =mostrarMatrizTabla(m);
document.getElementById("resultado").innerHTML=tablaMatriz;
