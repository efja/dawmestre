# Exercicio 17
Vamos a gestionar una lista de países haciendo uso de arrays. Para ello necesitas crear un archivo
llamado igual que el ejercicio de tipo JavaScript. El archivo contiene las siguientes funciones:

• Mostrar el número de elementos del array

• Mostrar todos los elementos del array

• Mostrar los elementos en sentido inverso.

• Mostrar los elementos ordenados 
alfabeticamente.

• Añadir un elemento al final del array

• Borrar un elemento al principio del array.

• Mostrar el elemento que se encuentra en una posición que el usuario indica.

Para tener en cuenta:

• E array será una variable global y que pasará por parámetro.