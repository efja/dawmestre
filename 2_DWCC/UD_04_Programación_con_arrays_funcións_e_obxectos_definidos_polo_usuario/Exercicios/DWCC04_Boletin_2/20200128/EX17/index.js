 var lista = ["Portugal", "españa", "Francia", "Italia", "Alemania", "Reino Unido"];
lista.toString()
//• Mostrar el número de elementos del array
function tamano(list){
    return list.length;
}
//• Mostrar todos los elementos del array
function mostrarElementos(list){
    return list.toString()  
}

//• Mostrar los elementos en sentido inverso. 
function listaInversa(list){
    return list.reverse();
}
//• Mostrar los elementos ordenados alfabeticamente. 
function ordenarLista(list){
    return list.sort((a, b) => a.localeCompare(b));
}

//• Añadir un elemento al final del array 
function anadirFinal(list, pais){
    return list.push(pais);
}

//• Borrar un elemento al principio del array. 
function borrarInicio(list){
    let resultado = new Array();
    //Verificar que a lista ten mais de un elemento
    if(list.length>1){
        resultado= list.slice(1,lista.length)
    }
     return resultado;
}

//• Mostrar el elemento que se encuentra en una posición que el usuario indica. 

function getElemento(list,pos){
    if(list.length>0 && list.length>pos) {
        return list[pos];
    }else{
        return null;
    }

}