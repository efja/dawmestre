# Problema 
Dado o codigo do ficheiro modulo.js precisase que sexa importado como un módulo noutro ficheiro js chamado main.js. Exactamente temos que importar a clase xogador renomeando co nome de Player.  No ficheiro modulo.js só se pode editar para incorporar o código preciso para configurar a importación/exportación. 

_Ficheiro: modulo.js_ 

```js
class Xogador{
    constructor(nome, dorsal){
        this._nome=nome;
        this._dorsal=dorsal;
    }

    get contentHtml(){
        return `<ul> ${this._nome} co dorsal ${this._dorsal} </ul>`;
    }
}
```