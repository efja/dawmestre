# Ex 02
Define en un modulo os seguintes elementos:
1. Unha constante chamada curso que conteña o curso actual. 
2. Unha función chamada saudo que teña como parametro un nome e obteña como resposta "ola nome pasado como parámetro". Exemplo: Ola Manolo
3. Unha función chamada despedida  que teña como parametro un nome e obteña como resposta "adeus nome pasado como parámetro".  Exemplo: Adeus Manolo

Deberas ter en conta o seguinte:
- Configura os puntos 1. e 2. no export e logo cargaos nun ficheiro chamado main.js. 
- Verifica que dende o modulo principal (main.js) non se pode invocar a función despedida. 