class Coche {
    constructor(marca,modelo,combustible,km,color) {
        this._marca = marca;
        this._modelo = modelo;
        this._combustible = combustible;
        this._kilometros = km;
        this._color = color;
    }

     //Getters e Setters de todos os atributos.
     get marca() { return this._marca}
     get modelo() { return this._modelo}
     get combustible() { return this._combustible}
     get kilometros() { return this._kilometros}
     get color() { return this._color}
  
     set marca(x) {this._marca = x;}
     set modelo(x) {this._modelo = x;}
     set combustible(x) {this._combustible = x;}
     set kilometros(x) {this._kilometros = x;}
     set color(x) {this._color = x;}

 //Redifinido toString
 toString(){
    let saida=" \n";
    for (let   a in this){
        saida+=a +" ==> "+this[a] + "\n";
    }
    return saida;
}

}

let c1 = new Coche("ford",  "fiesta","Gasoil", 1010,"negro");
let c2 = new Coche("Renault",  "Clio","Diesel", 1010,"Verde");
c1.marca="Ford"; //Set de la marca del vehículo
c1.modelo="Fiesta";
c1.marca; //Get de la marca

let lista = new Array();
lista.push(c1);
lista.push(c2);

lista.forEach(item=> console.log(item.toString()));


