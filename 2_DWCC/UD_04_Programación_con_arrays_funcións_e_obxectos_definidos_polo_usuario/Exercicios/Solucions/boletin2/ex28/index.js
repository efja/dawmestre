const tipoMusic={"rock":1, "pop":2, "folk:":3}

class Disco {

    // Atributos.
    nombreDisco;
    grupoMusica;
    anoPublicacion;
    tipoMusica;
    localizacion;
    prestado = false;

    // Constructor.
    constructor() {
        this.nombreDisco = '';
        this.grupoMusica = '';
        this.anoPublicacion = 0;
        this.tipoMusica = '';
        this.localizacion = 0;
        this.prestado = false;
    }

    // Método para inicializar la información de un disco.
    inicializarDisco(nombreDisco, grupoMusica, anoPublicacion, tipoMusica, localizacion) {
        this.nombreDisco = nombreDisco;
        this.grupoMusica = grupoMusica;
        this.anoPublicacion = anoPublicacion;
        
        if(tipoMusic[tipoMusica]==undefined){
            //O tipo de musica non está na coleccion de tipos 
            this.tipoMusica = null;
        }else{
            this.tipoMusica = tipoMusica;
        }
       
        this.localizacion = localizacion;
    }

    // Método para guardar si un disco es prestado o no.
    set prestado(prestado) {
        this.prestado = prestado;
    }
    mutarEstado(){
        this.prestado = !this.prestado ;
    }

    // Método que muestra la información de un disco.
    mostrarInfoDisco() {
        return 'Disco { ' 
                + ' <strong>Nombre:</strong> ' + this.nombreDisco 
                + ', <strong>Grupo:</strong> ' + this.grupoMusica 
                + ', <strong>Año de publicación:</strong> ' + this.anoPublicacion 
                + ', <strong>Tipo de música:</strong> ' + this.tipoMusica 
                + ', <strong>Localizacion:</strong> ' + this.localizacion 
                + ', <strong>Es prestado:</strong> ' + (this.prestado ? 'SI' : 'NO')
            + ' }';  
    }

     //Redifinido toString
     toString(){
        let saida=" \n";
        for (let   a in this){
            saida+=a +" ==> "+this[a] + "\n";
        }
        return saida;
    }
}

//PROBAS
let lista = new Array();
let d1 = new Disco();
let d2 = new Disco();
let d3 = new Disco();

d1.inicializarDisco("Atlántico",2018,"Folk","Biblioteca");
d2.inicializarDisco("Augas de Maio",1999,"Folk","Biblioteca");
d3.inicializarDisco("O Berro Seco",1980,"Folk","Depósito");
d3.setPrestado(true);

console.log(d1.mostrarInfoDisco());
console.log(d2.mostrarInfoDisco());
console.log(d3.mostrarInfoDisco());

lista.push(d1);
lista.push(d2);
lista.push(d3);


console.log(lista.toString());
 