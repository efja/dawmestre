//Obxecto literal
var teacher = {
    //Atributos
    name:"Iván Gómez", age:30, dept:"Computer Science",
    //Métodos
    print:function () { return ("Hello! My name is " +this.name+" and I work in "+ this.dept); },
    getCode:function () {
        var code = this.dept.slice(0,3);
        var names = this.name.split(" ");
        for (var i in names) {
            code += names[i][0];
        }
        code += Math.ceil(Math.random()*this.age);
        return code.toUpperCase();
    }
}

//Invocar métodos do obxecto literal
console.log(teacher.print());
console.log(teacher.getCode());
//Invocar método do obxecto literal pasando outro obxecto
var tutor = {name:"Pedro Cabana Valdivia",age:25,dept:"History"};
console.log(teacher.getCode.call(tutor));