# Exercicio 02 
Partindo da clase base que se proporciona, facer as seguintes tarefas:
- Engadir getters/setters á clase base
- Crear unha subclase para representar atletas coas seguintes características:
  - Terá atributos para a altura (en cm) e peso (en kg).
  - Terá métodos getters/setters.
  - Terá un método para devolver o valor de todos os atributos nun string.
  - Terá un método para calcular o BMI (Body Mass Index). PESO /ALTURA^2

  - Terá un método para adelgazar e outro para engordar nos que, se non se indica un valor, os cambios no peso será de 1 kg.
Crear un obxecto da subclase atleta e probar todos os métodos.

```js
//Clase persoa
class Person {
    constructor(name) { this._name = name; }
    print() { return "Hello! My name is " +this._name; }
}
```