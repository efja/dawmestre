//Bucle WHILE
//Bucle FOR
//Bucle FOR-IN
//Bucle FOR-OF


var seasons = new Array();
seasons[0] = "Spring";
seasons[1] = "Summer";
seasons[2] = "Autumn";
seasons[3] = "Winter";

//Bucle WHILE
var i=0;
while (i<seasons.length) {
    console.log(seasons[i]);
    i++;
}

//Bucle FOR
for (var i=0; i<seasons.length; i++) {
    console.log(seasons[i]);
}

//Bucle FOR-IN
for (i in seasons) {
    console.log(seasons[i]);
}

//Bucle FOR-OF
for (var season of seasons) {
    console.log(season);
}