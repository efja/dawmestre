# Problema
Tense un array con un conxunto de nomes e quérese analizar o número de nomes repetidos nunha clase.

```js 
const nomes=['Manuel','Víctor','Elena','María','Balbina','Saladina','Manuel','Efrén','Elena','Filomena','Luisa','Luis','Lois'];
```

Pista:

* NaN || 1
* undefined || 1

Saida esperada
* Manuel = 2
* Víctor = 1
* Elena = 2
