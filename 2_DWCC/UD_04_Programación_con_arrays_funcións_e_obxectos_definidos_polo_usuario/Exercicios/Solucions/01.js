var frutas= ["guindas", "manzanas", "bananas"]; 

function ordenar (a,b) {
    return a.localeCompare(b); 
}

frutas.sort((a,b)=>ordenar(a,b)); 

var puntos = [1,10,2,21]; 
puntos.sort((a,b)=>ordenar(a,b)); 

var cosas = ['word', 'Word', '1 Word', '2 Words'];
cosas.sort((a,b)=>ordenar(a,b)); 
