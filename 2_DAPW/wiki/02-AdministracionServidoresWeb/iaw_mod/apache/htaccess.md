﻿


> Written with [StackEdit](https://stackedit.io/).
# Arquivos `.htaccess`

Os ficheiros `.htaccess` son ficheiros de configuración distribuída. A súa utilidade é incluír **directivas que afecten unicamente ao directorio no que están situados e, por extensión, aos seus subdirectorios**.

Recoméndase evitar o uso destes ficheiros sempre que sexa posible, xa que a procura dos mesmos a través dos directorios retarda moito o servidor Apache e pode constituír un problema de seguridade. É dicir, sempre que a persoa que vai configurar o devandito directorio teña acceso aos ficheiros de configuración globais, debe situar nestes as directivas relativas a ese directorio (dentro dun bloque `Directory`).

Hai **situacións**, como por exemplo aquelas nas que varios usuarios empregan un mesmo servidor Apache para os seus sitios web e ditos usuarios non teñen acceso aos ficheiros globais de configuración, nas que a única solución é o emprego de ficheiros `.htaccess`.

Cando está permitido o uso de arquivos `.htaccess` e estes se modifican, **non é necesario o reinicio do servidor Apache**. De feito, se o usuario tivese permiso para levar a cabo este reinicio, non debería modificar a configuración en ditos arquivos, senón no arquivo principal de configuración do servidor.

### Directiva `AccessFileName`

Utilízase para modificar o nome dos ficheiros de configuración distribuídos (por defecto, .htaccess). Exemplo:

```
AccessFileName .config
```

Isto faría que a configuración dun directorio se situase en ficheiros chamados `.config`.

### Directiva `AllowOverride`

Indica que directivas declaradas nos ficheiros `.htaccess` deben sobrescribir ás declaradas nos ficheiros de configuración globais. Os seus valores poden ser, entre outros:

* `None`: Os ficheiros `.htaccess` son ignorados completamente.

* `All`: Todas as directivas declaradas nos `.htaccess` sobrescriben ás declaradas nos ficheiros globais.

* `AuthConfig`: Permite o uso de directivas de autorización (`AuthGroupFile`, `AuthName`, `AuthType`, `AuthUserFile`, `Require`, etc.).

* `Indexes`: Permite o uso de directivas que controlan a indexación de directorios (por exemplo, `DirectoryIndex`).

* `FileInfo`: Directivas relacionadas co mapeo de URL: redireccións, alias, …
* `Limit`: Directivas de control de acceso:  `Allow`,  `Deny`  y  `Order`.

A directiva por defecto é none.
Con All podo usar todas as directivas detalladas neste apartado.
Pódese poñer máis de 1 directiva.
