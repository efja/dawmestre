﻿


> Written with [StackEdit](https://stackedit.io/).
> 
# Módulos de Multiprocesamiento (MPM)

Los servidores web pueden ser configurados por la forma en que se crean y gestionan los subprocesos de apache necesarios para que éste maneje y responda las peticiones que recibe. En el caso de Apache, los MPM (Módulos de multiprocesamiento) son los que dan soporte a esta funcionalidad del servidor..


## prefork

Este módulo crea diferentes procesos independientes para manejar las diferentes peticiones. Esta técnica de crear varios procesos se la denomina forking, de ahí el nombre mpm-prefork. Al iniciar Apache Prefork se crean varios procesos hijo y cada uno puede responder una petición.

Para cambiar de MPM tenemos que desactivar el actual y activar el nuevo módulo::

```
# a2dismod mpm_event
# a2enmod mpm_prefork
# service apache2 restart	

# apachectl -V
...
Server MPM:     prefork
...
```

Para ver los procesos de apache que están levantados:

    ps -A | grep apache2

Me deberían aparecer 6 procesos (el apache padre y los 5 procesos hijos por defecto)

En `/etc/apache2/mods-availables/mpm_prefork.conf` podemos configurar las opciones de configuración de este módulo:

-   `StartServers`: Número de procesos hijos que se crean al iniciar el servidor, por defecto 5.
-   `MinSpareServers`: Mínimo número de procesos esperando para responder, por defecto 5.
-   `MaxSpareServers`: Máximo número de procesos esperando para responder, por defecto 10.
-   `MaxRequestWorkers`: Límite de peticiones simultaneas que se pueden responder, por defecto 150.
-   `MaxConnectionsPerChild`: Límite en el número de peticiones que un proceso hijo puede atender durante su vida, por defecto 0 => no se especifica ese número. Si se pone un límite, cuando llega a ese límite se elimina y se crea otro proceso.


## event

Por defecto Apache 2.4 se configura con el MPM event, podemos ver el MPM que estamos utilizando con la siguiente instrucción:

```
# apachectl -V
...
Server MPM:     event
...
```

El MPM [event](https://httpd.apache.org/docs/2.4/mod/event.html) en versiones anteriores de apache2 era un módulo experimental, pero en Apache 2.4 se considera estable y mejora el funcionamiento del [PMP worker](https://debian-handbook.info/browse/es-ES/stable/sect.http-web-server.html). 
Este módulo usa procesos y al mismo tiempo hace uso de threads (también llamados hilos), es decir, combina las técnicas de **forking** (de procesos) y **threading**. Al iniciar Apache Event se crean varios procesos hijo (por defecto 2) y a su vez cada proceso hijo emplea varios threads. Con esto se consigue que cada proceso hijo pueda manejar varias peticiones simultaneas gracias a los threads. 
Este modo es más eficiente que el prefork ya que para atender un número determinado de peticiones se necesitan menos procesos, que consumen mucha menos memoria al atender peticiones mediante threads (mucho más ligeros que los procesos). Además trabaja mucho mejor con conexiones persistentes.

En `/etc/apache2/mods-availables/mpm_event.conf` podemos configurar las opciones de configuración de este módulo:

-   `StartServers`: Número de procesos hijos que se crean al iniciar el servidor, por defecto 2.
-   `MinSpareThreads`: Mínimo número de hilos esperando para responder, por defecto 25.
-   `MaxSpareThreads`: Máximo número de hilos esperando para responder, por defecto 75.
-   `ThreadLimit`: Límite superior del número de hilos por proceso hijo que pueden especificarse, por defecto 64.
-   `ThreadsPerChild`: Número de hilos de cada proceso, por defecto 25.
-   `MaxRequestWorkers`: Límite de peticiones simultaneas que se pueden responder, por defecto 150.
-   `MaxConnectionsPerChild`: Límite en el número de peticiones que un proceso hijo puede atender durante su vida, por defecto 0 (no se indica ningún límite). Si se pone un límite, cuando llega a ese límite se elimina y se crea otro proceso.

IMPORTANTE: Para utilizar procesado de lenguaje dinámico el servidor Apache utiliza por defecto un módulo php que es incompatible con el modo event (programación con hilos)

**Solución:** 
No obstante, el modulo event es incompatible con el módulo PHP de Apache, por lo que al instalarlo desactiva el módulo event y activa el prefork.
 - Deshabilitar el módulo php por defecto `sudo a2dismod php7.2`
 - Deshabilitar el módulo prefork `sudo a2dismod mpm_prefork`
 - Habilitar el módulo event `sudo a2enmod mpm_event`
 - Instalar y habilitar el módulo php-fm y otros módulos de apache que necesita para funcionar:
 
  `sudo a2enconf php7.2-fpm`
   
Más info [aquí](https://www.digitalocean.com/community/tutorials/how-to-configure-apache-http-with-mpm-event-and-php-fpm-on-ubuntu-18-04-es)
