﻿


> Written with [StackEdit](https://stackedit.io/).
> # # Asegurando el servicio de transferencia de archivos.
Empleando el protocolo  ftp  cualquiera que tenga acceso al canal de transmisión podrá ver en texto claro todo lo que se transmite, esto es, los datos no se cifran.

Entonces, cuando interese asegurar el servicio de transferencia de archivos debes descartar el protocolo  ftp  y empezar a pensar en otras alternativas, como:  **ftps**  o  **sftp**.

FTPS  es una extensión del protocolo  FTP  que asegura el cifrado en la transferencia mediante los protocolos  SSL/TLS. Permite tres tipos de funcionamiento:

-   SSL  Implícito:
    -   Como conexiones  HTTPS
    -   Usa los puertos 990 y 989.
-   SSL  Explícito
    -   El cliente usa los mismos puertos estándar FTP: 20 y 21 pero se efectúa el cifrado en ellos.
    -   Usa  AUTH SSL.
-   TLS  Explícito:
    -   Similar a SSL Explícito pero usa  AUTH TLS.

El cifrado al que nos referimos es el  [cifrado de clave pública o asimétrico](http://www.criptored.upm.es/intypedia/video.php?id=criptografia-asimetrica&lang=es "Acceder a la web de criptored.upm.es para ver un vídeo sobre el funcionamiento del cifrado de clave pública o asimétrico (Se abre en una ventana nueva)"):  **clave pública (kpub)**  y  **clave privada (kpriv)**. La  **kpub**  interesa publicarla para que llegue a ser conocida por cualquiera, la  **kpriv**  no interesa que nadie la posea, solo el propietario de la misma. Ambas son necesarias para que la comunicación sea posible, una sin la otra no tienen sentido, así una información cifrada mediante la  **kpub**  solamente puede ser descifrada mediante la  **kpriv**  y una información cifrada mediante la  **kpriv**  sólo puede ser descifrada mediante la  **kpub**.

En el cifrado asimétrico podemos estar hablando de individuos o de máquinas, en nuestro caso hablamos de máquinas y de flujo de información entre el  **cliente  ftp** **(A)**  y el  **servidor  ftp (B)**. 
Funcionamiento del cifrado asimétrico.
![Cifrado_Clave_Publica_o_Asimetrico](img/Cifrado_Clave_Publica_o_Asimetrico.jpg)

![Certificado_Firma_Digital](img/Certificado_Firma_Digital.jpg)
