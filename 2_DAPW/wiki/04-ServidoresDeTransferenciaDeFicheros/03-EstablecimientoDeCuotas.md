# Establecemento de cotas de espazo en disco

Unha cota é un límite establecido polo administrador do sistema que restrinxe determinados aspectos do uso do sistema de ficheiros. A súa función é a de asignar o espazo de disco dun modo razoable.

## Definición de cotas

### Instalar o paquete quota:

```
sudo apt-get install quota
```

### Habilitación 

Habilitamos o uso de cotas no noso sistema de arquivos. Editamos /etc/fstab para engadir `usrquota` e `grpquota` á partición adecuada, que habilitan o uso de cotas para usuarios e grupos, respectivamente:

```
/dev/hda1/ ext4 usrquota,grpquota,errors=remount-ro 0 1
```

### Recargamos os cambios:

```
sudo mount -o remount /
```

### Creamos os arquivos de cotas no sistema:

```
sudo quotacheck -cmug /
```

Estas son as opcións escollidas:

* `-c` Crear os arquivos de cotas.
* `-m` Non monta os sistemas de ficheiros de só lectura.
* `-u` Unido a -c crea o ficheiro de cotas de usuarios.
* Se non se especifica nin `-g` nin `-u`, só crea o ficheiro de cotas de usuarios.

### Reiniciamos a máquina.

```
sudo reboot
```

## Establecemento de cotas

```
setquota -u/g NomeUsuario/NomeGrupo bloques-brando bloques-duro inodes-blando inodes-duro disco
```

A opción `-u` indica que se establece unha cota para un usuario, `-g` para un grupo.

Un bloque é 1KB e os inodes indican os nodos (número de ficheeros) que pode tener o usuario/grupo.

```
setquota -u prueba 10240 20480 0 0 /dev/sda1
```

Lembremos que para ver as particións do sistema e o seu uso emprégase o comando `df –h`.

## Edición de cuotas

edquota -u/g NombreUsuario/NombreGrupo

## Modificación do período de graza

```
edquota -t
```

## Copia de cotas dun usuario a outro

```
edquota -p usuario1 usuario2
```

Copia a configuración de cotas que tivese `usuario1` a `usuario2`.

## Listado de cotas e usuarios

```
repquota -a
```

Para amosar a información de cotas tanto de usuarios como de grupos engadiremos a opción `–g`.

```
repquota -ag
```

## Consultar cota dun usuario/grupo particular

```
quota -u/g NombreUsuario/NombreGrupo
```

## Reconocimento

Los autores originales de este exto fueron la Profesora Amalia Falcón Docampo, la Profesora Laura Fernández Nocelo y la Profesora Marta Rey López.

![Reconocimiento y licencia](img/licencia-CSIFC03_MP0614_V000402_UD04_A02_Configuracion_FTP.png)