﻿


> Written with [StackEdit](https://stackedit.io/).

## >OPCIONES DE HOSTING

> Dentro de la elección de hosting tenemos muchas alternativas dependiendo de qué tipo de servicios necesitamos, con cuánto presupuesto contemos y otros servicios.

Una clasificación a grosso modo podría ser la siguiente:

-   Tradicional -> compartición de recursos "best-effort" [Ejemplos externos](https://hostingsaurio.com/hosting-gratuito/) + [hospedaxe.local](https://axuda.iessanclemente.net/index.php/Creacion_de_dominios_.local_no_IES_San_Clemente)
-   VPS ( Servidor Privado Virtual) -> 
	- recursos (CPU, RAM garantizados)
	- servicios no administrados: te dan un S.O. configurado para poder instalar y administrar tus servicios
	-  Ejemplos: [DigitalOcean](https://www.digitalocean.com/)
-   Plesk / Cpanel ( muy poco flexibles )
-   PAAS / IAAS
    -   Openstack
    - Azure (Microsoft)
    -   AWS (Amazon)
    - Google Cloud
-   PAAS
    -   Heroku / Modulus
