﻿


> Written with [StackEdit](https://stackedit.io/).
> ## Escalado

Cuando nuestro sitio o aplicación web crece en volumen de visitas  **concurrentes**, o bien tenemos que mejorar los tiempos de respuesta, podemos llegar a tener un problema si no hemos preparado nuestra infraestructura para ello.

Escalar una aplicación no es trivial y dependiendo de si la aplicación o sitio web está sujeto a un  [**SLA**]("Acuerdo de nivel de Servicio"), puede que no nos podamos permitir una parada de servicio, o ante un volumen masivo de visitas concurrentes nuestro servicio directamente deje de responder.

### Escalado Vertical

Supone el escalado mediante el uso de recursos de hardware más potentes (tendiendo a un número menor de máquinas):

-   **CPU’s**  con más cores o más potentes  
    
-   Mayor cantidad de  **RAM**  o más rápida  
    
-   **Disco**  (SSD)  
    
-   Mayor  **Ancho de Banda**

Dependiendo de cómo sea nuestro servidor (Instancia física o virtualizado), podremos aplicar cierto escalado sin interrupción de servicio o no.

> **IMPLICA**

-   Mayor coste  
    
-   Puede implicar  **parada o degradación**  de servicio  
    
-   Pérdida de cierto control, si se delega en servicios a terceros, dependiendo del grado de flexibilidad que ofrezcan

**NOTA**  Es fundamental medir el rendimiento de nuestra aplicación en su conjunto. Idealmente se debería plantear la arquitectura de escalado una vez se tienen métricas del funcionamiento de la aplicación o web con una base estable.

### Escalado Horizontal

Supone el escalado en base a más máquinas menos potentes se distribuyan la carga del trabajo a realizar.

> **IMPLICA**

-   Coste (Depende del caso, pero generalmente implica menor coste que un escalado vertical)  
    
-   puede implicar  degradación de servicio  
    
-   Balanceo de Carga  
    
-   Coordinación de las sesiones de usuario en caso de haberlas  (**Sticky Sessions**) o algún mecanismo para que iniciada una transacción entre cliente y servidor que consta de varias peticiones, toda la transacción sea atómica (todas las peticiones vayan al mismo servidor).

## Alta disponibilidad

Consiste en garantizar que siempre haya al menos un mínimo de elementos de la arquitectura para poder procesar correctamente una “petición” (por petición entendemos aquí una transacción completa).

Aquí aparecen el concepto de  **redundancia**  y  [**SPOF**]( "Single point of failure").

Una arquitectura será más robusta cuanto más redundados estén los elementos mínimos necesarios para llevar a cabo una transacción completa. Idealmente no debería haber ningún  **SPOF**.

## Balanceo de Carga

Consiste en distribuir la carga de (tráfico, consultas a la base de datos…) entre distintos elementos de la arquitectura.

El balanceo puede hacerse mediante  **hardware**  (más caro pero más fiable) o mediante  **software**  (más flexible pero exige un conocimiento mayor).

## CDN : Content Delivery Network

Es una red de servidores distribuída geográficamente destinada a reducir los tiempos de latencia en la entrega de contenidos.

La latencia puede ser crítica según el tipo de proyecto:

-   Streaming de Vídeo  
    
-   Aplicaciones en  tiempo real  
    
-   Si tu web va a ser visitada desde un móvil y la  **velocidad de la red móvil es mala**.


## Caché

Es un mecanismo para “acelerar” respuestas previamente generadas y reutilizar recursos previamente obtenidos.

La caché puede situarse a nivel de cliente, servidor, base de datos, código…

Puedes hacer uso de ciertos mecanismos en  **HTTP**  para “cachear” contenidos:  [Almacenar HTTP en caché](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching?hl=es "Almacenar HTTP en caché")



## Conceptos sobre Seguridad

### CORS (Cross Origin Resource Sharing)

Mecanismo por el cual se puede limitar/retringir el uso de recursos de dominios fuera de del dominio propio de la web vía cabeceras HTTP.

Para más información sobre el tema, podéis consultar la entrada de la Wikipedia al respecto :  [Cross-origin Resource Sharing](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing "Cross Origin resource sharing")

### Ataques

Todo servicio expuesto en Internet puede ser vulnerable a ataques de distinta naturaleza. Si somos nosotros los que administramos el servicio, debemos preocuparnos de estar preparados ante ciertos ataques como:

-   **DOS y DDOS**: Denegación de servicio. Se produce por saturación de peticiones.
    **
-   **XSS**  y  **SQL Injection**: Sucede cuando no se comprueban y sanitizan las entradas (vía formulario generalmente) en nuestra web.

Estos son algunos de los ataques más comunes. Dependiendo del tipo de ataque y su explotación debemos tomar medidas en diferentes frentes.

