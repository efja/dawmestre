﻿


> Written with [StackEdit](https://stackedit.io/).
> ## Integracion apache y tomcat

Un servlet container como Tomcat es capaz de servir contenidos estáticos, pero un servidor web como Apache lo hace de mamera más eficiente. Por ello podemos tener un servidor web como Apache que reciba todas las peticiones y configurarlo para que reenvíe a Tomcat las peticiones a recursos web java (servlets, JSP, etc). De esta manera podremos tener alojadas página estáticas, páginas dinámicas en tecnología PHP, python y Servlets y JSPs (éstos servidos por un Tomcat)

Por defecto, Apache correrá en el puerto 80 y Tomcat en el 8080. En esta configuración habría que deshabilitar los accesos remotos a Tomcat, sólo se podrían enviar peticiones al puerto 8080 desde localhost (http://localhost:8080) 

Para integrar Apache con Tomcat vamos a utilizar el módulo proxy que básicamente "pasa" las peticiones que entran a una url desde Apache a Tomcat.

Habilitamos los módulos con:

a2enmod proxy

a2enmod proxy_http

Editamos el fichero _/etc/apache2/mods-enabled/proxy.conf_ y ponemos:

    <Proxy *>  
    AddDefaultCharset off  
    Require all granted  
    </Proxy>

Editamos p.ej.el sitio virtual por defecto /etc/apache2/sites-available/default (se puede hacer también con cualquier otro sitio virtual) y añadimos al inicio ( p.ej. después del DocumentRoot ) lo siguiente:

ProxyPass /sample http://localhost:8080/sample  
ProxyPassReverse /sample http://localhost:8080/sample

siendo sample el nombre de una aplicacion desplegada en Tomcat. Le decimos a Apache que haga de intermediario ( proxy ) y que cuando reciba la url _/sample_, se la pase a Tomcat, que es quien realmente puede ejecutar el servlet o servlets de la aplicación web java.

Ahora falta modificar el fichero server.xml de Tomcat y modificar el conector para especificar que recibe peticiones desde un proxy.

    <Connector port="8080" protocol="HTTP/1.1"  
    connectionTimeout="20000"  
    URIEncoding="UTF-8"  
    redirectPort="8443" proxyPort="80" />

Reiniciamos Apache y Tomcat y entramos en http://ip_server/sample

