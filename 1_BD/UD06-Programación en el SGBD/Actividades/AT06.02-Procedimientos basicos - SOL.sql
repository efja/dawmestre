use pruebas;
/* 1. Utilizando el comando IF, crea un procedimiento ‘siDivisible’ que reciba 2 números y muestre sí el primero es divisible por el segundo. 
	Por ejemplo, si introducimos los números 4 y 2. El procedimiento deberá mostrar el mensaje: 4 es divisible por 2. */
delimiter /
DROP PROCEDURE IF EXISTS pr_siDivisible / -- El fin de instrucción ahora es con /
CREATE PROCEDURE pr_siDivisible(IN p_dividendo int,IN p_divisor int)
BEGIN
	if (p_dividendo % p_divisor=0) then
		select concat(p_dividendo,' es divisible por ',p_divisor) as resultado;
	else
		select concat(p_dividendo,' no es divisible por ',p_divisor) as resultado;
	end if;
END
/
DELIMITER ;
call pr_siDivisible(10,2);
call pr_siDivisible(11,2);

/* 2. Utilizando el comando CASE, crear un procedimiento ‘notasPrimaria’ que reciba dos parámetros: p_Numero (nota de un alumno) 
	y p_Nombre (nombre del alumno). El procedimiento deberá mostrar un mensaje con el siguiente formato y según el siguiente baremo:
	- si la nota está entre 9 y 10, deberá mostrar “el alumno [nombre] tiene un Sobresaliente”.
	- si la nota está entre 7 y 8, deberá mostrar “el alumno [nombre] tiene un Notable”.
	- si la nota es un 6, deberá mostrar “el alumno [nombre] tiene un Bien”.
	- si la nota es un 5, deberá mostrar “el alumno [nombre] tiene un Suficiente”.
	- si la nota es menor que 5, deberá mostrar “el alumno [nombre] tiene un Insuficiente”.
*/
drop procedure if exists pr_notasprimaria;
delimiter /
create procedure pr_notasprimaria(IN p_numero int,IN p_nombre varchar(20))
BEGIN
	case when p_numero between 9 and 10 then
		select concat('el alumno ', p_nombre, ' tiene un sobresaliente') as resultado;
	when p_numero between 7 and 8 then
		select concat('el alumno ', p_nombre, ' tiene un notable') as resultado;
	when p_numero=6 then
		select concat('el alumno ', p_nombre, ' tiene un bien') as resultado;
	when p_numero=5 then
		select concat('el alumno ', p_nombre, ' tiene un suficiente') as resultado;
	else
		select concat('el alumno ', p_nombre, ' tiene un insuficiente') as resultado;
	end case;
END
/
delimiter ;
call pr_notasprimaria(9,'pepe');
call pr_notasprimaria(10,'adrian');
call pr_notasprimaria(11,'adrian');

/* 3. Modifica el procedimiento anterior para tener en cuenta que el usuario puede introducir una nota que no sea un valor entre 0 y 10. 
	En ese caso, el procedimiento deberá mostrar el siguiente mensaje: ‘Nota incorrecta, debe introducir una nota entre 0 y 10’.*/
drop procedure if exists pr_notasprimaria2;
delimiter /
create procedure pr_notasprimaria2(IN p_numero int,IN p_nombre varchar(20))
BEGIN
	case when p_numero between 9 and 10 then
		select concat('el alumno ', p_nombre, ' tiene un sobresaliente') as resultado;
	when p_numero between 7 and 8 then
		select concat('el alumno ', p_nombre, ' tiene un notable') as resultado;
	when p_numero=6 then
		select concat('el alumno ', p_nombre, ' tiene un bien') as resultado;
	when p_numero=5 then
		select concat('el alumno ', p_nombre, ' tiene un suficiente') as resultado;
	when p_numero between 0 and 4 then 
		select concat('el alumno ', p_nombre, ' tiene un insuficiente') as resultado;
    else
		select 'La nota del alumno debe estar entre 0 y 10' as resultado;
	end case;
END
/
delimiter ;
call pr_notasprimaria2(9,'pepe');
call pr_notasprimaria2(11,'pepe');

/* 4. Utilizando un bucle REPEAT, crea un procedimiento que muestre la suma de los números pares comprendidos entre 100 y 90 (100-98-96-94-92-90).*/
drop procedure if exists pr_suma_90_100;
delimiter /
create procedure pr_suma_90_100()
begin
	declare v_contador int default 90 ;
	declare v_suma int default 0;
    repeat 
		set v_suma= v_suma + v_contador;
        set v_contador=v_contador+2;
	until v_contador>100
	end repeat;
    select v_suma;
end;
/
delimiter ;
call pr_suma_90_100();
/* 5. Utilizando un bucle WHILE, crea un procedimiento que muestre la suma de los 100 primeros números naturales (1-2-............-99-100).*/
drop procedure if exists pr_suma_1_100;
delimiter /
create procedure pr_suma_1_100()
begin
	declare v_contador int default 1;
	declare v_suma int default 0;
    while v_contador <= 100 do
		set v_suma= v_suma + v_contador;
        set v_contador=v_contador+1;
	end while;
    select v_suma;
end
/
delimiter ;
call pr_suma_1_100();
/* 6. Utilizando un bucle LOOP, crear un procedimiento que muestre la suma de los números pares comprendidos entre 1 y 20 
	excepto el 10 (2-4-6-8-12-14-16-18-20).*/
drop procedure if exists pr_suma_pares;
delimiter /
create procedure pr_suma_pares()
begin
	declare v_contador int default 2;
    declare v_suma int default 0;
    
    inicio:loop
		if v_contador >20 then 
			leave inicio;
        end if;
        if v_contador!=10 then
			set v_suma= v_suma + v_contador;
		end if;
        set v_contador=v_contador+2;
    end loop;
	select v_suma;  
end
/
delimiter ;

call pr_suma_pares();
/* 7. Crear un procedimiento ‘operacionesAritméticas’, que reciba 2 números enteros o decimales y que muestre la suma, resta, 
	producto y división de dichos números.*/
drop procedure if exists pr_operaciones_aritmeticas;
delimiter &
create procedure pr_operaciones_aritmeticas (in operador1 decimal(6,2), in operador2 decimal(6,2))
begin
	declare v_suma decimal(7,2);
    set v_suma = operador1+operador2;
    select concat('La suma es: ',v_suma);
    select concat('La resta es: ',operador1-operador2);
    select concat('El producto es: ',operador1*operador2);
    select concat('La division es: ',operador1/operador2);
end
&
delimiter ;
call pr_operaciones_aritmeticas (5, 7.3);

/* 8. Crear un procedimiento ‘primeros20Impares’ que muestre los 20 primeros números impares.*/
drop procedure if exists pr_primeros20impares;
delimiter /
create procedure pr_primeros20impares()
begin
	declare v_contador1 int default 1;
    declare v_contador2 int default 0;
	while v_contador2<20 do
		select concat(' El numero impar es ', v_contador1);
        set v_contador1=v_contador1+2;
        set v_contador2=v_contador2+1;
	end while;
end
/
delimiter ;
call pr_primeros20impares();

/* 9. Modifica el procedimiento anterior para que muestre los 20 números impares que sigan al número que se indique como parámetro*/
drop procedure if exists pr_20impares;
delimiter /
create procedure pr_20impares(in p_valor int)
begin
	declare v_contador1 int;
    declare v_contador2 int default 0;
    if p_valor%2=0 then 
		set v_contador1=p_valor+1;
	else
		set v_contador1=p_valor;
	end if;
	while v_contador2<20 do
		select concat(' El numero impar es ', v_contador1);
        set v_contador1=v_contador1+2;
        set v_contador2=v_contador2+1;
	end while;
end
/
delimiter ;
call pr_20impares(4);

/* 10. Crear un procedimiento ‘fechaLarga’ que reciba una fecha (dd-mm-aaaa) y muestra la fecha en formato largo. 
	Por ejemplo: 31-10-2014 deberá mostrar: Viernes, 31 de Octubre de 2014 */
drop procedure if exists pr_fechaLarga;
delimiter /
create procedure pr_fechaLarga(in p_fecha date)
begin 
	declare v_diasemana varchar(20);
	declare v_diames int;
	declare v_mes varchar(20);
	declare v_anho int;
    declare v_diasemana_es varchar(20);
    declare v_mes_es varchar(20);
	set v_diasemana=date_format(p_fecha,'%a');
    set v_diames=date_format(p_fecha,'%d');
    set v_mes=date_format(p_fecha,'%m');
    set v_anho=year(p_fecha);
    case v_diasemana
		when 'mon' then set v_diasemana_es='lunes';
		when 'tue' then set v_diasemana_es='martes';
		when 'wed' then set v_diasemana_es='miercoles';
		when 'thu' then set v_diasemana_es='jueves';
		when 'fri' then set v_diasemana_es='viernes';
		else set v_diasemana_es='finde';
    end case;
	case v_mes
		when '01' then set v_mes_es='enero';
		when '02' then set v_mes_es='febrero';
		when '03' then set v_mes_es='marzo';
		when '04' then set v_mes_es='abril';
		when '05' then set v_mes_es='mayo';
		when '06' then set v_mes_es='junio';
		when '07' then set v_mes_es='julio';
		when '08' then set v_mes_es='agosto';
		when '09' then set v_mes_es='septiembre';
		when '10' then set v_mes_es='octubre';        
		when '11' then set v_mes_es='noviembre';
		when '12' then set v_mes_es='diciembre';
		else set v_mes_es='error';
    end case;
    select concat(v_diasemana_es,' , ',v_diames,' de ', v_mes_es, ' de ' ,v_anho);
end
/
delimiter ;

call pr_fechaLarga('2013-02-1');