use ventas;

/*  FUNCIONES  */
-- 1. Crea una función que devuelva el siguiente número de departamento que se deberá asignar cuando se registre un nuevo departamento (que funcione como un auto_increment para el dep_no), es decir, cuando se llama a la función, devolverá el máximo número de departamento existente en la tabla más 1.
drop function if exists fn_next_iddepto;
delimiter /
create function fn_next_iddepto() returns int
reads sql data
begin
	declare v_retorno int default 1;
    select max(dep_no) into v_retorno from departamentos;
    set v_retorno=v_retorno+10;
    return ifnull(v_retorno,10);
end;
/
delimiter ;
select fn_next_iddepto();
select * from departamentos;
insert into departamentos(dep_no,dnombre,localidad) values (fn_next_iddepto(),'GESTION','SANTIAGO');
-- 2. Crea una función que devuelva el nombre del jefe de un empleado. La función aceptará como parámetro un número de empleado y buscará el nombre del jefe de ese empleado y devolverá ese valor.
drop function if exists fn_busca_jefe;
delimiter /
create function fn_busca_jefe(par_emp int) returns varchar(100)
reads sql data
begin
	declare v_retorno varchar(100);
    select jefe.apellido into v_retorno
    from empleados emp, empleados jefe where emp.director=jefe.emp_no
    and emp.emp_no=par_emp;
    return v_retorno;
end;
/
delimiter ;
select * from empleados;
select fn_busca_jefe(7499);
-- 3. Modifica la función anterior para que en caso de que no encuentre un jefe válido devuelva el valor “Jefe no encontrado”.
drop function if exists fn_busca_jefe;
delimiter /
create function fn_busca_jefe(par_emp int) returns varchar(100)
reads sql data
begin
	declare v_retorno varchar(100);
    select jefe.apellido into v_retorno
    from empleados emp, empleados jefe where emp.director=jefe.emp_no
    and emp.emp_no=par_emp;
    return ifnull(v_retorno,'Jefe no encontrado');
end;
/
delimiter ;
select *,fn_busca_jefe(emp_no) from empleados;
select fn_busca_jefe(749);

-- PROCEDIMIENTOS
-- 4. Desarrollar un procedimiento que visualice el apellido, el nombre del jefe y la fecha de alta de todos los empleados ordenados por apellido. Para mostrar el nombre del jefe deberá utilizar la función creada en el ejercicio 3.
drop procedure if exists pr_ver_emp;
delimiter /
create procedure pr_ver_emp()
begin
	select apellido,fn_busca_jefe(emp_no),fecha_alta from empleados order by apellido;
end;
/
delimiter ;
call pr_ver_emp();

-- 5. Escribir un programa que visualice el apellido y el salario de los cinco empleados que tienen el salario más alto. Sin usar LIMIT
drop procedure if exists pr_cinco_ricos;
delimiter /
create procedure pr_cinco_ricos()
begin
	declare v_salida int default 0;
    declare v_apellido varchar(100);
    declare v_salario decimal(8,2);
    declare v_contador int default 0;
    declare c_datos cursor for select apellido,salario from empleados order by salario desc;
    declare continue handler for not found set v_salida=1;
    
    open c_datos;
    loop1:loop
		fetch c_datos into v_apellido,v_salario;
        select v_apellido,v_salario;
        if v_contador>=5 or v_salida=1 then 
			leave loop1;
		end if;
        set v_contador=v_contador+1;
	end loop;
    close c_datos;
end;
/
delimiter ;
call pr_cinco_ricos();

-- 6. Crea un procedimiento que acepte un número de departamento y devuelva mediante dos parámetros de tipo OUT su nombre y localidad.
drop procedure if exists pr_info_depto;
delimiter /
create procedure pr_info_depto(in par_depno int,out par_nombre varchar(100),out par_localidad varchar(100))
begin
	select dnombre,localidad into par_nombre, par_localidad
    from departamentos where dep_no=par_depno;
end;
/
delimiter ;
call pr_info_depto(50,@nombre,@loc);
select @nombre, @loc;
select * from departamentos;

-- 7. Crea un procedimiento que acepte un número de departamento como argumento y visualice todos los datos de los empleados que trabajan en ese departamento.
drop procedure if exists pr_emps_depto;
delimiter /
create procedure pr_emps_depto(par_depto int)
begin
	select dnombre from departamentos where dep_no=par_depto;
	select * from empleados where dep_no=par_depto;
end;
/
delimiter ;
call pr_emps_depto(10);
-- 8. Codifica un procedimiento que muestre el nombre de cada departamento y el número de empleados que tiene.
drop procedure if exists pr_emps_deptos;
delimiter /
create procedure pr_emps_deptos()
begin
	declare v_salida int default 0;
    declare v_depto int;
    declare v_depno varchar(100);
    declare c_deptos cursor for select dep_no,dnombre from departamentos;
	declare continue handler for not found set v_salida=1;
	open c_deptos;
    loop1:loop
		fetch c_deptos into v_depto,v_depno;
        if v_salida=1 then
			leave loop1;
		end if;
        select v_depno,count(*) from empleados where dep_no=v_depto;
	end loop;
    close c_deptos;
end;
/
delimiter ;
call pr_emps_deptos();
-- 9. Modifica el procedimiento anterior para que tras mostrar el nombre y número de empleados de cada departamento muestre un listado de los nombres de todos los empleados de ese departamento.
drop procedure if exists pr_emps_deptos;
delimiter /
create procedure pr_emps_deptos()
begin
	declare v_salida int default 0;
    declare v_depto int;
    declare v_depno varchar(100);
    declare c_deptos cursor for select dep_no,dnombre from departamentos;
	declare continue handler for not found set v_salida=1;
	open c_deptos;
    loop1:loop
		fetch c_deptos into v_depto,v_depno;
        if v_salida=1 then
			leave loop1;
		end if;
        select v_depno,count(*) from empleados where dep_no=v_depto;
        select apellido from empleados where dep_no=v_depto;
	end loop;    
    close c_deptos;
end;
/
delimiter ;
call pr_emps_deptos();

-- 10. Escribir un procedimiento que reciba una cadena y visualice el apellido y el número de empleado de todos los empleados cuyo apellido contenga la cadena especificada. Al finalizar mostrará el número de empleados incluidos en el listado.
drop procedure if exists pr_busca_emp;
delimiter /
create procedure pr_busca_emp(par_cadena varchar(20))
begin
	select emp_no,apellido from empleados where apellido like concat('%',par_cadena,'%');
    select count(*) from empleados where apellido like concat('%',par_cadena,'%');
end;
/
delimiter ;
call pr_busca_emp('O');
delimiter /
create procedure pr_busca_emp2(par_cadena varchar(20))
begin
	declare v_cont int default 1;
    declare v_salida int default 0;
    declare v_no int;
    declare v_ape varchar(100);
    declare c_datos cursor for select emp_no,apellido from empleados where apellido like concat('%',par_cadena,'%');
    declare continue handler for not found set v_salida=1;
	open c_datos;
    loop1:loop
		fetch c_datos into v_no,v_ape;
        set v_cont=v_cont+1;
        if v_salida=1 then
			leave loop1;
		end if;
	end loop;
    close c_datos;
end;
/
delimiter ;
call pr_busca_emp('O');

/* 11. Crea un procedimiento que reciba como parámetro una localidad y cree automáticamente 1 departamento en esa localidad con los siguientes valores:
	a. número de departamento: será el valor devuelto por la función creada en el ejercicio 1
	b. nombre: el nombre del departamento tendrá el formato “departamento X“, siendo X el valor asignado como número de departamento.
	c. Localidad: se asignará el valor pasado como parámetro.
*/
drop procedure pr_crea_depto;
delimiter /
create procedure pr_crea_depto(par_localidad varchar(100))
begin
	declare v_id int default fn_next_iddepto() ;
	insert into departamentos (DEP_NO,DNOMBRE,LOCALIDAD)
	values (v_id,concat('DEPARTAMENTO ',v_id),par_localidad);
end;
/
delimiter ;
call pr_crea_depto('ALGALIA DE ARRIBA');
select * from departamentos order by dep_no desc;

/* 12. Crea un procedimiento al que se pase como parámetro un número de departamento y cree 3 empleados en ese
 departamento con la siguiente información:
	a. EMP_NO: se debe cubrir por medio de una función similar a la empleada para el código del departamento.
	b. APELLIDO: se asignará la cadena “desconocido”.
	c. OFICIO: se asignará a los tres empleados el oficio que menos empleados de la empresa tengan.
	d. DIRECTOR: se les asignará como director al jefe de la empresa (al empleado que no tenga ningún jefe).
	e. FECHA_ALTA: fecha actual.
	f. SALARIO: se les asignará el salario mínimo de la empresa (el menor salario que haya en la empresa).
	g. COMISION: nula
	h. DEP_NO: se asignará el número de departamento que se pase como parámetro.
	i. TELEFONO: nulo
*/
drop function if exists fn_next_idemp;
delimiter /
create function fn_next_idemp() returns int
reads sql data
begin
	declare v_retorno int default 1;
    select max(emp_no) into v_retorno from empleados;
    set v_retorno=v_retorno+1;
    return ifnull(v_retorno,1);
end;
/
delimiter ;
drop procedure if exists pr_crea_emp;
delimiter /
create procedure pr_crea_emp(par_depto int)
begin
	declare v_id int ;
    declare v_oficio varchar(100);
    declare v_director int;
    declare v_sal decimal(8,2);
	declare v_cont int default 1;
    
    select min(salario) into v_sal
    from empleados;
    select emp_no into v_director
    from empleados where director is null;
    while v_cont <=3 do
		set v_id=fn_next_idemp();
		select oficio into v_oficio 
		from empleados group by oficio order by count(*) asc limit 1;
		insert into empleados (EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO,TELEFONO)
		values(v_id,'DESCONOCIDO',v_oficio,v_director,now(),v_sal,null,par_depto,null);
        set v_cont=v_cont+1;
	end while;
end;
/
delimiter ;
call pr_crea_emp(70);
select * from empleados order by dep_no desc;

-- 13. Modifica el procedimiento anterior para que si le pasamos un número de departamento que no exists, en lugar de abortar al tratar de insertar un empleado en un departamento no existente, devuelva el error ‘Departamento no existente’ y finalice correctamente (sin abortar).
drop procedure if exists pr_crea_emp;
delimiter /
create procedure pr_crea_emp(par_depto int)
begin
	declare v_id int ;
    declare v_oficio varchar(100);
    declare v_director int;
    declare v_sal decimal(8,2);
	declare v_cont int default 1;
    declare exit handler for 1452 select 'Departamento no existente';
    
    select min(salario) into v_sal
    from empleados;
    select emp_no into v_director
    from empleados where director is null;
    while v_cont <=3 do
		set v_id=fn_next_idemp();
		select oficio into v_oficio 
		from empleados group by oficio order by count(*) asc limit 1;
		insert into empleados (EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO,TELEFONO)
		values(v_id,'DESCONOCIDO',v_oficio,v_director,now(),v_sal,null,par_depto,null);
        set v_cont=v_cont+1;
	end while;
end;
/
delimiter ;
call pr_crea_emp(1);
-- 14. Modifica el procedimiento anterior para que no falle en caso de que no exista ningún oficio en la BD (al buscar el menos repetido no devolvería nada y trataría de insertar un oficio nulo). En este caso debería devolver el error ‘No es posible insertar un empleado sin oficio’ y finalizar. 
drop procedure if exists pr_crea_emp;
delimiter /
create procedure pr_crea_emp(par_depto int)
begin
	declare v_id int ;
    declare v_oficio varchar(100);
    declare v_director int;
    declare v_sal decimal(8,2);
	declare v_cont int default 1;
    declare exit handler for 1452 select 'Departamento no existente';
    
    select min(salario) into v_sal
    from empleados;
    select emp_no into v_director
    from empleados where director is null;
    while v_cont <=3 do
		set v_id=fn_next_idemp();
		select oficio into v_oficio 
		from empleados group by oficio order by count(*) asc limit 1;
        if v_oficio is null then
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No es posible insertar un empleado sin oficio';
		end if;
		insert into empleados (EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO,TELEFONO)
		values(v_id,'DESCONOCIDO',v_oficio,v_director,now(),v_sal,null,par_depto,null);
        set v_cont=v_cont+1;
	end while;
end;
/
delimiter ;
call pr_crea_emp(10);

-- 15. Crea un procedimiento que busque los departamentos que no tienen ningún empleado, y en cada uno de ellos inserte 3 empleados empleando el procedimiento anterior.
drop procedure if exists pr_llena_deptos;
delimiter /
create procedure pr_llena_deptos()
begin
	declare v_salida int default 0;
    declare v_depno int;
    declare c_datos cursor for select dep_no from departamentos where dep_no not in (select ifnull(dep_no,0) from empleados);
    declare continue handler for not found set v_salida=1;
    
    open c_datos;
    loop1:loop
		fetch c_datos into v_depno;
        if v_salida=1 then
			leave loop1;
		end if;
        call pr_crea_emp(v_depno);
	end loop;
    close c_datos;
end;
/
delimiter ;
select dep_no from departamentos where dep_no not in (select ifnull(dep_no,0) from empleados);
select * from empleados;
call pr_llena_deptos();

/*  TRIGGERS  */
-- 16. Crea un trigger en la tabla empleados que asigne automáticamente la fecha actual al empleado en la fecha de contratación si la fecha de contratación que se ha indicado es nula.
drop trigger tg_ins_emp;
delimiter /
create trigger tg_ins_emp before insert on empleados
for each row
begin
	if new.fecha_alta is null then
		set new.fecha_alta=now();
	end if;
end;
/
delimiter ;
select * from empleados order by emp_no desc;
insert into empleados (emp_no, apellido,oficio,director,fecha_alta,salario,comision,dep_no,telefono)
values (9017,'DESCONOCIDO','VENDEDOR',7839,null,1000.00,null,70,null);

select * from information_schema.triggers;
show create trigger tg_del_emp;

-- 17. Modifica el trigger anterior para que impida insertar empleados con número de empleado mayor que 1000.
drop trigger tg_ins_emp;
delimiter /
create trigger tg_ins_emp before insert on empleados
for each row
begin
	if new.fecha_alta is null then
		set new.fecha_alta=now();
	end if;
    if new.emp_no>1000 then 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No es posible insertar un empleado con codigo mayor que 1000';
	end if;
end;
/
delimiter ;
-- 18. ¿Qué ocurriría si intentas en lugar de modificar el trigger anterior creas un nuevo trigger before insert en empleados?
delimiter /
create trigger tg_ins_emp2 before insert on empleados
for each row
begin
	if new.fecha_alta is null then
		set new.fecha_alta=now();
	end if;
    if new.emp_no>1000 then 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No es posible insertar un empleado con codigo mayor que 1000';
	end if;
end;
/
delimiter ;
-- Mysql permite crear varios triggers para la misma acción

-- 19. Añade un campo “importe” en la tabla PEDIDO de tipo numeric(9,2). Crea un disparador que se ejecute cada vez que se inserte o modifique una fila en la tabla PEDIDO que calcule el valor del campo importe de la siguiente manera: importe=precio del producto por cantidad. El precio del producto tendrás que buscarlo en la tabla PRODUTO.
alter table pedidos add importe decimal(9,2);
drop trigger tg_ins_pedido;
delimiter /
create trigger tg_ins_pedido before insert on pedidos
for each row
begin
	declare v_precio decimal(8,2);
    select precio_actual into v_precio
    from productos where producto_no=new.producto_no;
	set new.importe=ifnull(v_precio,0)*new.unidades;
end;
/
create trigger tg_upd_pedido before update on pedidos
for each row
begin
	declare v_precio decimal(8,2);
    select precio_actual into v_precio
    from productos where producto_no=new.producto_no;
	set new.importe=ifnull(v_precio,0)*new.unidades;
end;
/
delimiter ;
-- 20. Crea una tabla empleados_historico que guarde los registros de los empleados borrados; junto con los atributos del empleado cada registro guardará el nombre del usuario que hace el borrado y la fecha de borrado. Crea un trigger que se ejecute antes de hacer el borrado de un empleado y guarde en la tabla empleados_historico cada empleado que es borrado.
create table empleados_historico like empleados;
alter table empleados_historico add usuario varchar(50);
alter table empleados_historico add fecborrado timestamp;

drop trigger if exists tg_del_emp;
delimiter /
create trigger tg_del_emp after delete on empleados
for each row
begin
	insert into empleados_historico(EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO,TELEFONO,usuario,fecborrado)
    values (old.EMP_NO,old.APELLIDO,old.OFICIO,old.DIRECTOR,old.FECHA_ALTA,old.SALARIO,old.COMISION,old.DEP_NO,old.TELEFONO,CURRENT_USER(),now());
end;
/
delimiter ;
select * from empleados order by emp_no desc;
select * from empleados_historico;
delete from empleados where emp_no>9014;
-- 21. Crea un TRIGGER BEFORE INSERT que cuando se inserta un empleado sin salario le asigne el salario más bajo de cualquier empleado de la empresa (calculado sin tener en cuenta los salarios NULL o 0).
drop trigger if exists tg_ins_emp;
delimiter /
create trigger tg_ins_emp3 before insert on empleados
for each row 
begin
	declare v_sal decimal(8,2);
    if new.salario is null then
		select min(salario) into v_sal from empleados;
        set new.salario=v_sal;
	end if;
end;
/
delimiter ;

-- 22. Elabora un mecanismo que impida que se inserte un registro en la tabla de pedidos cuando el número de unidades pedidas de un producto supera el stock disponible de ese producto. Además mostrará un mensaje indicando que no se puede realizar el pedido por falta de stock. En caso de que se pueda realizar el pedido restará al stock del producto la cantidad comprada en el pedido.
desc productos;
delimiter /
create trigger tg_ins_pedido before insert on pedidos
for each row
begin
	declare v_stock float default 0;
    select STOCK_DISPONIBLE into v_stock from productos where producto_no=new.producto_no;
    if v_stock<new.unidades then
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No hay stock suficiente';
	else
		update productos set stock_disponible=stock_disponible-new.unidades where producto_no=new.producto_no;
    end if;
end;
/
delimiter ;

-- 23. Indica cómo se puede conseguir lo siguiente: Deseamos evitar que se borre un empleado mientras sea jefe de otros empleados
alter table empleados
add constraint FK_jefe_emp foreign key (director) references empleados(emp_no)
on delete restrict;

-- 24. Indica cómo se puede conseguir lo siguiente: Deseamos evitar que un empleado pueda ser jefe de más de 3 empleados (no se permitirá introducir o modificar a un empleado si su nuevo jefe ya tiene 3 subordinados, además mostrará un mensaje de error apropiado)
delimiter /
create trigger tg_ins_emp5 before insert on empleados
for each row
begin
	declare v_numemp int;
    select count(*) into v_numemp from empleado where director=new.director;
    if v_numemp>=3 then 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Un empleado no puede ser jefe de más de 3 compañeros';
	end if;
end;
/
-- Tiene que ser mayor o igual porque como el trigger es before, cuando hace la comprobación no está contanto el registro actual
delimiter ;
drop trigger tg_upd_emp5;
delimiter /
create trigger tg_upd_emp5 before update on empleados
for each row
begin
	declare v_numemp int;
    select count(*) into v_numemp from empleados where director=new.director;
    if v_numemp>=3 then 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Un empleado no puede ser jefe de más de 3 compañeros';
	end if;
end;
/
delimiter ;
-- Tiene que ser mayor o igual porque como el trigger es before, cuando hace la comprobación no está contanto el registro actual
update empleados set director=9009 where emp_no=9004;
-- 25. Indica cómo se puede conseguir lo siguiente: Deseamos evitar que por error se introduzca el mismo producto varias veces en la tabla de productos (aunque sea de distintos fabricantes).
alter table productos add constraint UQ_producto unique (descripcion);