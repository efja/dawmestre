-- DESDE CMD:
C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql -u alumno -p nba < c:\users\alumno\script.sql


use pruebas;

/* VARIABLES */
	drop procedure if exists pr_prueba_variables;
	delimiter /
	create procedure pr_prueba_variables()
	begin
		declare v_entero1,v_entero2,v_entero3 int;
		declare v_entero4 int default -4000;
		declare v_real1 float default 344.67;
		declare v_real2 float default 1.5E14;
		declare v_real3 numeric(7,2) default 4561.44;
		declare v_caracter1 char(1) default 'Y';
		declare v_caracter2 varchar(20);
		declare v_fecha1 date default '1996-11-03';
		declare v_fecha2 date;
		declare v_enum enum('metalico','tarjeta','transferencia');
		
		set @v_caracter3='N';
		set v_enum='tarjeta';
		select current_date into v_fecha2;
		
		select v_entero4;
		select v_real3;
		select @v_caracter3;
		select v_fecha2;
		select v_enum;
	end;
	/
	delimiter ;

	call pr_prueba_variables();

/* PARAMETROS */
	drop procedure if exists pr_prueba_parametros;
	delimiter /
	create procedure pr_prueba_parametros(IN p_par1 int, INOUT p_par2 int,out p_par3 int)
	begin
		select 'resultado antes de cambiar nada: ',p_par1,p_par2,p_par3;
		set p_par2=p_par2+1;
		set p_par3=p_par1+p_par2;
		select 'resultado tras cambiar datos: ',p_par1,p_par2,p_par3;
	end;
	/
	delimiter ;
	set @entrada_salida=1;
	call pr_prueba_parametros(5,@entrada_salida,@salida);
	select 'resultado tras la ejecuci�n del procedimiento: ',@entrada_salida,@salida;

/* CONDICIONAL */
	drop procedure if exists pr_prueba_condicion;
	delimiter /
	create procedure pr_prueba_condicion(p_par1 int)
	begin
		declare v_aux int default 1;
		declare v_div int default 0;

		if (p_par1 % 2 =0) then
			select 'el par�metro es m�ltiplo de dos';
			set v_aux=2;
		elseif (p_par1 % 3=0) then 
			select 'el par�metro es m�ltiplo de tres';
			set v_aux=3;
		else 
			select 'el parametro no es multiplo ni de dos ni de tres';
			set v_aux=5;
		end if;
		
		case when v_aux in (2,3) then
			select concat('el valor de aux es ',v_aux);
		when v_aux>0 then	
			select 'aux es mayor que cero';
		else
			select 'aux es menor o igual a cero';
		end case;
	end;
	/
	delimiter ;
	call pr_prueba_condicion(4);
	-- call pr_prueba_condicion(6);
	-- call pr_prueba_condicion(9);

/* BUCLES */
	drop procedure if exists pr_prueba_bucles;
	delimiter /
	create procedure pr_prueba_bucles(p_limite int)
	begin
		declare v_contador int default 0;
		drop table if exists temp;
		create table temp (num int primary key);
		-- bucle WHILE: comprueba la condici�n antes de entrar en el bucle (si no se cumple, no entra)
		while v_contador<p_limite do 
			insert into temp (num) values (v_contador);
			set v_contador=v_contador+1; -- OJO: siempre hay que incrementar la variable contador!!!
		end while;
		select * from temp;
		drop table temp;
		create table temp (num int primary key);  
		set v_contador=0;
		-- buble REPEAT: primero entra en el bucle y luego comprueba la condici�n
		repeat
			insert into temp(num) values (v_contador);
			set v_contador=v_contador+1; -- OJO: siempre hay que incrementar la variable contador!!!
		until v_contador>=p_limite end repeat;
		select * from temp;
		drop table temp;
		create table temp (num int primary key);  
		set v_contador=0;
		-- bucle LOOP: primero entra en el bucle y luego comprueba la condici�n
		bucle:loop
			insert into temp(num) values(v_contador);
			set v_contador=v_contador+1;
			if (v_contador)>=p_limite then leave bucle;
			end if;
		end loop;
		select * from temp;
	end;
	/
	delimiter ;
	
	call pr_prueba_bucles(0);
	--call pr_prueba_bucles(10);

/* EXCEPCIONES */
	drop procedure if exists pr_prueba_excepciones;
	delimiter /
	create procedure pr_prueba_excepciones()
	begin
		declare v_ultima_fila int default 0;
		declare v_aux int;
		declare v_error varchar(50);
		declare c_temp cursor for select * from temp;    
		declare continue handler for not found 
		begin
			set v_ultima_fila=1;
		end;
		DECLARE continue HANDLER FOR SQLEXCEPTION BEGIN
			GET DIAGNOSTICS CONDITION 1
			v_coderror = RETURNED_SQLSTATE, v_msgerror = MESSAGE_TEXT;
			select v_msgerror;
		END;
		declare exit handler for 1366 begin
			select ('asignamos un char a un int y salimos');
		end;
		
		set v_error='Division entre 0';
		set v_aux= 1 div 0;
		set v_error='Insertamos un valor nulo en una PK';
		insert into temp (num) values (null);
		set v_error='insertamos un valor duplicado en una PK';
		insert into temp (num) values (1);
		set v_error='NOTFOUND';
		open c_temp;
		ini_bucle:loop
			fetch c_temp into v_aux;
			if v_ultima_fila=1 then 
				leave ini_bucle;
			end if;
			select concat('Se coge el valor ',v_aux) as 'dentro del bucle';
		end loop;
		close c_temp;
		insert into temp (num) values ('hola');
		select 'ejecutamos la ultima instruccion';
	end;
	/
	delimiter ;

	call pr_prueba_excepciones();

/* TRANSACCIONES */
	drop procedure if exists pr_prueba_transacciones;
	delimiter /
	create procedure pr_prueba_transacciones(par_entrada int)
	begin
		declare v_ultima_fila int default 0;
        declare v_error varchar(100);
        declare v_aux int;
        declare c_temp cursor for select * from temp;
		declare continue handler for not found 
		begin
			set v_ultima_fila=1;
            select 'salimos del bucle y comitamos';
            commit;
		end;
		DECLARE continue HANDLER FOR SQLEXCEPTION BEGIN
			select v_error as resultado;
			rollback;
		END;
		declare exit handler for 1366 begin
			select ('asignamos un char a un int y salimos');
            rollback;
		end;
		
		start transaction;
        select concat('M�ximo valor de temp:',max(num)) from temp;
        insert into temp (num) select max(num)+1 from temp;
		case 
			when par_entrada=1 then begin
				set v_error='Insertamos un valor nulo en una PK';
                insert into temp (num) values (null);
			end;
			when par_entrada=2 then begin
				set v_error='recorremos el cursor y generamos el NOT FOUND';
                open c_temp;
				ini_bucle:loop
					fetch c_temp into v_aux;
					if v_ultima_fila=1 then 
						leave ini_bucle;
					end if;
				end loop;
				close c_temp;			
			end;
			when par_entrada=3 then begin
				set v_error='insertamos una cadena en un campo de tipo num�rico';
				insert into temp (num) values ('hola');
			end;
		end case;
		commit;
	end;
	/
	delimiter ;

	call pr_prueba_transacciones(1);
    call pr_prueba_transacciones(2);
    call pr_prueba_transacciones(3);
	select max(num) from temp; 
	
/* CURSORES */
	drop procedure if exists pr_prueba_cursor;
	delimiter /
	create procedure pr_prueba_cursor()
	begin
		declare v_aux int;
		declare v_aux2 int;
		declare v_salida int default 0;
		declare v_contador int default 0;
		declare c_simple cursor for select max(num) from temp;
		declare c_complejo cursor for select num, num as num2 from temp;
		declare continue handler for not found set v_salida=1;
		
		open c_simple;
		fetch c_simple into v_aux;
		select concat('el cursor simple devuelve un solo valor: ',v_aux) as resultado;
		close c_simple;
		open c_complejo;
		loop1:loop
			fetch c_complejo into v_aux, v_aux2;
			select concat('Pasada ',v_contador,' del bucle. Valores: ',v_aux, ',',v_aux2) as resultado;
			set v_contador=v_contador+1;
			if (v_salida=1) then 
				leave loop1;
			end if;
		end loop;
	end;
	/
	delimiter ;

	call pr_prueba_cursor();
	
/* FUNCIONES */
	drop function if exists fn_prueba_func;
	delimiter /
	create function fn_prueba_func() returns varchar(20)
    begin
		declare v_fecha date default date(now());
		declare v_resultado varchar(20) default '';
		case DAYOFWEEK(v_fecha) 
		when 2 then
			set v_resultado='Lunes';
		when 3 then	
			set v_resultado='Martes';
		when 4 then	
			set v_resultado='Miercoles';
		when 5 then
			set v_resultado='Jueves';
		when 6 then 
			set v_resultado='Viernes';
		else
			set v_resultado='Fin de semana';
		end case;
		return v_resultado;
	end;
	/
	delimiter ;
    
    select fn_prueba_func();

/* PROCEDIMIENTOS */
	drop procedure if exists pr_prueba_proc;
	delimiter /
	create procedure pr_prueba_proc()
	begin
		declare v_dia varchar(20);
		set v_dia=fn_prueba_func();
		if v_dia in ('Lunes','Martes') then select 'Lunes o martes';
		elseif v_dia in ('Miercoles','Jueves') then select 'Mi�rcoles o Jueves';
		else select 'Fin de semana';
		end if;
	end;
	/
	delimiter ; 
    
    call pr_prueba_proc();
	
/* TRIGGERS */
    drop table if exists temp2;
    create table temp2(msg varchar(30));
	
	drop trigger if exists tg_prueba_triggers;
	delimiter /
	create trigger tg_prueba_triggers
	before delete on temp
	for each row 
    begin
		declare exit handler for sqlexception 
        begin
            -- rollback;
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Se ha producido una excepcion'; -- Para "simular" el rollback, generamos una excepcion que impide el borrado
        end;
        -- select 'Esto da un error, un trigger no puede devolver datos';
        insert into temp2(msg) values (concat('Se va a borrar ',OLD.num));
        insert into temp2(msg) values ('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'); -- metemos un valor muy grande para que genere la excepcion
	end;
    /
	delimiter ;
	delete from temp where num=11;-- Si no se realiza el borrado (el valor no existe), no se ejecuta el trigger
    delete from temp where num=1;
	select * from temp where num=1;
