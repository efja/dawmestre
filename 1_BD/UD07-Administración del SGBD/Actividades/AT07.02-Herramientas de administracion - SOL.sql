/*1.	En primer lugar, vamos a cambiar el nombre de la base de datos �emple01� por �Empleados�. Este cambio no podemos hacerlo directamente por medio de
comandos, con lo cual lo que haremos ser� utilizar mysqldump para exportar la BD original e importarla en la BD nueva:
-	Exporta la base de datos �emple01� en un fichero llamado �copia1.sql�.
-	Crea una nueva base de datos llamada �Empleados�.
-	Importa en la BD �Empleados� el fichero �copia1.sql�.
-	Borra la base de datos �emple01�. */
mysqldump -u alumno --password=alumno --databases emple01 > copia1.sql
mysql -u alumno -p < copia1.sql
drop database if exists emple01;

-- 2.	Crea una nueva copia de seguridad de la base de datos Empleados que se almacene en el fichero �copia2.sql� y guarde �nicamente la tabla �depto�. 
mysqldump -u alumno --password=alumno --databases empleados --tables depto > copia2.sql

/* 3.	Crea una nueva copia de seguridad de la base de datos Empleados que almacene cada tabla en un fichero diferente, y guarde todos ellos en una carpeta �copia3�. En la copia no se deben incluir los datos ni los procedimientos. */
mysqldump -u alumno -p --no-data --routines=false empleados --tab=c:/users/alumno/copia3
/* Cuando generamos el export con datos, crea dos ficheros: un .sql con la estructura de la tabla, y un .txt con los datos. El sql lo crea con el usuario del sistema operativo que estamos usando (wadmin), pero el txt lo crea con el usuario que gestiona el servicio de Mysql (Servicio de red), que no tiene permisos para escribir en esa carpeta, por lo que da un error.
Para que no fallase el export con datos, hay que dar previamente al usuario "Servicio de red" permisos de control total sobre la carpeta "copia3", y sino habr�a que ejecutarlo con la carpeta "c:\program files\Mysql\mysql server 8.0\Uploads" en lugar de copia3.*/


-- 4.	Crea una base de datos Emple02 e importa en ella el fichero �copia2.sql�.
create database emple02;
mysql -u alumno --password=alumno emple02 < copia2.sql

-- 5.	Crea una base de datos Emple03 e importa en ella el contenido de la carpeta �copia3�.
create database emple03;
mysql -u alumno --password=alumno emple03 < c:/users/alumno/copia3/emp.sql
mysql -u alumno --password=alumno emple03 < c:/users/alumno/copia3/depto.sql

-- 6.	Repite las operaciones anteriores empleando MysqlWorkbench

-- 7.	�C�mo programar�as el backup autom�tico de la base de datos �empregados� para que copie todo el contenido de la BD todos los domingos a las 00:30?
mysqldump -u alumno --password=alumno --databases empregados > backup.sql


-- 8. Haz un backup de las bases de datos: empleados, almacen_farmacia y empresa_ventas
mysqldump -u alumno --password=alumno --databases empleados almacen_farmacia empresa_ventas > backup.sql

-- 9. Borra la base de datos empresa_ventas, vu�lvela a crear e importa en ella todos los datos del backup
drop database empresa_ventas;
create database empresa_ventas;
mysql -u alumno --password=alumno --one-database empresa_ventas < backup.sql
