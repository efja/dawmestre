use empresa_ventas;
-- 1. Usuario: emp01. Permisos: leer en todas las tablas
create user emp01 identified by "emp01";
grant select on empresa_ventas.* to emp01;

-- 2. Usuario: emp02. Permisos: leer en tabla clientes
create user emp02 identified by "emp02";
grant select on empresa_ventas.clientes to emp02;

-- 3. Usuarios: emp03 y emp04. Permisos: leer, y actualizar datos en las tablas cliente y empleados
create user emp03 identified by "emp03";
create user emp04 identified by "emp04";
grant select, update on empresa_ventas.clientes to emp03,emp04;
grant select, update on empresa_ventas.empleados to emp03,emp04;

-- 4. Cambia la contraseña del usuario emp04 por 123.
set password for emp04="123.";
select * from mysql.user where user='emp04';

-- 5. Usuario: emp05. Permisos: todos los permisos en las tablas cliente y empleados, excepto conceder permisos a otros usuarios.
create user emp05 identified by "emp05";
grant all on empresa_ventas.clientes to emp05;
grant all on empresa_ventas.empleados to emp05;
select * from mysql.tables_priv where user='emp05';

/* 6. Usuario: emp06. Permisos:
- todos los permisos en la tablas clientes, y además conceder permisos a otros usuarios.
- Mostrar los datos de la tabla empleados; */
create user emp06 identified by "emp06";
grant all on empresa_ventas.clientes to emp06 with grant option;
grant select on empresa_ventas.empleados to emp06;

/* 7. Usuario: emp07. Permisos:
- ver el IdEmpleado, apellido y oficio de los empleados.
- Modificar el oficio de los empleados */
create user emp07 identified by "emp07";
grant select (idempleado,apellido,oficio) on empresa_ventas.empleados to emp07;
grant update(oficio) on empresa_ventas.empleados to emp07;

/* 8 - Usuarios: emp08, emp09. Permisos:
- Pueden ver todos los atributos de todas las tablas (excepto el salario y comisión de los empleados)
- Puede realizar cualquier acción con la tabla pedidos
- Puede modificar el stock de los productos. */
create user emp08 identified by "emp08";
create user emp09 identified by "emp09";
select concat('grant select on empresa_ventas.',table_name,' to emp08,emp09;') from information_schema.tables where table_schema='empresa_ventas' and table_name !='empleados';
grant select on empresa_ventas.clientes to emp08,emp09;
grant select on empresa_ventas.departamentos to emp08,emp09;
grant select on empresa_ventas.pedidos to emp08,emp09;
grant select on empresa_ventas.productos to emp08,emp09;
grant select (IdEmpleado,APELLIDO,OFICIO,IdJefe,FECHA_ALTA,IdDepartamento,TELEFONO) on empresa_ventas.empleados to emp08,emp09;
create or replace view empresa_ventas.v_emp89 as select IdEmpleado,APELLIDO,OFICIO,IdJefe,FECHA_ALTA,IdDepartamento,TELEFONO from empresa_ventas.empleados;
grant select on empresa_ventas.v_emp89 to emp08,emp09;
grant all on empresa_ventas.pedidos to emp08,emp09;
grant update(STOCK_DISPONIBLE) on empresa_ventas.productos to emp08,emp09;

/* 9 - Usuario: emp10. Permisos:
- Tendrá control total sobre todas las BD si se conecta de manera local al servidor, con posibilidad de
conceder permisos a otros usuarios.
- Tendrá control total sobre las tablas empresa.empleados y empresa.clientes si se conecta desde la IP que
utilices en la máquina real para conectarte al servidor. */
create user emp10@localhost identified by "emp10";
grant all on *.* to emp10@localhost with grant option;
create user emp10@'10.0.2.15' identified by "emp10";
grant all on empresa_ventas.clientes to emp10@10.0.2.15;
grant all on empresa_ventas.empleados to emp10@10.0.2.15;

-- 10. Quita al usuario emp10 todos los permisos que tenía sobre la tabla empleados
revoke all on *.* from emp10@localhost;
-- Damos permisos en todas las BDs excepto empresa_ventas
select distinct concat('grant all on ',table_schema,'.* to emp10@localhost;')  from information_schema.tables where table_schema!='empresa_ventas';
grant all on almacen_farmacia.* to emp10@localhost;
grant all on dwlg2019.* to emp10@localhost;
grant all on emple02.* to emp10@localhost;
grant all on emple03.* to emp10@localhost;
grant all on empleados.* to emp10@localhost;
grant all on information_schema.* to emp10@localhost;
grant all on lg2019.* to emp10@localhost;
grant all on mysql.* to emp10@localhost;
grant all on performance_schema.* to emp10@localhost;
grant all on sys.* to emp10@localhost;
-- y damos permisos a las tablas (excepto empleados) en empresa_ventas
grant all on empresa_ventas.clientes to emp10@localhost;
grant all on empresa_ventas.departamentos to emp10@localhost;
grant all on empresa_ventas.pedidos to emp10@localhost;
grant all on empresa_ventas.productos to emp10@localhost;
revoke all on empresa_ventas.empleados from emp10@10.0.2.15;
-- 11. Modifica el usuario emp08 para que no pueda modificar ningún dato de la BD.
revoke update on empresa_ventas.pedidos from emp08;
revoke update(STOCK_DISPONIBLE) on empresa_ventas.productos from emp08;
show grants for emp08;
-- 12. Borra el usuario emp09.
drop user emp09;
-- 13. El usuario emp10 está de baja temporal y en lugar de borrarlo debemos impedir que se conecte a la BD, para ello no tendrá ningún permiso sobre ninguna BD.
alter user emp10@localhost account lock;
alter user emp10@'10.0.2.15' account lock;
select * from mysql.user where user like 'emp10';
-- 14. De que 2 maneras podemos hacer que el SGBD haga efectivos los cambios de permisos que hemos introducido 
flush privileges;
-- reiniciando el SGBD

/* 15. Usuario: emp11. Permisos:
- puede hacer SELECT sobre la tabla empleados cuando se conecta desde el propio servidor.
- puede hacer SELECT sobre la tabla pedidos cuando se conecta desde el equipo 10.0.0.2
- puede hacer SELECT sobre la tabla clientes cuando se conecta desde cualquier lugar. */
create user emp11@localhost identified by "emp11";
grant select on empresa_ventas.empleados to emp11@localhost;
create user emp11@'10.0.0.2' identified by "emp11";
grant select on empresa_ventas.pedidos to emp11@10.0.0.2;
create user emp11@'%' identified by "emp11";
grant select on empresa_ventas.clientes to emp11;
/* 16. En estas condiciones,
- ¿puede emp11 hacer SELECT sobre la tabla clientes cuando se conecta desde el propio servidor? - NO
- ¿puede emp11 hacer SELECT sobre la tabla clientes cuando se conecta desde el equipo 10.0.0.2? - NO
- ¿puede emp11 hacer SELECT sobre la tabla clientes cuando se conecta desde un equipo que se encuentra en la red 192.168.26.0 / 24? - SI
*/ 

/*17. Usuario: emp12. Permisos:
- puede hacer SELECT sobre los siguientes atributos de la tabla empleados (IdEmpleado,apellido,oficio) cuando se conecta desde el propio servidor. */
create user emp12@localhost identified by "emp12";
grant select (idempleado,apellido,oficio) on empresa_ventas.empleados to emp12@localhost;

-- 18. Revoca al usuario el permiso de ver el oficio en la tabla empleados
revoke select(oficio) on empresa_ventas.empleados from emp12@localhost;
select * from mysql.columns_priv where user='emp12';
-- 19. Revoca al usuario todos los permisos sobre todas las bases de datos.
revoke all on *.* from emp12@localhost;
select * from mysql.user where user='emp12';
select * from mysql.db where user='emp12';
select * from mysql.tables_priv where user='emp12';
select * from mysql.columns_priv where user='emp12';
show grants for emp12@localhost;
-- 20. Usuario: emp13. Permisos: 
-- puede hacer SELECT, UPDATE y DELETE sobre las tablas empleados, departamentos, pedidos cuando se conecta desde cualquier lugar.
create user emp13 identified by "emp13";
grant select on empresa_ventas.empleados to emp13;
grant select on empresa_ventas.departamentos to emp13;
grant select on empresa_ventas.pedidos to emp13;
-- 21. Revoca al usuario emp13 todos los permisos sobre todas las bases de datos.
revoke all on *.* from emp13;
show grants for emp13;
-- 22. Mostrar con SELECT los usuarios del SGBD que empiezan por emp (nombre, equipo desde el que se puede conectar y hash del password).
select user,host ,authentication_string from mysql.user where user like 'emp%';
-- 23. Elimina todos los usuarios que empiecen por “emp”.
select concat('drop user ',user,'@\'',host,'\';') from mysql.user where user like 'emp%';
drop user emp03@'%';
drop user emp04@'%';
drop user emp05@'%';
drop user emp06@'%';
drop user emp07@'%';
drop user emp08@'%';
drop user emp11@'%';
drop user emp13@'%';
drop user emp11@'10.0.0.2';
drop user emp10@'10.0.2.15';
drop user emp10@'localhost';
drop user emp11@'localhost';
drop user emp12@'localhost';