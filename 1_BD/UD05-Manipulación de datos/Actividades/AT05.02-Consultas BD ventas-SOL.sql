use ventas;

-- 1. Obtén el número de empleado, el apellido y el número de departamento de todos los empleados de la tabla empleados.
SELECT EMP_NO,APELLIDO,DEP_NO FROM EMPLEADOS;
-- 2. Mostrar la siguiente información de todos los empleados: apellido, la fecha de alta, el salario y el salario incrementado por una gratificación de 1000€.
SELECT APELLIDO,FECHA_ALTA,SALARIO,SALARIO+1000 as nuevo_sal from empleados;
-- 3. Obtén todas las filas de la tabla empleados para los que en la columna OFICIO aparezca el valor 'VENDEDOR'.
select * from empleados where oficio like '%VENDEDOR%';
-- 4. Visualizar la expresión formada por las columnas APELLIDO, SALARIO, COMISION, y una columna que muestre lo que gana juntando el salario y la comisión de la tabla empleados
select apellido,salario,comision,salario+ifnull(comision,0) as sal_total from empleados;
-- 5. APELLIDO y OFICIO de los empleados sin comisión.
select apellido,oficio from empleados where comision is null;
-- 6. Mostrar el apellido y salario de los tres empleados con mayor salario
select apellido,salario from empleados order by salario desc limit 3;
-- 7. Empleados cuyo OFICIO sea VENDEDOR y que además su SALARIO sea estrictamente superior a 1500
select * from empleados where oficio='VENDEDOR' and salario>1500;
-- 8. Empleados cuyo OFICIO sea VENDEDOR o que su SALARIO sea superior o igual a 1500.
select * from empleados where oficio='VENDEDOR' or salario>=1500;
-- 9. Empleados cuyo OFICIO NO sea VENDEDOR y SALARIO sea estrictamente superior a 1500 o que sean del departamento 20.
select * from empleados where oficio!='VENDEDOR' and (salario>1500 or dep_no=20);
-- 10. Muestra los Empleados de tipo vendedor y también a los de tipo director que ganen menos de 3000€.
select * from empleados where oficio='VENDEDOR' or (oficio='DIRECTOR' and salario<3000);
-- 11. Obtén el total a cobrar anualmente por cada empleado, sabiendo que el salario almacenado es el salario mensual y que a dicho salario hay que sumarle la comisión, si la tuviera. La columna se llamará ingresos anuales.
select (salario+ifnull(comision,0))*12 as ingresos_anuales from empleados;
-- 12. Mostrar una fila para cada empleado que muestre de qué trabaja cada uno de ellos con el siguiente formato: “[apellido] es [oficio]”.
select concat(apellido,' es ',oficio) as resultado from empleados;
-- 13. Selecciona aquellos empleados cuyo apellido empiece por “M” y su salario esté comprendido entre 1.000 y 2.000 €. Visualiza su número de empleado, apellido y departamento.
select emp_no,apellido,dep_no from empleados where apellido like 'M%' and salario between 1000 and 2000;
-- 14. Selecciona aquellos empleados cuyo apellido incluya una “A” en el segundo carácter.
select * from empleados where apellido like '_A%';
-- 15. Seleccionar los empleados existentes en los departamentos 10 y 30.
select * from empleados where dep_no in (10,30);
-- 16. Obtén todos los datos de los empleados, ordenados alfabéticamente por número del departamento y después inversamente por apellido.
select * from empleados order by dep_no asc,apellido desc;
-- 17. Todos los atributos de los empleados cuya comisión no está vacía ni tampoco es 0 ordenados por comisión ascendente.
select * from empleados where ifnull(comision,0)>0 order by comision asc;
-- 18. Obtén la masa salarial mensual de todos los empleados, el salario medio y el salario máximo.
select sum(salario),avg(salario),max(salario) from empleados;
-- 19. Obtén los salarios máximo, mínimo y la diferencia existente entre ambos.
select maX(salario),min(salario),max(salario)-min(salario) as diferencia from empleados;
-- 20. Obtén la fecha de alta más reciente.
select max(fecha_alta) from empleados;
-- 21. Obtén los salarios medios por departamento.
select dep_no,avg(salario) from empleados group by dep_no;
-- 22. Obtén cuántos empleados hay en cada departamento, ordenando el resultado en orden decreciente por el número de empleados de cada departamento.
select dep_no,count(emp_no) from empleados group by dep_no order by 2 desc;
-- 23. Obtén el salario del empleado que mayor salario tiene dentro de cada oficio, excluyendo al presidente.
select oficio,max(salario) from empleados where oficio!='PRESIDENTE' group by oficio;
-- 24. Seleccionar los oficios que tengan dos o más empleados.
select oficio,count(emp_no) from empleados 
group by oficio having count(emp_no)>=2;
-- 25. Seleccionar los oficios que tengan dos o más empleados cuyo salario supere los 1400 €.
select oficio,count(emp_no) from empleados 
where salario>1400
group by oficio having count(emp_no)>=2;
-- 26. Obtén de por medio de subconsultas y con una consulta multitabla los distintos departamentos (código y nombre) en los que trabaja alguien.
select dep_no,dnombre from departamentos where dep_no in (select dep_no from empleados);
select distinct dep.dep_no,dep.dnombre from departamentos dep,empleados emp where dep.dep_no=emp.dep_no;
select distinct dep.dep_no,dep.dnombre from departamentos dep join empleados emp on (dep.dep_no=emp.dep_no);
-- 27. Comprueba si hay algún empleado que no tenga asignado un departamento.
select * from empleados where dep_no is null;
-- 28. Obtén una lista de empleados cuyo salario supere el salario medio.
select * from empleados where salario>(select avg(salario) from empleados);
-- 29. Visualizar el departamento con más empleados
select depto.dnombre,count(emp.emp_no) as num_empleados
from empleados emp, departamentos depto
where emp.dep_no=depto.dep_no
group by depto.dnombre
order by 2 desc
limit 1;
-- 30. Visualizar el departamento con más personal del oficio ‘empleado’
select depto.dnombre,count(emp.emp_no) as num_empleados
from empleados emp, departamentos depto
where emp.dep_no=depto.dep_no
and oficio='EMPLEADO'
group by depto.dnombre
order by 2 desc
limit 1;
-- 31. Visualizar el departamento con más presupuesto asignado para pagar el salario y la comisión de sus empleados
select depto.dnombre,sum(salario+ifnull(comision,0))
from empleados emp, departamentos depto
where emp.dep_no=depto.dep_no
group by depto.dnombre
order by 2 desc
limit 1;
-- 32. Obtener información de los empleados que ganan más que cualquier empleado del departamento 30 (hacerla de 2 formas distintas).
select * from empleados emp where salario>ALL(select salario from empleados where dep_no=30);
select * from empleados emp where salario>(select max(salario) from empleados where dep_no=30);
-- 33. Mostrar el código de aquellos empleados que son director. Se permite mostrar duplicados
select director from empleados;
-- 34. Mostrar el código de todos los productos que se han vendido alguna vez. No se permite mostrar duplicados
select distinct pr.* 
from productos pr,pedidos ped
where ped.producto_no=pr.producto_no;
-- 35. Mostrar los siguientes datos relativos a empleados: apellido, número, nombre de departamento y localidad.
select emp.apellido,emp.emp_no,emp.dep_no,depto.localidad
from empleados emp left outer join departamentos depto
on (emp.dep_no=depto.dep_no);
-- 36. Seleccionar el número, apellido y oficio de los empleados que pertenezcan al departamento de VENTAS por medio de subconsultas y por medio de una consulta multitabla.
Select emp_no,apellido,oficio from empleados
where dep_no=(select dep_no from departamentos where dnombre='VENTAS');
select emp_no,apellido,oficio
from empleados emp,departamentos dep
where emp.dep_no=dep.dep_no
and dep.dnombre='VENTAS';
-- 37. Visualizar los apellidos y su longitud, de los empleados cuyo departamento contenga las letras ‘ON’ en la 2ª y 3ª posición.
select apellido,length(apellido) 
from empleados e,departamentos d
where e.dep_no=d.dep_no and d.dnombre like '_ON%';
-- 38. Obtén todos los clientes junto con todos los productos que han comprado. Mostrar todos los atributos.
select c.*,pr.*
from clientes c, pedidos p, productos pr
where c.cliente_no=p.cliente_no and p.producto_no=pr.producto_no;
-- 39. Obtén todas las combinaciones posibles de apellido de empleado con nombre de departamento. No se trata de relacionar a cada empleado con el departamento en el que efectivamente trabaja, sino de relacionar a cada empleado con cada departamento en el que podría trabajar.
select apellido,dnombre from empleados, departamentos;
-- 40. Mostar la descripción y precio de los productos vendidos por el vendedor “ALONSO”.
select e.apellido,pr.descripcion,pr.precio_actual
from productos pr, pedidos p,clientes c, empleados e
where pr.producto_no=p.producto_no
and p.cliente_no=c.cliente_no
and c.vendedor_no=e.emp_no
and e.apellido='ALONSO';
-- 41. Apellido, salario y localidad de los empleados de Sevilla y Valencia
select apellido,salario,localidad from empleados e, departamentos d
where e.dep_no=d.dep_no and localidad in ('sevilla','valencia');
-- 42. Apellido, localidad y oficio de los empleados de Madrid que ganan menos de 2.000 y los de Barcelona que ganan menos de 1.500
select apellido,localidad,oficio
from empleados e, departamentos d
where e.dep_no=d.dep_no
and ((localidad='Madrid' and salario<2000) or (localidad='Barcelona' and salario<1500));
-- 43. Obtén los empleados cuyo salario supera al de sus compañeros de departamento.
select * from empleados e where salario = (select max(salario) from empleados e2 where e2.dep_no=e.dep_no);
select * from empleados e where salario >= ALL (select salario from empleados e2 where e2.dep_no=e.dep_no);
-- 44. Para cada empleado indicar su apellido, número de departamento y localidad del departamento
select apellido,ifnull(localidad,'DESCONOCIDA') as localidad,d.dep_no
from empleados e left outer join departamentos d on (e.dep_no=d.dep_no);
-- 45. Para cada empleado indicar el nombre del empleado y el nombre de su jefe
select emp.apellido as empleado,jefe.apellido as jefe
from empleados emp, empleados jefe
where emp.director=jefe.emp_no
order by 2 desc;
-- 46. Para cada pedido indicar el nombre del cliente y el nombre del producto comprado
select ped.pedido_no,cli.nombre,pr.descripcion
from pedidos ped,clientes cli, productos pr
where ped.cliente_no=cli.cliente_no and ped.producto_no=pr.producto_no;
-- 47. Obtén un listado de los productos vendidos por cada empleado (apellido del empleado y descripción y precio actual de los productos).
select emp.apellido,pr.descripcion,pr.precio_actual
from empleados emp, clientes cli,pedidos ped, productos pr
where ped.producto_no=pr.producto_no
and cli.cliente_no=ped.cliente_no
and cli.vendedor_no=emp.emp_no;
-- 48. Comprueba el número de veces que ha sido vendido cada producto.
select pr.descripcion,count(ped.pedido_no) as pedidos
from productos pr, pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion
order by 2 desc;
-- 49. Comprueba qué producto ha sido vendido un mayor número de veces.
select pr.descripcion,count(ped.pedido_no) as pedidos
from productos pr, pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion
order by 2 desc
limit 1;
-- 50. Comprueba de qué producto se han vendido más unidades.
select pr.descripcion,truncate(sum(ped.unidades),0) as total_unidades
from productos pr, pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion
order by 2 desc
limit 1;
-- 51. Calcular los ingresos medios (salario + comisión) de los empleados del departamento de VENTAS.
select truncate(avg(salario+ifnull(comision,0)),0) as promedio_sal
from empleados emp,departamentos dep
where emp.dep_no=dep.dep_no
and dep.dnombre='VENTAS';
-- 52. Cuántos pedidos ha realizado el cliente DISTRIBUCIONES GOMEZ.
select count(ped.pedido_no) as numpedidos
from pedidos ped,clientes cli
where ped.cliente_no=cli.cliente_no and cli.nombre='DISTRIBUCIONES GOMEZ';
-- 53. Cuántos clientes tiene a su cargo el empleado ALONSO.
select count(cli.cliente_no) as clientes
from clientes cli, empleados emp
where cli.VENDEDOR_NO=emp.emp_no
and emp.apellido='ALONSO';
-- 54. Cuántos empleados tiene a su cargo (de cuántos es jefe) el empleado ALONSO.
select count(emp.emp_no)
from empleados emp, empleados jefe
where emp.director=jefe.emp_no
and jefe.apellido='ALONSO';
-- 55. Indicar el nombre y la fecha en que se ha vendido por última vez cada producto.
select pr.descripcion,max(ped.fecha_pedido)
from productos pr, pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion;
-- 56. Para cada departamento indicar Cuánto dinero gasta en salarios, cuánto gasta en comisiones y cuanto gasta en total (salario + comisión).
select ifnull(dep.dnombre,'DESCONOCIDO'), sum(salario) as salarios,sum(ifnull(comision,0)) as comisiones,sum(salario+ifnull(comision,0)) as total
from empleados emp left outer join departamentos dep on (emp.dep_no=dep.dep_no)
group by dep.dnombre;
-- 57. Nombre de cada empleado que atiende a algún cliente y Cuántos clientes tiene a su cargo ese empleado. Mostrar sólo aquellos empleados que atiendan a más de 2 clientes
select emp.apellido,count(cli.cliente_no)
from empleados emp, clientes cli
where cli.VENDEDOR_NO=emp.emp_no
group by emp.apellido
having count(cli.cliente_no)>2;
-- 58. Para los productos que se han vendido alguna vez: Indicar el nombre de cada producto y Cuántas unidades se han vendido de ese producto, Mostrar sólo aquellos de los que se han vendido más de 15 unidades.
select pr.descripcion,sum(ped.unidades)
from productos pr,pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion
having sum(unidades)>15;
-- 59. Indicar el nombre y la fecha en que se ha vendido por última vez cada producto siempre y cuando el importe total de ventas de ese producto (a precio actual) sea superior a 100.
select pr.descripcion,max(ped.fecha_pedido)
from productos pr, pedidos ped
where pr.producto_no=ped.producto_no
group by pr.descripcion,pr.precio_actual
having pr.precio_actual*sum(ped.unidades)>100;
-- 60. Listar las localidades donde existan departamentos con empleados cuya comisión supere el 10% del salario
select distinct localidad
from departamentos d,empleados e
where d.dep_no=e.dep_no
and ifnull(comision,0)>salario*0.1;
select distinct localidad from departamentos dep where exists(select 1 from empleados emp where emp.dep_no=dep.dep_no and ifnull(comision,0)>salario*0.1);
-- 61. Obtener el nombre de cada departamento y la media salarial de ese departamento, para los departamentos cuya media salarial es mayor que el salario del empleado 7654
select d.dnombre,avg(salario+ifnull(comision,0)) as media_salarial
from departamentos d, empleados e
where d.dep_no=e.dep_no
group by d.dnombre
having avg(salario+ifnull(comision,0))>(select salario from empleados where emp_no=7654);
/* Utiliza funciones predefinidas de Mysql para obtener las siguientes consultas: */
-- 62. mostrar el número de pedido y la fecha del pedido de aquellos pedidos realizados en el año 2000. Repetir este ejercicio mostrando la fecha en estos formatos: 2015-1-23, 23-1-2015, 23-01-2015
select pedido_no,fecha_pedido,DATE_FORMAT(fecha_pedido,'%Y-%c-%e'), DATE_FORMAT(fecha_pedido,'%e-%c-%Y'),DATE_FORMAT(fecha_pedido,'%d-%m-%Y') 
from pedidos 
where year(fecha_pedido)=2000;
-- 63. mostrar el número de pedido y la fecha del pedido de aquellos pedidos realizados en el año 1999
select pedido_no,fecha_pedido from pedidos where year(fecha_pedido)=1999;
-- 64. mostrar el número de pedido y la fecha del pedido de aquellos pedidos realizados en noviembre del año 1999
select pedido_no,fecha_pedido from pedidos where year(fecha_pedido)=1999 and month(fecha_pedido)=11;
select pedido_no,fecha_pedido from pedidos where fecha_pedido between '1999-11-01' and '1999-11-30';
-- 65. mostrar el número de pedido y la fecha del pedido de aquellos pedidos realizados en noviembre o diciembre del año 1999.
select pedido_no,fecha_pedido from pedidos where year(fecha_pedido)=1999 and month(fecha_pedido) in (11,12);
-- 66. mostrar el número de pedido y la fecha del pedido de aquellos pedidos realizados en el año 1999 fuera del mes de agosto
select pedido_no,fecha_pedido from pedidos where year(fecha_pedido)=1999 and month(fecha_pedido)!=8;
-- 67. Muestra los nombres y empleos de los empleados tales que su empleo acaba en "OR" y su nombre empieza por A.
select apellido,oficio from empleados where right(oficio,2)='OR' and left(apellido,1)='A';
-- 68. Halla los nombres de los empleados que tienen como máximo cinco caracteres en su nombre.
select apellido from empleados where length(apellido)<=5;
-- 69. Obtener los datos de los empleados contratados en el año 1981
select * from empleados where year(FECHA_ALTA)=1981;
select * from empleados where FECHA_ALTA between '1981-01-01' and '1981-12-31';
-- 70. Obtener los datos de los empleados contratados en febrero del año 1981
select * from empleados where year(FECHA_ALTA)=1981 and month(FECHA_ALTA)=2;
select * from empleados where date_format(fecha_alta,'%Y/%m')='1981/02';
-- 71. Obtener los datos de los empleados contratados antes de enero del año 1983
select * from empleados where year(fecha_alta)<1983;
-- 72. Deseamos conocer cuál es el mes en el que más empleados se contratan históricamente. Para ello, muestra al lado de cada mes (muestra el mes como número) cuántos empleados se han contratado en ese mes (a lo largo de los años).
select month(fecha_alta),count(emp_no)
from empleados
group by month(fecha_alta)
order by 2 desc limit 1;
-- 73. Muestra el importe promedio de los pedidos de la empresa redondeado a 1 decimal, truncado (sin decimales), y el entero siguiente al promedio.
select avg(ped.unidades*pr.precio_actual) as importe_promedio,round(avg(ped.unidades*pr.precio_actual),1) as promedio_1_dec,truncate(avg(ped.unidades*pr.precio_actual),0) as truncado,ceil(avg(ped.unidades*pr.precio_actual)) as siguiente
from pedidos ped,productos pr
where ped.producto_no=pr.producto_no;
-- 74. Halla los clientes cuyo nombre contiene la cadena “S.A.” a partir de la posición 12 de su nombre.
select * from clientes where locate('S.A.',nombre,12)>0;
-- 75. Comprueba si hay algún cliente que tenga espacios a la izquierda o a la derecha de su nombre.
select * from clientes where (nombre!=rtrim(nombre) or nombre!=ltrim(nombre));
-- 76. Muestra el nombre de todos los empleados de forma que el primer carácter del nombre se muestre en mayúsculas y el resto en minúsculas.
select concat(upper(left(apellido,1)),lower(right(apellido,length(apellido)-1))) from empleados;
-- 77. Muestra todos los empleados cuyo código de empleado es múltiplo de 3.
select * from empleados where mod(emp_no, 3)=0;
select * from empleados where emp_no % 3=0;
select * from empleados where emp_no mod 3=0;
-- 78. Queremos obtener un listado telefónico de los empleados, de forma que para cada empleado aparezca su nombre y teléfono, excepto para el presidente, que debe figurar el teléfono “DESCONOCIDO”.
select apellido,if(oficio!='presidente',telefono,'DESCONOCIDO') as telefono
from empleados;
select apellido,case when oficio!='presidente' then telefono else 'DESCONOCIDO' end as telefono
from empleados;
-- 79. Muestra las ventas por empleado, de forma que si el empleado cobra más de 1500€ se cuenten sus pedidos, pero si cobra 1500 € o menos se consideren las unidades vendidas.
select apellido,salario,if(salario<=1500,sum(unidades),count(pedido_no))
from empleados e, clientes c, pedidos p
where c.vendedor_no=e.emp_no and c.cliente_no=p.cliente_no
group by apellido,salario;