
create table Autores ( 
	codautor autoincrement primary key,
	nombre varchar(50) not null,
	apellido1 varchar(50) not null,
	apellido2 varchar(50),
	alias varchar(50),
	fecnac date,
	fecdefunc date,
	anho_lg int not null,
	genero varchar(20) not null,
	edad_primera_obra smallint  not null,
	miembro_RAGL bit,
	defunc_natural bit,
	codlocnac smallint not null, 
	codlocdefunc smallint not null
);

create table Asociacion (
	codasoc autoincrement primary key,
	asociacion varchar(100) not null,
	es_partido bit,
	es_galleguista bit
);

create table Residencias (
	codautor int ,
	codloc smallint,
	fecini date,
	fecfin date not null,
	constraint PK_residencias primary key (codautor,codloc,fecini)
);

create table Obras(
	codobra autoincrement primary key,
	fecha date,
	numobra smallint,
	idioma varchar(30) not null,
	edad_autor smallint ,
	codautor int ,
	codtipoobra int ,
	codtemaprincipal int,
	codeditorial int
);

create table TipoObra (
	codtipo autoincrement primary key,
	tipo varchar(80) not null
);

create table Influencias(
    joven bit,
    codinfluyente int ,
    codinfluido int ,
	constraint PK_INFLUENCIAS primary key (codinfluyente,codinfluido)
);

 create table titulacion(
    nombre varchar(50),
    codtitulacion autoincrement primary key
);

create table Profesion (
	codprof autoincrement primary key,
	profesion varchar(30) not null
);

create table Profesion_autor (
	codprof int,
	codautor int,
	principal bit,
	constraint pk_profesion_autor primary key (codprof, codautor)
);

create table miembros_asociaciones(
	codasoc int,
	codautor int,
	directivo bit,
	constraint PK_miembros_asoc primary key (codasoc,codautor)
);

create table exilia(
	codautor int ,
	codpais smallint ,
	fecha date,
	constraint pk_exilia primary key (codautor,codpais,fecha)
);

create table Pais(
    codpais smallint primary key,
    pais varchar(50) not null,
    continente varchar(50) not null
);

create table Comunidad(
    codcom smallint primary key,
    comunidad varchar(50) not null,
    codpais smallint not null    
);

create table Provincia(
    codprov smallint primary key,
    provincia varchar(50) not null,
    codcom smallint not null
);

create table Localidad(
    codlocalidad smallint primary key,
    localidad varchar(100) not null,
    codprov smallint not null
);

create table Editorial(
    codeditorial autoincrement primary key,
    nombre varchar(100) not null,
    procedencia varchar(50),
    codloc int not null
);

create table Tematica(
    codtema autoincrement primary key,
    tematica  varchar(100) not null,
    descripcion  text
);

create table TipoPremio (
    codtipopremio autoincrement primary key,
    tipopremio varchar(50)
);

create table Premio (
    codpremio autoincrement primary key,
    nombre varchar(50),
    organizacion varchar(50),
    codtipopremio int    
);

create table Nominaciones (
    codobra int,
    codpremio int,
    galardonada bit,
    fecha date not null,
    constraint PK_Nominaciones primary key (codobra, codpremio)
);

create table estudios(
    codautor int ,
    codtitulacion int,
	constraint pk_estudios primary key (codautor,codtitulacion)
);

alter table estudios add constraint FK_estudios_autores foreign key (codautor) references autores(codautor);
alter table estudios add constraint FK_estudios_titulacion foreign key (codtitulacion) references titulacion(codtitulacion);
alter table nominaciones add constraint FK_premio foreign key (codpremio) references Premio(codpremio);
alter table nominaciones add constraint FK_obra foreign key (codobra) references Obras(codobra);
alter table Obras add constraint FK_obras_tipoobra foreign key (codtipoobra) references TipoObra(codtipo);
alter table Obras add constraint FK_obras_autor foreign key (codautor) references Autores(codautor);
alter table Obras add constraint FK_obras_tema foreign key (codtemaprincipal) references Tematica(codtema);
alter table Obras add constraint FK_obras_editorial foreign key (codeditorial) references Editorial(codeditorial);
alter table Autores add constraint fk_locnac foreign key (codlocnac) references Localidad(codlocalidad);
alter table Autores add constraint fk_locdefunc foreign key (codlocdefunc) references Localidad(codlocalidad);
alter table Residencias add constraint fk_codautor foreign key (codautor) references Autores(codautor);
alter table Residencias add constraint fk_codloc foreign key (codloc) references Localidad(codlocalidad);
alter table Profesion_autor add constraint fk_profesorautor_profesion foreign key (codprof) references Profesion(codprof);
alter table Profesion_autor add constraint fk_profesorautor_autor foreign key (codautor) references Autores(codautor);
alter table influencias add constraint FK_influencias_influye foreign key (codinfluyente) references autores(codautor);
alter table influencias add constraint FK_influencias_influido foreign key (codinfluido) references autores (codautor);
alter table miembros_asociaciones add constraint fk_miem_asoci FOREIGN KEY (codasoc) references asociacion (codasoc);
alter table miembros_asociaciones add constraint fk_miem_autor FOREIGN KEY (codautor) references autores (codautor);
alter table exilia add constraint fk_exilia_autor FOREIGN KEY (codautor) references autores (codautor);
alter table exilia add constraint fk_exilia_pais FOREIGN KEY (codpais) references pais (codpais);
alter table comunidad add constraint FK_pais_com foreign key (codpais) references Pais(codpais);
alter table provincia add constraint FK_com_prov foreign key (codcom) references Comunidad(codcom);
alter table localidad add constraint FK_prov_loc foreign key (codprov) references Provincia(codprov);
alter table editorial add constraint FK_loc_edi foreign key (codloc) references Localidad(codlocalidad);
alter table premio add constraint FK_tipopremio foreign key (codtipopremio) references TipoPremio(codtipopremio);
