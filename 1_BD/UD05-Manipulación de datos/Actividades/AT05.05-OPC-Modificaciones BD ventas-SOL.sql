use ventas;

-- 1.	Inserta en la tabla departamentos un nuevo departamento con código 50 y sede en “SANTIAGO” que tendrá por nombre “operaciones”. 
insert into departamentos (DEP_NO,DNOMBRE,LOCALIDAD) values (50,'OPERACIONES','SANTIAGO');
-- 2.	Asigna a dicho departamento al empleado “Lopez”, que como será el jefe de ese departamento no tendrá director asignado.
update empleados set dep_no=50, director=null where apellido='LOPEZ';
select * from empleados;
-- 3.	Inserta con los códigos 8000, 8001 y 8002 a los empleados “MARTINEZ”, “PEREZ” y “RODRIGUEZ”, analistas que acaban de ser contratados 
-- por el departamento de operaciones. Sus salarios serán respectivamente de 1000, 1100 y 1200€, sin comisiones. Su director será el 7521.
insert into empleados (EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO, TELEFONO )
values(8000,'MARTINEZ','ANALISTA',7521,date(now()),1000,null,50,null),
	(8001,'PEREZ','ANALISTA',7521,date(now()),1100,null,50,null),
	(8002,'RODRIGUEZ','ANALISTA',7521,date(now()),1200,null,50,null);
-- 4.	Asignar una comisión de 2.000€ a los vendedores que no ganan nada en concepto de comisión
update empleados set comision=2000 where comision is null and oficio='VENDEDOR';
-- 5.	Actualizar con una sola operación el pedido 1000 de modo que aumente en 1 unidad el producto comprado y se reduzca en una unidad 
-- el stock disponible del producto vendido.
update pedidos ped,productos prod set ped.unidades=ped.unidades+1,prod.STOCK_DISPONIBLE=prod.STOCK_DISPONIBLE-1
where ped.producto_no=prod.producto_no and ped.pedido_no=1000;
-- 6.	El pedido 1001 no ha sido pagado, por ese motivo debemos añadir al debe del cliente el importe del pedido.
update clientes cl,pedidos ped,productos prod 
set debe=debe+(prod.precio_actual*ped.unidades)
where cl.CLIENTE_NO=ped.CLIENTE_NO and ped.PRODUCTO_NO=prod.producto_no
and ped.PEDIDO_NO=1001;
-- 7.	Inserta un nuevo departamento con código 0, nombre “DESCONOCIDO” y sede “DESCONOCIDA”.
insert into departamentos (DEP_NO,DNOMBRE,LOCALIDAD) values (0,'DESCONOCIDO','DESCONOCIDA');
-- 8.	Copia todos los empleados entre el 7839 y el 7876, poniéndoles como código el código actual menos 7838. 
-- Su fecha de contratación debe ser la fecha actual.
insert into empleados (EMP_NO,APELLIDO,OFICIO,DIRECTOR,FECHA_ALTA,SALARIO,COMISION,DEP_NO, TELEFONO )
select EMP_NO-7838,APELLIDO,OFICIO,DIRECTOR,date(now()),SALARIO,COMISION,DEP_NO, TELEFONO 
from empleados where emp_no between 7839 and 7876;
-- 9.	Crea una nueva tabla de “EMP_CONT” con la misma estructura que la tabla de empleados, e inserta en ella los empleados del departamento de contabilidad.
create table emp_cont as select * from empleados where dep_no=(select dep_no from departamentos where dnombre='CONTABILIDAD');
-- 10.	Sube el salario un 2% a los empleados con oficio vendedor y súbelo un 3% a los que no son vendedor ni director (con una sola instrucción).
update empleados set salario=salario*(case when oficio='VENDEDOR' then 1.02 when oficio not in ('VENDEDOR','DIRECTOR') then 1.03 else 1 end);
-- 11.	Crea una nueva tabla PUESTOs(codpuesto,puesto). El codpuesto debe ser la clave, y será auto incrementable.
--  Inserta en dicha tabla los distintos puestos que ocupan los empleados (sin repetir).
create table puestos(codpuesto smallint auto_increment primary key, puesto varchar(20));
insert into puestos (puesto) select distinct oficio from empleados;
-- 12.	Crea un campo codpuesto en la tabla de empleado y asigna a cada empleado el código de puesto que le corresponda en función de su oficio. 
-- Elimina posteriormente el campo OFICIO de la tabla de empleados.
alter table empleados add codpuesto smallint;
update empleados e, puestos p
set e.codpuesto=p.codpuesto where e.oficio=p.puesto;
alter table empleados drop oficio;
-- 13.	A todos los empleados que no tengan asignado un departamento, asígnales el departamento desconocido.
update empleados set dep_no=(select dep_no from departamentos where dnombre='DESCONOCIDO') where dep_no is null;
-- 14.	El empleado 7499 cambiará de lugar de trabajo, pasando al departamento CONTABILIDAD  con el puesto de EMPLEADO. 
-- Modifica ambos campos con una sola sentencia.
update empleados 
set dep_no=(select dep_no from departamentos where dnombre='CONTABILIDAD'),
	codpuesto=(select codpuesto from puestos where puesto='EMPLEADO')
where emp_no=7499;
-- 15.	Transfiere a todos los empleados del departamento 30 que no tengan comisión al departamento 40, y asígnales una comisión de 200€.
update empleados set dep_no=40, comision=200 
where dep_no=30 and comision is null;
-- 16.	Asigna una comisión de 500 a todos los empleados que no teniendo comisión tengan un salario superior a 2500€.
update empleados set comision=500 
where comision is null and salario>2500;
-- 17.	Asigna una comisión del 10% de su salario a todos los empleados que no teniendo comisión tengan un salario inferior a 2500€.
update empleados set comision=salario*0.1
where comision is null and salario<2500;
-- 18.	Baja todos los salarios un 10% e incrementa la comisión un 20%.
update empleados set salario=salario*0.9,COMISION=comision*1.2;
-- 19.	Elimina todos los empleados de la tabla EMP_CONT cuyo código sea mayor que 10.
delete from emp_cont where emp_no>10;
-- 20.	Despide (elimina) a aquellos empleados cuya comisión supera a su salario.
delete from empleados where comision>salario;
-- 21.	Jubila a aquellos empleados contratados antes del 1 de marzo de 1981.
delete from empleados where FECHA_ALTA<'19810301';
-- 22.	Borra el departamento CONTABILIDAD. Antes de eliminarlo, tendrás que reubicar a sus empleados y asignarlos al departamento DECONOCIDO.
update empleados set DEP_NO=(select dep_no from departamentos where dnombre='DESCONOCIDO')
where dep_no in (select dep_no from departamentos where dnombre='CONTABILIDAD');
delete from departamentos where dnombre='CONTABILIDAD';
-- 23.	Elimina todos los empleados del departamento de ventas cuyo apellido contenga como segunda letra una A, que tengan menos de 7 caracteres 
-- y que no terminen en 'O'.
delete e from empleados e, departamentos d 
where e.DEP_NO=d.dep_no and d.DNOMBRE='VENTAS' and e.APELLIDO like '_A%' and length(e.apellido)<7 and e.apellido not like '%O';
-- 24.	Añade un campo SEDE de tipo smallint a la tabla de departamentos.
alter table departamentos add sede smallint;
-- 25.	Asigna la sede 1 a todos los departamentos excepto al desconocido, que tendrá sede 0.
update departamentos set sede=if(DNOMBRE='CONTABILIDAD',0,1);
-- 26.	Duplica todos los departamentos de la sede 1 para crearlos también en la sede 2. Los códigos de los nuevos departamentos se obtendrán 
-- sumándole 50 al código de departamento que estén copiando.
insert into departamentos (DEP_NO,DNOMBRE,LOCALIDAD,sede)
select DEP_NO+50,DNOMBRE,LOCALIDAD,2 from departamentos where sede=1;
