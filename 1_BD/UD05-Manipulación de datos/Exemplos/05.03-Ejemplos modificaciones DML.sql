drop database if exists pruebas;
create database pruebas;

USE pruebas;

-- CREAR LAS TABLA CLIENTES

	CREATE TABLE CLIENTES(
	codigo INT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL ,
	provincia ENUM ('Coruña', 'Lugo','Ourense', 'Pontevedra') DEFAULT 'Coruña',
	profesion VARCHAR(20),
    fechaAlta TIMESTAMP DEFAULT NOW()
    );
    
 DESCRIBE CLIENTES;
    
 INSERT /*INTO*/ CLIENTES VALUES (1,'David Pérez González', 'Pontevedra', 'Técnico','2012/01/12'); 
 
 INSERT /*INTO*/ CLIENTES VALUES (2,'Manuel Pérez González', 'Coruña', '','2011/11/11');
 
 INSERT /*INTO*/ CLIENTES (nombre, codigo, provincia,fechaAlta) 
				   VALUES ('Jesús Pérez González',3,'Lugo', '2010/05/21');
 
 -- para insertar el valor por defecto, metemos default
 INSERT /*INTO*/ CLIENTES VALUES (4, 'Pablo López López', DEFAULT, 'Técnico', DEFAULT);
 
 
 INSERT /*INTO*/ CLIENTES VALUES (5, 'María Villegas Leste', 'Ourense', 'Economista', now());


-- POSIBLES ERRORES

-- Errores en tipo de datos.

 INSERT /*INTO*/ CLIENTES 
 VALUES (5, 'Felipe Juan Pablo Alfonso de Todos los Santos de Borbón y Grecia ', 'Ourense', 'Rey de España', now());
 
 INSERT /*INTO*/ CLIENTES 
 VALUES (5, 'Felipe VI ', 'Ourensee', 'Rey de España', now());
 
  -- Número de columnas NO corresponde con el número de datos.
 INSERT /*INTO*/ CLIENTES 
 VALUES (5, 'Felipe VI ', 'Ourense', 'Rey de España');

-- Duplicidad en la clave primaria.
 INSERT /*INTO*/ CLIENTES 
 VALUES (5, 'Felipe VI ', 'Ourense', 'Rey de España',DEFAULT);

-- CORRECTO
 INSERT /*INTO*/ CLIENTES 
 VALUES (6, 'Felipe VI ', 'Ourense', 'Rey de España',DEFAULT);

-- Sentencia INSERT extendida

INSERT /*INTO*/ CLIENTES VALUES
(7,'María Pérez González', 'Lugo', 'Economista','2001/12/26'),
(8,'David Pérez González', 'Ourense', '',now()),
(9,'Jesús Pérez González', DEFAULT, 'Técnico','2003/03/21');

SELECT * FROM CLIENTES;

-- Sentencia INSERT…SET

INSERT CLIENTES SET
	codigo = 10,
	nombre = 'Pablo García',
	provincia = DEFAULT,
	profesion = 'Administrativo',
    fechaAlta = '2012/02/14';


-- Sentencia INSERT ... SELECT 

-- CREAR LA TABLA ClientesNorteGalicia SÓLO CON los CAMPOS codigo y profesion
	CREATE TABLE ClientesNorteGalicia(
		codigo INT PRIMARY KEY,
		profesion VARCHAR(20)
	);

-- Utilizando CREATE...LIKE
CREATE TABLE ClientesNorteGalicia LIKE CLIENTES;

DESCRIBE ClientesNorteGalicia;

ALTER TABLE  ClientesNorteGalicia DROP nombre, DROP provincia, DROP fechaAlta;
			 
DESCRIBE ClientesNorteGalicia;

-- INSERTAR EN LA  TABLA CLIENTES CON  <INSERT…SELECT> SÓLO LOS CLIENTES DE LAS PROVINCIAS DE CORUÑA Y LUGO
INSERT ClientesNorteGalicia (codigo,profesion)
SELECT codigo,profesion
FROM CLIENTES
WHERE provincia LIKE 'CORUÑA' OR provincia LIKE 'LUGO'; 

SELECT * FROM ClientesNorteGalicia;


-- INSERTAR EL CONTENIDO DE 'CLIENTES.TXT' EN LA TABLA DE CLIENTES
-- OJO: asegurarse de que no haya líneas en blanco al final del fichero, sino puede dar errores:
LOAD DATA  LOCAL INFILE "C:\\Users/alumno/05.03-Clientes.txt"
INTO TABLE CLIENTES
FIELDS TERMINATED BY '-'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(CODIGO,NOMBRE,PROVINCIA,PROFESION);

SELECT * FROM CLIENTES;

/*  UPDATE  */

-- Modificar para los clientes que son técnicos de la coruña, el valor del campo profesión por ‘ingenieros’.
UPDATE CLIENTES SET PROFESION = 'Ingeniero',fechaalta=now()
WHERE PROVINCIA LIKE 'Coruña' AND PROFESION LIKE 'Técnico';

-- Para modificar dos campos, se indican separados por comas:
Update clientes set nombre='Pedro García',fechaalta=now() where codigo=10;

-- Modificar en las tablas ‘clientes’ y ‘clientesnortegalicia’ la profesión ‘administrativo/a’ por ‘contable’ respectivamente, 
-- solo para los clientes que sean administrativo/a.
UPDATE CLIENTES, CLIENTESNORTEGALICIA SET 
								 CLIENTES.PROFESION = 'Contable',
								 CLIENTESNORTEGALICIA.PROFESION = 'Contable'				
WHERE CLIENTES.PROFESION LIKE 'Administrativ_'AND
	  CLIENTESNORTEGALICIA.PROFESION LIKE 'Administrativ_';

SELECT * FROM CLIENTES;
SELECT * FROM CLIENTESNORTEGALICIA;


-- Eliminar de la tabla ‘clientes’ los clientes que no tienen asignada una profesion
DELETE FROM CLIENTES
		WHERE PROFESION IS NULL OR PROFESION LIKE '';
        
 -- Eliminar de las tablas ‘CLIENTES’ y 'CLIENTESNORTEGALICIA' el cliente que tiene codigo 4       
 DELETE CLIENTES, CLIENTESNORTEGALICIA
 from CLIENTES, CLIENTESNORTEGALICIA  
 WHERE (CLIENTES.codigo = CLIENTESNORTEGALICIA.codigo) 
 AND CLIENTES.codigo = 4;       

-- Insertar registro con REPLACE
REPLACE CLIENTES (CODIGO,NOMBRE, PROVINCIA,PROFESION) 
VALUES (14,'Jerusa Villergas', 'Coruña','Informática');

-- Insertamos un nuevo registro con el código del anterior
REPLACE CLIENTES (CODIGO, NOMBRE, PROVINCIA,PROFESION)
 VALUES (14,'Roberto Hidalgo', 'Lugo','Informática');

-- Insertamos registro con REPLACE .. SET
REPLACE CLIENTES SET CODIGO = 15,
					 NOMBRE = 'Susana Flórez', 
					 PROVINCIA = DEFAULT,
					 PROFESION = 'Bióloga';

-- Insertar registros con REPLACE desde otra tabla
REPLACE CLIENTESNORTEGALICIA (CODIGO,PROFESION)
SELECT CODIGO,PROFESION FROM CLIENTES WHERE CODIGO = 4;

 
SELECT * FROM CLIENTES;
SELECT * FROM CLIENTESNORTEGALICIA;
