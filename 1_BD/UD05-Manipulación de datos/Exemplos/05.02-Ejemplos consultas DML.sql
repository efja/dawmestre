@nba.sql
-- JUGADORES(codigo,Nombre,Procedencia,Altura,Peso,Posicion,Nombre_equipo)
-- EQUIPOS(Nombre,Ciudad,Conferencia,Division)
-- PARTIDOS(codigo,equipo_local,equipo_visitante,puntos_local,puntos_visitante,temporada)
-- ESTADISTICAS(temporada,jugador,Puntos_por_partido,Asistencias_por_partido,Tapones_por_partido,Rebotes_por_partido)

/********** SELECT *********/

-- Consultas simples
1. Mostrar el nombre de los jugadores de los Lakers.
2. Mostrar el c�digo, nombre y altura de los jugadores espa�oles que juegan en los Lakers.
3. Mostrar el nombre, altura y procedencia de los jugadores espa�oles y eslovenos que juegan en los Lakers.

-- operadores especiales
4. Mostrar el nombre, altura y procedencia de los jugadores espa�oles, eslovenos o serbios que juegan en los Lakers.
5. Mostrar nombre de los jugadores, nombre del equipo y peso los jugadores de la nba cuyo peso este entre 270 y 300 libras.
6. Mostrar el nombre del jugador, nombre del equipo y el peso en kilogramos de los jugadores de la NBA que pesen entre 120 y 150 kilos. Una libra equivales a 0.4535 kilos.
7. Mostrar el nombre de los jugadores y nombre del equipo de los jugadores de los que se desconoce su procedencia.
8. Mostrar el nombre de los jugadores y nombre del equipo de los jugadores de los que se conoce su procedencia.
9. Mostrar el nombre del equipo y procedencia de los equipos que empiecen por 'r'.
10. Mostrar el nombre del equipo y procedencia de los equipos que terminen en 'ts'.
11. Mostrar el nombre del equipo y procedencia de los equipos que contengan el car�cter 'p'.
12. Mostrar el nombre del equipo y procedencia de los equipos que contengan como segunda letra la 'o'.
13. Mostrar el nombre del equipo y procedencia de los equipos que empiecen por 'r' que terminen por 's' y que tengan 7 caracteres
14. Mostrar el nombre del equipo de los 5 primeros equipos.
15. Mostrar el nombre del equipo de los 3 primeros equipos a partir del 5� equipo.

-- order by
16. Mostrar nombre del equipo y divisi�n los equipos de la conferencia Oeste ordenados ascendentemente por divisi�n.
17. Mostrar nombre del equipo y divisi�n los equipos de la conferencia Oeste ordenados de forma ascendente por divisi�n y de forma descendente por nombre del equipo.

-- consultas de resumen
18. �Cu�nto pesa el jugador m�s pesado de la nba?
19. �Cu�nto mide el jugador m�s bajito de la nba?. Mostrar altura en pies y metros.
20. �Cu�ntos jugadores juegan en los Lakers?
21. �Cu�ntos jugadores juegan en los Warriors? Utiliza la columna procedencia c�mo argumento para la funci�n de resumen?
22. �Cu�nto pesan de media los jugadores de los Bulls?
23. Mostrar para cada equipo, el nombre del equipo, nombre y peso del jugador m�s pesado.
24. Mostrar por conferencia, cu�ntos equipos hay.
25. �Cu�nto pesan de media los jugadores de Espa�a, Italia y Francia?
26. Mostrar el nombre de los equipos de la nba cuyos jugadores pesen de media m�s de 228 libras, ordenarlos por la media del peso.
27. Seleccionar qu� equipos de la nba tienen 1 o m�s jugadores espa�oles, ordenarlos por nombre del equipo.

-- subconsultas
28. Mostrar el nombre de los jugadores que juegan en equipos de la divisi�n SouthWest.
29. Mostrar nombre y altura del jugador m�s alto de la nba.
30. Mostrar el nombre de la divisi�n de los equipos donde juegan jugadores espa�oles.
31. Mostrar el nombre de todos los equipos, s� hay jugadores espa�oles jugando en el equipo 'Raptors'.
32. Mostrar el nombre de los equipos que tienen jugadores espa�oles.
33. Mostrar el nombre de los equipos que no tienen jugadores espa�oles.
34. Nombre y peso de todos los jugadores de la nba que pesan m�s que todos los jugadores espa�oles.
35. Mostrar el nombre y peso de los jugadores base (posici�n = 'G') que pesan m�s que cualquier pivot (posici�n = 'C') de la nba.
36. Mostrar todos los datos del equipo donde juega el jugador m�s alto de la nba.

-- Consultas multitabla
37. N�mero de jugadores de cada conferencia
38. Nombre de equipo, conferencia y posici�n de los jugadores que hayan anotado m�s de 35 puntos en un partido
39. Promedio de altura de los jugadores que pongan m�s de 1,5 tapones por partido
40. Conferencia y nombre de los equipos local y visitante que hayan jugado partidos finalizados con m�s de 20 puntos de diferencia en la temporada 07/08
41. Nombre, Equipo, puntos por partido, asistencias por partido, tapones por partido y rebotes por partido de los jugadores espa�oles en la temporada 07/08
42. Para cada jugador, mostrar su nombre, nombre de equipo y puesto en el que juega.
43. Para cada equipo, mostrar todos los datos del equipo y n�mero de partidos que han jugado como locales.
44. Nombres de los equipos que jugaron alg�n partido resuelto por un solo punto de diferencia y temporada en que se jug� el partido.
45. Datos de los equipos que no han ganado ning�n partido como visitantes
46. Nombre, peso y equipo de los jugadores cuyo peso coincide con el de otro jugador de su mismo equipo (y nombre del jugador con el que coinciden)
47. Muestra para cada equipo, su nombre y el nombre de los equipos de su misma conferencia.
48. Nombre, conferencia y n�mero de jugadores de cada equipo. Si un equipo no tiene jugadores debe mostrar un 0.
49. N�mero de partidos ganados y perdidos por cada equipo. Si un equipo no tiene partidos, debe mostrar 0.
