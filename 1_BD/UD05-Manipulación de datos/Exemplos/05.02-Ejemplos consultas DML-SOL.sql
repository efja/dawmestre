use nba;
-- JUGADORES(codigo,Nombre,Procedencia*,Altura,Peso,Posicion,Nombre_equipo)
-- EQUIPOS(Nombre,Ciudad,Conferencia,Division)
-- PARTIDOS(codigo,equipo_local,equipo_visitante,puntos_local,puntos_visitante,temporada)
-- ESTADISTICAS(temporada,jugador,Puntos_por_partido,Asistencias_por_partido,Tapones_por_partido,Rebotes_por_partido)

/********** SELECT *********/
-- Consultas simples
-- 1. Mostrar el nombre de los jugadores de los Lakers.
select nombre from jugadores where nombre_equipo='Lakers';
--  2. Mostrar el c�digo, nombre y altura de los jugadores espa�oles que juegan en los Lakers.
select distinct procedencia from jugadores where nombre_equipo='Lakers';
select codigo,nombre,altura from jugadores where procedencia='Spain' and nombre_equipo='Lakers';
--  3. Mostrar el nombre, altura y procedencia de los jugadores espa�oles y eslovenos que juegan en los Lakers.
select nombre,altura,procedencia from jugadores where nombre_equipo='Lakers' and (procedencia ='spain' or procedencia='Slovenia');
--  -- operadores especiales
--  4. Mostrar el nombre, altura y procedencia de los jugadores espa�oles, eslovenos o serbios que juegan en los Lakers.
select nombre,altura,procedencia from jugadores where nombre_equipo='Lakers' and procedencia in ('spain','Slovenia','Serbia Montenegro');
--  5. Mostrar nombre de los jugadores, nombre del equipo y peso los jugadores de la nba cuyo peso este entre 270 y 300 libras.
select nombre,nombre_equipo,peso from jugadores where peso between 270 and 300;
--  6. Mostrar el nombre del jugador, nombre del equipo y el peso en kilogramos de los jugadores de la NBA que pesen entre 120 y 150 kilos. Una libra equivales a 0.4535 kilos.
select nombre,nombre_equipo,peso*0.4535 as peso_en_kilos
from jugadores where peso*0.4535 between 120 and 150;
--  7. Mostrar el nombre de los jugadores y nombre del equipo de los jugadores de los que se desconoce su procedencia.
select nombre, nombre_equipo,procedencia from jugadores where procedencia is null;
--  8. Mostrar el nombre de los jugadores y nombre del equipo de los jugadores de los que se conoce su procedencia.
select nombre, nombre_equipo,procedencia from jugadores where procedencia is not null;
--  9. Mostrar el nombre del equipo y ciudad de los equipos que empiecen por 'r'.
select nombre,ciudad from equipos where nombre like 'r%';
-- 10. Mostrar el nombre del equipo y ciudad de los equipos que terminen en 'ts'.
select nombre, ciudad from equipos where nombre like '%ts';
-- 11. Mostrar el nombre del equipo y ciudad de los equipos que contengan el car�cter 'p'.
select nombre, ciudad from equipos where nombre like '%p%';
-- 12. Mostrar el nombre del equipo y ciudad de los equipos que contengan como segunda letra la 'o'.
select nombre, ciudad from equipos where nombre like '_o%';
-- 13. Mostrar el nombre del equipo y ciudad de los equipos que empiecen por 'r' que terminen por 's' y que tengan 7 caracteres
select nombre, ciudad from equipos where nombre like 'r_____s';
select * from equipos where length(nombre)=7 and nombre like 'r%s';
-- 14. Mostrar el nombre del equipo de los 5 primeros equipos.
select nombre from equipos order by nombre limit 5;
-- 15. Mostrar el nombre del equipo de los 3 primeros equipos a partir del 5� equipo.
select nombre from equipos order by nombre limit 5,3;
-- -- order by
-- 16. Mostrar nombre del equipo y divisi�n los equipos de la conferencia Oeste ordenados ascendentemente por divisi�n.
select nombre,division from equipos where conferencia='West' order by division asc;
-- 17. Mostrar nombre del equipo y divisi�n los equipos de la conferencia Oeste ordenados de forma ascendente por divisi�n y 
-- de forma descendente por nombre del equipo.
select nombre,division from equipos 
where conferencia='West' order by division asc,nombre desc;
-- -- consultas de resumen
-- 18. �Cu�nto pesa el jugador m�s pesado de la nba?
select max(peso) from jugadores;
-- 19. �Cu�nto mide el jugador m�s bajito de la nba?. Mostrar altura en pies y metros.
select min(altura),min(altura) * 0.3048 as altura_metros from jugadores;
-- 20. �Cu�ntos jugadores juegan en los Lakers?
select count(*) from jugadores where nombre_equipo='Lakers';
select * from jugadores;
-- 21. �Cu�ntos jugadores juegan en los Warriors? Utiliza la columna procedencia c�mo argumento para la funci�n de resumen?
select count(procedencia) from jugadores where nombre_equipo='Warriors'; -- sale 0 porque estan todos a null
-- 22. �Cu�nto pesan de media los jugadores de los Bulls?
select avg(peso) from jugadores where nombre_equipo='Bulls';
-- 23. Mostrar para cada equipo, el nombre del equipo y peso del jugador m�s pesado.
select nombre_equipo,max(peso) from jugadores
group by nombre_equipo;
-- 24. Mostrar por conferencia, cuantos equipos hay.
select conferencia,count(*) from equipos group by conferencia;
-- 25. �Cu�nto pesan de media los jugadores de Espa�a, Italia y Francia?
select avg(peso) from jugadores where procedencia  in ('spain','italy','france');
-- 26. Mostrar el nombre de los equipos de la nba cuyos jugadores pesen de media m�s de 228 libras, ordenarlos por la media del peso.
select nombre_equipo,avg(peso) as peso_max from jugadores group by nombre_equipo having avg(peso)>228 order by 2;
-- 27. Seleccionar qu� equipos de la nba tienen 2 o m�s jugadores espa�oles, ordenarlos por nombre del equipo.
select nombre_equipo from jugadores
where procedencia='spain' 
group by nombre_equipo
having count(*)>=2
order by nombre_equipo;
 
-- -- subconsultas
-- 28. Mostrar el nombre de los jugadores que juegan en equipos de la divisi�n SouthWest.
select nombre_equipo,nombre from jugadores where nombre_equipo in (select nombre from equipos where division='Southwest') order by 1;
-- 29. Mostrar nombre y altura del jugador m�s alto de la nba.
select nombre,altura from jugadores where altura=(select max(altura) from jugadores);
-- 30. Mostrar el nombre de la divisi�n de los equipos donde juegan jugadores espa�oles.
select nombre, division from equipos where nombre in (select nombre_equipo from jugadores where procedencia='spain');
-- 31. Mostrar el nombre de todos los equipos, s� hay jugadores espa�oles jugando en el equipo 'Raptors'.
select nombre from equipos where exists(select 1 from jugadores where nombre_equipo='RAPTORS' and procedencia='spain');
-- 32. Mostrar la conferencia de los equipos que tienen jugadores espa�oles.
select distinct Conferencia from equipos where exists(select 1 from jugadores where procedencia='SPAIN' and jugadores.nombre_equipo=equipos.nombre);
-- 33. Mostrar el nombre de los equipos que no tienen jugadores espa�oles.
select nombre from equipos where not exists(select 1 from jugadores where procedencia='SPAIN' and jugadores.nombre_equipo=equipos.nombre);
select nombre from equipos where nombre not in (select nombre_equipo from jugadores where procedencia='SPAIN');
-- 34. Nombre y peso de todos los jugadores de la nba que pesan m�s que todos los jugadores espa�oles.
select nombre, peso from jugadores where peso > all (select peso from jugadores where procedencia='spain') order by peso desc;
-- 35. Mostrar el nombre y peso de los jugadores base (posici�n = 'G') que pesan m�s que algun pivot (posici�n = 'C') de la nba.
select nombre, peso from jugadores where posicion='G' and peso > any(select peso from jugadores where posicion='C');
-- 36. Mostrar todos los datos del equipo donde juega el jugador m�s alto de la nba.
select * from equipos where nombre =(select nombre_equipo from jugadores where altura=(select max(altura) from jugadores));

-- -- Consultas multitabla
-- 37. N�mero de jugadores de cada conferencia
select conferencia,count(codigo) 
from equipos e, jugadores j
where e.nombre=j.nombre_equipo
group by conferencia;
-- 38. Nombre de equipo, conferencia y posici�n de los jugadores que hayan anotado m�s de 35 puntos de media en una temporada
select distinct j.nombre_equipo,e.conferencia,j.posicion
from jugadores j, equipos e, estadisticas est
where j.nombre_equipo=e.nombre and j.codigo=est.jugador
and est.puntos_por_partido>35;
-- 39. Promedio de altura de los jugadores que pongan m�s de 1,5 tapones por partido de media en la temporada
select avg(j.altura)
from jugadores j, estadisticas est
where j.codigo=est.jugador
and tapones_por_partido>1.5;
-- 40. Conferencia y nombre de los equipos local y visitante que hayan jugado partidos finalizados con m�s de 20 puntos de 
-- diferencia en la temporada 07/08
select local.nombre,local.conferencia,visitante.nombre,visitante.conferencia, puntos_local,puntos_visitante
from equipos local,partidos p,equipos visitante
where local.nombre=p.equipo_local and visitante.nombre=p.equipo_visitante
and p.temporada='07/08' 
and abs(puntos_local-puntos_visitante)>20 ;
-- 41. Nombre, Equipo, puntos por partido, asistencias por partido, tapones por partido y rebotes por partido de los jugadores 
-- espa�oles en la temporada 07/08
select nombre, nombre_equipo,Puntos_por_partido,Asistencias_por_partido,Tapones_por_partido,Rebotes_por_partido
from jugadores j,estadisticas est
where j.codigo=est.jugador
and j.procedencia='spain' and est.temporada='07/08';
-- 42. Para cada jugador, mostrar su nombre, nombre de equipo y puesto en el que juega.
select nombre, nombre_equipo,posicion from jugadores;
-- 43. Para cada equipo, mostrar todos los datos del equipo y n�mero de partidos que han jugado como locales.
select Nombre,Ciudad,Conferencia,Division,count(p.codigo)
from equipos e,partidos p
where e.nombre=p.equipo_local
group by Nombre,Ciudad,Conferencia,Division;
-- 44. Nombres de los equipos que jugaron alg�n partido resuelto por un solo punto de diferencia y temporada en que se jug� el partido.
select  equipo_local
from partidos 
where abs(puntos_local-puntos_visitante)=1
union
select  equipo_visitante
from partidos 
where abs(puntos_local-puntos_visitante)=1;
-- 45. Datos de los equipos que no han ganado ning�n partido como visitantes
select * from equipos where nombre not in (select equipo_visitante from partidos where puntos_local<puntos_visitante);
-- 46. Nombre, peso y equipo de los jugadores cuyo peso coincide con el de otro jugador de su mismo equipo (y nombre del jugador con el que coinciden)
select j1.codigo,j1.nombre,j1.peso,j1.nombre_equipo,j2.codigo,j2.nombre,j2.peso
from jugadores j1, jugadores j2
where j1.nombre_equipo=j2.nombre_equipo and j1.peso=j2.peso and j1.codigo<j2.codigo;
-- 47. Muestra para cada equipo, su nombre y el nombre de los equipos de su misma conferencia.
select e1.nombre,e2.nombre,e1.conferencia,e2.conferencia
from equipos e1, equipos e2
where e1.conferencia=e2.conferencia
and e1.nombre<e2.nombre;
-- 48. Nombre, conferencia y n�mero de jugadores de cada equipo. Si un equipo no tiene jugadores debe mostrar un 0.
select e.nombre,e.conferencia,count(j.codigo) as jugadores
from jugadores j right outer join equipos e on( j.nombre_equipo=e.nombre)
group by e.nombre, e.conferencia;
-- 49. N�mero de partidos ganados O perdidos por cada equipo. Si un equipo no tiene partidos, debe mostrar 0.
select e.nombre,count(p.codigo)
from equipos e left outer join partidos p on ((e.nombre=p.equipo_local and puntos_local>puntos_visitante)
or (e.nombre=p.equipo_visitante and puntos_local<puntos_visitante))
group by e.nombre
order by 1;
-- 50. N�mero de partidos ganados Y perdidos por cada equipo. Si un equipo no tiene partidos, debe mostrar 0.
select e.nombre,Q1.partidos as ganados,Q2.partidos as perdidos
from equipos e,(select equipo_local,count(*) as partidos from partidos p where p.puntos_local>p.puntos_visitante group by equipo_local) Q1,(select equipo_visitante,count(*) as partidos from partidos p where p.puntos_local<p.puntos_visitante group by equipo_visitante) Q2
where e.nombre=Q1.equipo_local and e.nombre=Q2.equipo_visitante;
