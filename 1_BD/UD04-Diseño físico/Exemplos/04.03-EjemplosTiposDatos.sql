DROP DATABASE IF EXISTS PRUEBAS;
CREATE SCHEMA pruebas;

USE pruebas;


# EJEMPLO 1

create table comprueba_Enteros(
	serie	serial,
	tiny 	tinyint,
	untiny 	tinyint unsigned,
	small 	smallint,
	med 	mediumint,
	entero	int,
	big		bigint,
	zero	tinyint zerofill
);

insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
					values (127,255,32767,8388607,2147483647,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (128,255,32767,8388607,2147483647,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (127,256,32767,8388607,2147483647,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (127,255,32768,8388607,2147483647,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (127,255,32767,8388608,2147483647,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (127,255,32767,8388607,2147483648,9223372036854775807,1);
insert into comprueba_Enteros(tiny,untiny,small,med,entero,big,zero)
                    values (127,255,32767,8388607,2147483647,9223372036854775808,1);
select * from comprueba_enteros;

-- EJEMPLO 2

CREATE TABLE test2 (id TINYINT(10), d DECIMAL(3,1));

INSERT INTO test2 (id, d) VALUES (100000000, 123.4);	-- Error. 'id' Fuera de rango, valores entre -128 y 127.
INSERT INTO test2 (id, d) VALUES (127, 123.4);			-- Error. 'd' Fuera de rango, 3 digitos total (parte entera + parte decimal).
INSERT INTO test2 (id, d) VALUES (127, 13.4);

SELECT id, d FROM test2;

# EJEMPLO 3

-- La especificación del ancho se suele utilizar con zerofill porque resulta cómodo ver los resultados:

CREATE TABLE  test3 (id INT (3) ZEROFILL , id2 INT ZEROFILL);

INSERT INTO test3 ( id , id2 ) VALUES (22,22) ;

SELECT  * FROM  test3 ;

# EJEMPLO 4

CREATE TABLE test4 (v CHAR(256));	-- Error. Tamaño MÁXIMO para la columna 255
CREATE TABLE test4 (v CHAR(255));



CREATE TABLE test5 (i int , v VARCHAR(4), c CHAR(4));

INSERT INTO test5 VALUES (1,'', '');
INSERT INTO test5 VALUES (2,'ab', 'ab');
INSERT INTO test5 VALUES (3,'abcd', 'abcd');
INSERT INTO test5 VALUES (4,'abcdefgh', 'abcdefgh');	-- Error. Cadena demasiado larga. MAXIMO 4 caracteres.
INSERT INTO test5 VALUES (5,'ABCD   ', 'ABCD   ');		-- Advertencia. Datos truncados, porque son espacios en blanco.

SELECT * FROM test5;
SELECT * FROM test5 WHERE v LIKE 'abcd';				-- Comparación de cadenas.


#EJEMPLO 6

CREATE TABLE test6 (v VARCHAR(65535));	-- Error. Cadena demasiado larga. MAXIMO 21845 caracteres.

CREATE TABLE test6 (v VARCHAR(21845));	-- Error. Cadena demasiado larga. MAXIMO 21845 caracteres 
										-- (incluyendo el bit adicional para la longitud de cada una de las columnas).
CREATE TABLE test6 (v VARCHAR(21844));	-- Error. Cadena demasiado larga. MAXIMO 21845 caracteres
										-- (incluyendo el bit adicional para la longitud de cada una de las columnas).
 
CREATE TABLE test61 (v VARCHAR(21844), v1 VARCHAR(1));	-- Error. Cadena demasiado larga. MAXIMO 21845 caracteres.
														-- (incluyendo el bit adicional para la longitud de cada una de las columnas).
                                                        
CREATE TABLE test61 (v VARCHAR(21842), v1 VARCHAR(1));

#EJEMPLO 7

CREATE TABLE test7 (i int, B BINARY(4)); -- equivalente a  CREATE TABLE test7 (i int, B char(4) BINARY);

INSERT INTO test7 VALUES (1,'abcd');
INSERT INTO test7 VALUES (2,'ABCD');

SELECT * FROM test7 WHERE B LIKE 'abcd';
SELECT * FROM test7 WHERE B LIKE 'ABCD';

#EJEMPLO 8
CREATE TABLE test8 (i INT, b BLOB(4), t TEXT(4));

INSERT INTO test8 VALUES (1,'abcd', 'abcd');

SELECT * FROM test8 WHERE b LIKE 'abcd';
SELECT * FROM test8 WHERE b LIKE 'Abcd';

SELECT * FROM test8 WHERE t LIKE 'abcd';
SELECT * FROM test8 WHERE t LIKE 'Abcd';

#EJEMPLO 9 - ENUM
CREATE TABLE  test9  (i int, e  ENUM('','null','true', 'false'));

INSERT INTO test9 (i,e) VALUES (1,'true');
INSERT INTO test9 (i,e) VALUES (2,'folse'); -- Error. El valor NO existe en la lista de valores
select @@sql_mode -- consultamos el modo sql -> está en modo tradicional por defecto y por eso falla
set session sql_mode='ANSI'; -- lo ponemos a modo ANSI, más permisivo
INSERT INTO test9 (i,e) VALUES (2,'folse'); -- ahora sí que lo inserta
set session sql_mode='TRADITIONAL';

INSERT INTO test9 (i,e) VALUES (3,'');
INSERT INTO test9 (i,e) VALUES (4,NULL);
INSERT INTO test9 (i,e) VALUES (5,'false'); -- Error. El valor NO existe en la lista de valores
INSERT INTO test9 (i,e) VALUES(6,2);
INSERT INTO test9 (i,e) VALUES(7,1); -- si almacenamos un número (escrito sin comillas) en el atributo ENUM, entonces el número se considera el índice del valor que debe almacenarse. 

SELECT i,e from test9  ;

SELECT * FROM test9 WHERE e=0; -- Buscar filas de una tabla en las que el atributo de tipo ENUM ha tomado un valor inválido (solo con modo SQL no estricto)

SELECT i, e+0 FROM test9; -- Obtener el índice asociado a un ENUM


CREATE TABLE  test91  (i int, e  ENUM('','null','true', 'false') not null);

INSERT INTO test91  (i,e) VALUES (1,'true');
INSERT INTO test91  (i,e) VALUES (2,'');
INSERT INTO test91  (i,e) VALUES (3,NULL);	-- Valor no permitido porque la columna no permite nulos.

SELECT i,e from test91  ;


SELECT * from test9 where e = 1 or e is null or e = 3 or e = 4;

SELECT * from test9 order by e desc; --ordena por índice, no por valor numérico

#EJEMPLO 10 - SET

CREATE TABLE  test10 (fruta SET('manzana','mango','pera','banana',' '));

INSERT INTO test10 VALUES ('banana' ) ;
INSERT INTO test10 VALUES ('mango');
INSERT INTO test10 VALUES ('pera');
insert into test10 values ('manzana');

SELECT * FROM test10;

INSERT INTO test10 VALUES ('manzana,mango'); -- OJO: sin espacios!
SELECT * FROM test10 ORDER BY fruta;

SELECT * from test10 WHERE FIND_IN_SET('manzana', fruta)>0;

SELECT * from test10 WHERE fruta like '%man%';

SELECT * from test10 WHERE fruta='manzana,mango';

SELECT * from test10 WHERE fruta='mango,manzana'; -- no devuelve nada porque los valores deben indicarse en el mismo orden que en el set

SELECT * from test10 WHERE fruta & 1; -- devuelve los que tengan asignado el primer valor del conjunto (manzana)

#EJEMPLO 11

CREATE TABLE  test11 ( f DATETIME );

INSERT INTO   test11 VALUES 	('98-12-31 11:30:45'),
								('98.12.31 11+30+45'),
								(981231113045),
                            	('981231113045'),
                            	(now());
                                
INSERT INTO   test11 VALUES 	('982131119045'); -- Error. Mes y minutos incorrecto
                                

SELECT * FROM test11;

#EJEMPLO 12

CREATE TABLE  test12 ( t TIMESTAMP );

INSERT INTO   test12 VALUES ('98-12-31 11:30:45'),
			       	        ('98.12.31'),
							(981231113045),
							('681231111045'),	-- Error. Año ilegal debe ser igual o mayor a 1970.
                            (date(now()));

SELECT * FROM test12;

#EJEMPLO 13

create table test13(
  c1 int(2),
  c2 decimal(4,2),
  c3 decimal(2,0)
);

insert into test13(c1,c2,c3) values (99,99.99,99);
insert into test13(c1,c2,c3) values (999,99.99,99); -- FUNCIONA!!! -> no debería
insert into test13(c1,c2,c3) values (99,999.9,99); -- Error (el decimal(4,2) excede el rango) -> se comporta correctamente
insert into test13(c1,c2,c3) values (99,99.99,99.9); -- Error (el decimal(2,0) excede el rango) -> se comporta correctamente

-- miramos cuanto está ocupando realmente cada campo:
select column_name,data_type,numeric_precision,numeric_scale from information_schema.columns where table_name='test13'; -- El int(2) lo está guardando como un int, por eso acepta 10 dígitos!!!

#EJEMPLO 14

create table test14(
  c1 char(2),
  c2 varchar(2)
);

insert into test14(c1,c2) values ('aa','aa'); 
insert into test14(c1,c2) values ('aaa','aa'); -- Error -> correcto
insert into test14(c1,c2) values ('aa','aaa'); -- Error -> correcto

-- miramos la longitud de cada una de las columnas en el diccionario de datos
select column_name,data_type,character_maximum_length from information_schema.columns where table_name='test14'; -- Con las cadenas sí que funciona correctamente


